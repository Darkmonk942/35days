package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.objects.bonus.BonusTrainingMainObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetBonusList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 24.12.2016.
 */

public class BonusMainActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_list) LinearLayout linearList;
    // ToolBar
    @BindView(R.id.toolbar_menu) Toolbar toolbarMenu;
    // Примитивы
    String token;
    Optimization optimization;
    GetBonusList getBonusList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bonus_main);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен
        token = "Bearer " + Paper.book().read("token");

        // Добавляем Дроер
        NavigationDrawer navigationDrawer = new NavigationDrawer();
        navigationDrawer.NavigationDrawer(this, toolbarMenu, 7);

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Формируем запрос на получение списка тренировок
        getBonusList=retrofit.create(GetBonusList.class);

        // Отправляем запрос на получение листа тренировок
        getTrainingList();

    }

    // Получаем лист тренировок
    public void getTrainingList(){
        Call<BonusTrainingMainObject> getTrainingsSuccess = getBonusList.getBonusList(token);

        getTrainingsSuccess.enqueue(new Callback<BonusTrainingMainObject>() {
            @Override
            public void onResponse(Call<BonusTrainingMainObject> call, Response<BonusTrainingMainObject> response) {
                Log.d("БонусыЛист", String.valueOf(response.code()));
                if(response.code()==200){
                    // Создаём список тренировок
                    createTrainsList(response.body());
                }

            }
            @Override
            public void onFailure(Call<BonusTrainingMainObject> call, Throwable t) {
                Log.d("БонусыЛист", t.toString());
            }
        });
    }

    // Создаём список видео тренировок
    public void createTrainsList(BonusTrainingMainObject trainList){
        for (int i=0;i<trainList.allTrains.size();i++){

            View trainFrame = View.inflate(this, R.layout.item_bonus_main_list, null);

            TextView trainType=(TextView)trainFrame.findViewById(R.id.train_type);
            TextView trainName=(TextView)trainFrame.findViewById(R.id.train_name);
            ImageView imageTrain=(ImageView)trainFrame.findViewById(R.id.train_image);
            Button btnStart=(Button)trainFrame.findViewById(R.id.btn_start);

            Pair<Integer,String> trainObject=
                    new Pair(trainList.allTrains.get(i).id,trainList.allTrains.get(i).bonusText);
            trainName.setText(trainList.allTrains.get(i).bonusText);
            btnStart.setTag(trainObject);
            Glide.with(this)
                    .load(trainList.allTrains.get(i).bonusPic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageTrain);

            optimization.Optimization((FrameLayout)trainFrame);

            linearList.addView(trainFrame);
        }
    }

    // Переход на экран выбранной темы со списком тренировок
    public void currentTrainClick(View v){
        Pair<Integer,String> trainObject=( Pair<Integer,String>)v.getTag();
        if(trainObject.first>4){
            startActivity(new Intent(this, TrainVideoActivity.class)
                    .putExtra("trainType","bonus")
                    .putExtra("day", String.valueOf(trainObject.first))
                    .putExtra("trainName",trainObject.second));
        }else {
            startActivity(new Intent(this, BonusTrainActivity.class)
                    .putExtra("id", trainObject.first)
                    .putExtra("title",trainObject.second));
        }
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
