package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.days35.days35app.objects.voting.VotingMainScreenObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetVotingMain;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 13.12.2016.
 */

public class VotingMainActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // ToolBar
    @BindView(R.id.toolbar_menu) Toolbar toolbarMenu;
    // TextView
    @BindView(R.id.voting_available_text) TextView votingAvailableText;
    @BindView(R.id.voting_type_text) TextView votingTypeText;
    @BindView(R.id.text_rule_main) TextView textRule;
    @BindView(R.id.text_rule_body) TextView textRuleBody;
    // Button
    @BindView(R.id.btn_send) Button btnSend;
    // Примитивы
    GetVotingMain getVotingMain;
    VotingMainScreenObject votingMainObject;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voting_main);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Добавляем Дроер
        NavigationDrawer navigationDrawer=new NavigationDrawer();
        navigationDrawer.NavigationDrawer(this,toolbarMenu,6);

        // Получаем Токен
        token="Bearer "+ Paper.book().read("token");

        // Подключаем ретрофит и формируем запросы
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getVotingMain=retrofit.create(GetVotingMain.class);

        // Отправляем запрос на получение информации для главной голосования
        getMainInfo();
    }

    // Получаем с сервера информацию для главной страницы голосования
    public void getMainInfo(){
        Call<VotingMainScreenObject> getInfoSuccess = getVotingMain.getVotingInfo(token);

        getInfoSuccess.enqueue(new Callback<VotingMainScreenObject>() {
            @Override
            public void onResponse(Call<VotingMainScreenObject> call, Response<VotingMainScreenObject> response) {
                Log.d("ГолосованиеГлавная", String.valueOf(response.code()));
                if(response.code()==200){
                    votingMainObject=response.body();
                    textRule.setText(votingMainObject.caption);
                    textRuleBody.setText(votingMainObject.ruleText);
                    votingAvailableText.setText(response.body().alertDate);
                    votingTypeText.setText(votingMainObject.alertText);

                    // Проверка на доступность голосования
                    if(votingMainObject.votingAvailable){
                        btnSend.setVisibility(View.VISIBLE);
                        btnSend.setTag("voting");
                    }

                    // Проверка на доступность топа
                    if(votingMainObject.topAvailable){
                        btnSend.setVisibility(View.VISIBLE);
                        btnSend.setTag("top");
                        btnSend.setText(getResources().getString(R.string.vote_results));
                    }


                }else{
                }

            }
            @Override
            public void onFailure(Call<VotingMainScreenObject> call, Throwable t) {

            }
        });
    }

    // Клик для начала голосования
    public void voteStatsClick(View v){
        if (v.getTag().toString().equals("voting")){
            startActivity(new Intent(this,TinderTest.class));
        }else{
            startActivity(new Intent(this,VotingStatsActivity.class));
        }

    }

    public void statsClick(View v){
        startActivity(new Intent(this,VotingStatsActivity.class));
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
