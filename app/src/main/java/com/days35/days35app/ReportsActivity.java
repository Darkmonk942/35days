package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.days35.days35app.objects.reports.ReportMainObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetReportsInfo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 04.11.2016.
 */

public class ReportsActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // ToolBar
    @BindView(R.id.toolbar_menu) Toolbar toolbarMenu;
    // ImageView
    @BindView(R.id.img_triangle1) ImageView trianglePhoto;
    @BindView(R.id.img_triangle2) ImageView triangleVideo;
    @BindView(R.id.img_triangle3) ImageView triangleTest;
    @BindView(R.id.img_triangle4) ImageView triangleParams;
    // TextView
    @BindView(R.id.text_date1) TextView textPhoto;
    @BindView(R.id.text_date2) TextView textVideo;
    @BindView(R.id.text_date3) TextView textTest;
    @BindView(R.id.text_date4) TextView textParams;
    // Примитивы
    GetReportsInfo getReportsInfo;
    String token;
    Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен
        token="Bearer "+ Paper.book().read("token");

        // Добавляем Дроер
        NavigationDrawer navigationDrawer=new NavigationDrawer();
        navigationDrawer.NavigationDrawer(this,toolbarMenu,5);

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getReportsInfo=retrofit.create(GetReportsInfo.class);

        // Запрос на получение инфы об отчётах
        downloadReportsInfo();

    }

    // Выкачиваем всю инфу об отчётах
    public void downloadReportsInfo(){
        Call<List<ReportMainObject>> getReportsSuccess = getReportsInfo.getReportsInfo(token);

        getReportsSuccess.enqueue(new Callback<List<ReportMainObject>>() {
            @Override
            public void onResponse(Call<List<ReportMainObject>> call, Response<List<ReportMainObject>> response) {

                Log.d("ReportTable", String.valueOf(response.code()));
                if(response.code()==200){

                    // Отображаем все сообщения об отчётах
                    setReportsInfo(response.body());
                }

            }
            @Override
            public void onFailure(Call<List<ReportMainObject>> call, Throwable t) {
                Log.d("ReportTableEx", t.toString());
            }
        });
    }

    // Выводим всю инфу об отчётах
    public void setReportsInfo(List<ReportMainObject> allInfo){

        // Фото отчёты
        textPhoto.setText(allInfo.get(0).message);
        if(!allInfo.get(0).message.equals("")){
            trianglePhoto.setBackgroundResource(R.drawable.img_triangle_red);
        }

        // Видео отчёты
        textVideo.setText(allInfo.get(1).message);
        if(!allInfo.get(1).message.equals("")){
            triangleVideo.setBackgroundResource(R.drawable.img_triangle_red);
        }

        // Тесты тренировок
        textTest.setText(allInfo.get(2).message);
        if(!allInfo.get(2).message.equals("")){
            triangleTest.setBackgroundResource(R.drawable.img_triangle_red);
        }

        // Параметры тела пользователя
        textParams.setText(allInfo.get(3).message);
        if(!allInfo.get(3).equals("")){
            triangleParams.setBackgroundResource(R.drawable.img_triangle_red);
        }

        // Критические сообщения для СнекБара
        if(!allInfo.get(4).message.equals("")){
            showSnackbar(allInfo.get(4).message);
        }

    }

    // Переход на фото отчёты (Отчётное задание)
    public void clickPhotoReport(View v){
        startActivity(new Intent(this,ReportsPhotoActivity.class));
    }

    // Переход на видео отчёты (Отчётное задание)
    public void clickVideoReport(View v){
        startActivity(new Intent(this,ReportsVideoActivity.class));
    }

    // Переход на экран фитнес теста
    public void clickTableTest(View v){
        startActivity(new Intent(this,ReportsTableTestActivity.class));
    }

    // Переход на экран таблицы параметров
    public void clickTableParams(View v){
        startActivity(new Intent(this,ReportsTableParamsActivity.class));
    }

    // Снекбар
    public void showSnackbar(String alertText){
        snackbar = Snackbar
                .make(findViewById(android.R.id.content), alertText, Snackbar.LENGTH_INDEFINITE)
                .setAction("выкл", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });
        snackbar.show();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
