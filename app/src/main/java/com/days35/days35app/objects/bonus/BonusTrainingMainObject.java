package com.days35.days35app.objects.bonus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 03.01.2017.
 */

public class BonusTrainingMainObject {
    @SerializedName("trainings")
    @Expose
    public ArrayList<BonusBodyMainObject> allTrains;

    @SerializedName("is_bonus_available")
    @Expose
    public boolean isAvailable;

}
