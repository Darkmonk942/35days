package com.days35.days35app.objects.bonus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 31.12.2016.
 */

public class BonusTrainingObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("training_title")
    @Expose
    public String trainingTitle;

    @SerializedName("training_time")
    @Expose
    public int trainingTime;

    @SerializedName("video_time")
    @Expose
    public int videoTime;

    @SerializedName("training_pic")
    @Expose
    public String trainingPic;

    @SerializedName("training_vid")
    @Expose
    public String trainingVid;

    public String textPreview="";
}
