package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 04.12.2016.
 */

public class UserParamsObject {

    @SerializedName("week_id")
    @Expose
    public int weekId;

    @SerializedName("weight")
    @Expose
    public int weight;

    @SerializedName("chest")
    @Expose
    public int chest;

    @SerializedName("waist")
    @Expose
    public int waist;

    @SerializedName("hips")
    @Expose
    public int hips;

    @SerializedName("feet")
    @Expose
    public int feet;

    @SerializedName("hand")
    @Expose
    public int hand;



}
