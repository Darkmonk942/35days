package com.days35.days35app.objects.reports;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 22.12.2016.
 */

public class ReportsTestsBodyObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("burpee")
    @Expose
    public int burpee;

    @SerializedName("dropout")
    @Expose
    public int dropOut;

    @SerializedName("pushup")
    @Expose
    public int pushUp;

    @SerializedName("hyperextension")
    @Expose
    public int hyperextension;

    @SerializedName("climb")
    @Expose
    public int climb;

    @SerializedName("press")
    @Expose
    public int press;
}
