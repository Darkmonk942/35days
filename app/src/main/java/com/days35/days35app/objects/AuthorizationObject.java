package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Darkmonk on 08.09.2016.
 */
public class AuthorizationObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("access_token")
    @Expose
    public String token;

    @SerializedName("first_name")
    @Expose
    public String firstName;

    @SerializedName("last_name")
    @Expose
    public String lastName;

    @SerializedName("username")
    @Expose
    public String userName;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("user_pic")
    @Expose
    public String userPic="";

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("age")
    @Expose
    public int age;

    @SerializedName("sex")
    @Expose
    public int sex;

    @SerializedName("weight")
    @Expose
    public int weight;

    @SerializedName("height")
    @Expose
    public int height;

    @SerializedName("chest")
    @Expose
    public int chest;

    @SerializedName("waist")
    @Expose
    public int waist;

    @SerializedName("hips")
    @Expose
    public int hips;

    @SerializedName("feet")
    @Expose
    public int legs;

    @SerializedName("hand")
    @Expose
    public int arms;

    @SerializedName("is_pic_before")
    @Expose
    public int isPhotoBefore;

    @SerializedName("is_profile_full")
    @Expose
    public int isProfileFull;

    @SerializedName("about_me")
    @Expose
    public String aboutMe;

    @SerializedName("premium_id")
    @Expose
    public int premiumId;

    @SerializedName("premium")
    @Expose
    public String premiumName;

    @SerializedName("immunity")
    @Expose
    public int immunityCount;
}
