package com.days35.days35app.objects;

/**
 * Created by DarKMonK on 26.09.2016.
 */

public class CarouselItem {
    public int imageId;
    public String name;

    public CarouselItem (int imageId, String name){
        this.imageId = imageId;
        this.name = name;
    }
}
