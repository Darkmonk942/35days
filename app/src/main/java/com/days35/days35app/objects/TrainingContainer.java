package com.days35.days35app.objects;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.days35.days35app.JCCustomPlayer;

/**
 * Created by DarKMonK on 13.02.2017.
 */

public class TrainingContainer {

    public JCCustomPlayer videoPlayer;
    public ProgressBar progressBar;
    public ImageView trainImage;
    public ImageView startImage;
    public ImageView shadowImage;
    public TextView trainTime;
    public String videoLink;

    public TrainingContainer(JCCustomPlayer videoPlayer,
                             ProgressBar progressBar,
                             ImageView trainImage,
                             ImageView startImage,
                             ImageView shadowImage,
                             TextView trainTime,
                             String videoLink){

        this.videoPlayer=videoPlayer;
        this.progressBar=progressBar;
        this.trainImage=trainImage;
        this.startImage=startImage;
        this.shadowImage=shadowImage;
        this.trainTime=trainTime;
        this.videoLink=videoLink;
    }
}
