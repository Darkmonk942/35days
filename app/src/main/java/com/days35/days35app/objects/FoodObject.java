package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 03.11.2016.
 */

public class FoodObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("food_title")
    @Expose
    public String foodTitle;

    @SerializedName("food_name")
    @Expose
    public String foodName;

    @SerializedName("food_text")
    @Expose
    public String foodText;

    @SerializedName("food_ingredients")
    @Expose
    public ArrayList<IngredientsObject> ingredientsList;

    @SerializedName("food_pic")
    @Expose
    public String foodPic;

}
