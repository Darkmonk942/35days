package com.days35.days35app.objects;

import android.widget.TextView;

/**
 * Created by DarKMonK on 29.10.2016.
 */

public class LikeViewObject {
    public int postId;
    public boolean isLiked;
    public TextView likeCount;
}
