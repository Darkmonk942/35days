package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 08.11.2016.
 */

public class TrainWeekObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("week_title")
    @Expose
    public String weekTitle="";

    @SerializedName("week_text")
    @Expose
    public String weekText="";

    @SerializedName("start_date")
    @Expose
    public String startDate="";

    @SerializedName("active")
    @Expose
    public boolean isActive;

}
