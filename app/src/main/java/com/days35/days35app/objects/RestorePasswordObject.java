package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 16.09.2016.
 */
public class RestorePasswordObject {

    @SerializedName("message")
    @Expose
    public String message;
}
