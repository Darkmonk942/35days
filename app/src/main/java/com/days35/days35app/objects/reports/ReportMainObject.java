package com.days35.days35app.objects.reports;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 08.12.2016.
 */

public class ReportMainObject {
    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("highlined")
    @Expose
    public int highlined;

    @SerializedName("type")
    @Expose
    public String type;
}
