package com.days35.days35app.objects.reports;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 21.12.2016.
 */

public class ReportVideoBodyObject {
    @SerializedName("week_id")
    @Expose
    public int weekId;

    @SerializedName("report_text")
    @Expose
    public String reportVideoText;

    @SerializedName("report_pic")
    @Expose
    public String reportPicLink;

    @SerializedName("report_vid")
    @Expose
    public String reportVideoLink;
}
