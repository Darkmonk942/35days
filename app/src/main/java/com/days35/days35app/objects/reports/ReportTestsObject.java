package com.days35.days35app.objects.reports;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 22.12.2016.
 */

public class ReportTestsObject {
    @SerializedName("week")
    @Expose
    public ArrayList<ReportsTestsBodyObject> weekStats;

    @SerializedName("alert")
    @Expose
    public String alertText;
}
