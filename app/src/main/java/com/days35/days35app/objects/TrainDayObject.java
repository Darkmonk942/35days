package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 10.11.2016.
 */

public class TrainDayObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("day_text")
    @Expose
    public String dayText;

    @SerializedName("active")
    @Expose
    public boolean isActive;

    @SerializedName("day_time")
    @Expose
    public String timeText;

    @SerializedName("day_title")
    @Expose
    public String titleText;

    @SerializedName("day_pic")
    @Expose
    public String dayPic;
}
