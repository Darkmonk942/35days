package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 18.09.2016.
 */
public class ImageUploadObject {
    @SerializedName("user_pic")
    @Expose
    public String userPic;
}
