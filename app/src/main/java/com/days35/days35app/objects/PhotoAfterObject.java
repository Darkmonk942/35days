package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 10.12.2016.
 */

public class PhotoAfterObject {

    @SerializedName("front1_after")
    @Expose
    public String frontAfter1="";

    @SerializedName("front2_after")
    @Expose
    public String frontAfter2="";

    @SerializedName("back_after")
    @Expose
    public String backAfter="";

    @SerializedName("left_after")
    @Expose
    public String leftAfter="";

    @SerializedName("right_after")
    @Expose
    public String rightAfter="";
}
