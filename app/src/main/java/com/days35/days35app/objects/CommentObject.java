package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 21.10.2016.
 */

public class CommentObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("post_id")
    @Expose
    public int postId;

    @SerializedName("user")
    @Expose
    public UserPost userInfo;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

    @SerializedName("comment_text")
    @Expose
    public String commentText;
}
