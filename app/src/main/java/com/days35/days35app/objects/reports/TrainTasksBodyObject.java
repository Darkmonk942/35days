package com.days35.days35app.objects.reports;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 25.12.2016.
 */

public class TrainTasksBodyObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("training_title")
    @Expose
    public String trainingTitle;

    @SerializedName("training_time")
    @Expose
    public int trainingTime;

    @SerializedName("training_pic")
    @Expose
    public String trainingPic;

    @SerializedName("training_vid")
    @Expose
    public String trainingVid;

    @SerializedName("is_test")
    @Expose
    public boolean isTestTrain;

    @SerializedName("is_pause")
    @Expose
    public boolean isPauseTrain;

    @SerializedName("pause_time")
    @Expose
    public int pauseTime;
}
