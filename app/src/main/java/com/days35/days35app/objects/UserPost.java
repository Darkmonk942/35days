package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 19.10.2016.
 */

public class UserPost {
    @SerializedName("first_name")
    @Expose
    public String firstName;

    @SerializedName("last_name")
    @Expose
    public String lastName;

    @SerializedName("user_pic")
    @Expose
    public String userPic;

    @SerializedName("id")
    @Expose
    public int userId;

}
