package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DarKMonK on 04.03.2017.
 */

public class UserStatsObject {

    @SerializedName("current_week")
    @Expose
    public int currentWeek;

    @SerializedName("is_available")
    @Expose
    public boolean isAvailable;

    @SerializedName("stats")
    @Expose
    public List<UserParamsObject> userParams;
}
