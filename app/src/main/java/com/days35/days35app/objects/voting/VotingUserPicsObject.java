package com.days35.days35app.objects.voting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 26.12.2016.
 */

public class VotingUserPicsObject {
    @SerializedName("left_before")
    @Expose
    public String leftBefore;

    @SerializedName("front1_before")
    @Expose
    public String frontBefore;

    @SerializedName("back_before")
    @Expose
    public String backBefore;

    @SerializedName("left_after")
    @Expose
    public String leftAfter;

    @SerializedName("front1_after")
    @Expose
    public String frontAfter;

    @SerializedName("back_after")
    @Expose
    public String backAfter;
}
