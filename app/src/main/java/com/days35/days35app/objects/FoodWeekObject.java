package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 31.10.2016.
 */

public class FoodWeekObject {
    @SerializedName("week")
    @Expose
    public String week;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("available")
    @Expose
    public boolean isAvailable;


}
