package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 04.12.2016.
 */

public class PhotoBeforeObject {
    @SerializedName("front1_before")
    @Expose
    public String frontBefore1;

    @SerializedName("front2_before")
    @Expose
    public String frontBefore2;

    @SerializedName("back_before")
    @Expose
    public String backBefore;

    @SerializedName("left_before")
    @Expose
    public String leftBefore;

    @SerializedName("right_before")
    @Expose
    public String rightBefore;
}
