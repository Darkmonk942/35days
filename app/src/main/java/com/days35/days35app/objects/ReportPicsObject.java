package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 17.12.2016.
 */

public class ReportPicsObject {

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("moderated")
    @Expose
    public int moderated;

    @SerializedName("days_left_till_after_upload")
    @Expose
    public String daysTillUpload;

    @SerializedName("is_after_pic_upload_available")
    @Expose
    public boolean isAfterUploadAvailable;

    @SerializedName("is_before_pic_upload_available")
    @Expose
    public boolean isBeforeUploadAvailable;

    @SerializedName("front1_before")
    @Expose
    public String frontBefore1;

    @SerializedName("front2_before")
    @Expose
    public String frontBefore2;

    @SerializedName("back_before")
    @Expose
    public String backBefore;

    @SerializedName("left_before")
    @Expose
    public String leftBefore;

    @SerializedName("right_before")
    @Expose
    public String rightBefore;

    @SerializedName("front1_after")
    @Expose
    public String frontAfter1="";

    @SerializedName("front2_after")
    @Expose
    public String frontAfter2="";

    @SerializedName("back_after")
    @Expose
    public String backAfter="";

    @SerializedName("left_after")
    @Expose
    public String leftAfter="";

    @SerializedName("right_after")
    @Expose
    public String rightAfter="";

    @SerializedName("before_message")
    @Expose
    public String beforeMessage="";

    @SerializedName("after_message")
    @Expose
    public String afterMessage="";



}
