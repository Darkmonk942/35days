package com.days35.days35app.objects;

import com.days35.days35app.objects.reports.TrainTasksBodyObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DarKMonK on 10.11.2016.
 */

public class TrainTasksObject {
    @SerializedName("training")
    @Expose
    public List<TrainTasksBodyObject> allVideos;

    @SerializedName("training_available")
    @Expose
    public boolean isTesting;
}


