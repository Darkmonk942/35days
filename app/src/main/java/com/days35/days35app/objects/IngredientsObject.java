package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 03.11.2016.
 */

public class IngredientsObject {

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("count")
    @Expose
    public String count;
}
