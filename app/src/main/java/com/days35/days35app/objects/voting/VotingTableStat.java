package com.days35.days35app.objects.voting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 30.12.2016.
 */

public class VotingTableStat {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("first_name")
    @Expose
    public String firstName;

    @SerializedName("last_name")
    @Expose
    public String lastName;

    @SerializedName("user_pic")
    @Expose
    public String userPic;

    @SerializedName("vote")
    @Expose
    public int vote;

    @SerializedName("is_pass")
    @Expose
    public boolean isPassed;

    @SerializedName("place")
    @Expose
    public int place;
}
