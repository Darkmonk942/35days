package com.days35.days35app.objects.bonus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 07.03.2017.
 */

public class BonusBodyMainObject {
    @SerializedName("type")
    @Expose
    public int id;

    @SerializedName("text")
    @Expose
    public String bonusText;

    @SerializedName("pic")
    @Expose
    public String bonusPic;

    @SerializedName("orient")
    @Expose
    public int orientation;
}
