package com.days35.days35app.objects;

/**
 * Created by Darkmonk on 08.09.2016.
 */
public class UserProfileObject {
    public int userId;
    public String userPic;
    public String name;
    public String surname;
    public String email;
    public String phone;
    public int age;
    public int sex;
    public int weight;
    public int height;
    public int chest;
    public int waist;
    public int hips;
    public int legs;
    public int arms;

}
