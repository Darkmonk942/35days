package com.days35.days35app.objects.reports;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 21.12.2016.
 */

public class ReportVideoObject {

    @SerializedName("week")
    @Expose
    public ArrayList<ReportVideoBodyObject> allVideos;

    @SerializedName("current_week")
    @Expose
    public int currentWeek;

    @SerializedName("is_available")
    @Expose
    public boolean isAvailable;
}
