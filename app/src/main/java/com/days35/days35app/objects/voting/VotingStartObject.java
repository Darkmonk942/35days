package com.days35.days35app.objects.voting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DarKMonK on 26.12.2016.
 */

public class VotingStartObject {

    @SerializedName("voting")
    @Expose
    public List<VotingMainObject> userObject;

    @SerializedName("time")
    @Expose
    public String time;
}
