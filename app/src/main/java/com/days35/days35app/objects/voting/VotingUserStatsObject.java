package com.days35.days35app.objects.voting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 26.12.2016.
 */

public class VotingUserStatsObject {

    @SerializedName("user_id")
    @Expose
    public String userId;

    @SerializedName("week_id")
    @Expose
    public String weekId;

    @SerializedName("weight")
    @Expose
    public String weight;

    @SerializedName("chest")
    @Expose
    public String chest;

    @SerializedName("waist")
    @Expose
    public String waist;

    @SerializedName("hips")
    @Expose
    public String hips;

    @SerializedName("feet")
    @Expose
    public String feet;

    @SerializedName("hand")
    @Expose
    public String hand;
}
