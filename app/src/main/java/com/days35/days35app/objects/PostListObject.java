package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 17.10.2016.
 */

public class PostListObject {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("user")
    @Expose
    public UserPost userPost;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("post_type")
    @Expose
    public int postType;

    @SerializedName("post_pic")
    @Expose
    public String postPic;

    @SerializedName("post_text")
    @Expose
    public String postText;

    @SerializedName("post_vid")
    @Expose
    public String postVideo;

    @SerializedName("like_cnt")
    @Expose
    public int likeCount;

    @SerializedName("comment_cnt")
    @Expose
    public int commentCount;

    @SerializedName("shared_cnt")
    @Expose
    public int shareCount;

    @SerializedName("is_i_liked")
    @Expose
    public boolean isLiked;
}
