package com.days35.days35app.objects.voting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DarKMonK on 26.12.2016.
 */

public class VotingMainObject {

    @SerializedName("id")
    @Expose
    public int userId;

    @SerializedName("userPicProfile")
    @Expose
    public VotingUserPicsObject userPics;

    @SerializedName("userStats")
    @Expose
    public List<VotingUserStatsObject> userStatsList;


}

