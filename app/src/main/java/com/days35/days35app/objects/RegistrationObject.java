package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Darkmonk on 29.08.2016.
 */
public class RegistrationObject {
    @SerializedName("access_token")
    @Expose
    public String token;

}
