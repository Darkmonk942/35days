package com.days35.days35app.objects.voting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 30.12.2016.
 */

public class VotingTableAllStats {
    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("vote")
    @Expose
    public int vote;

    @SerializedName("place")
    @Expose
    public int place;

    @SerializedName("is_pass")
    @Expose
    public boolean isPassed;

    @SerializedName("user")
    @Expose
    public VotingTableStat userStats;
}
