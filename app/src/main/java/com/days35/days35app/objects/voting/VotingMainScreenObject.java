package com.days35.days35app.objects.voting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DarKMonK on 27.12.2016.
 */

public class VotingMainScreenObject {

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("caption")
    @Expose
    public String caption;

    @SerializedName("text")
    @Expose
    public String ruleText;

    @SerializedName("alert")
    @Expose
    public String alertText;

    @SerializedName("alert_date")
    @Expose
    public String alertDate;

    @SerializedName("is_voting_available")
    @Expose
    public boolean votingAvailable;

    @SerializedName("is_top_available")
    @Expose
    public boolean topAvailable;
}
