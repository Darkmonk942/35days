package com.days35.days35app.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 03.11.2016.
 */

public class FoodRequestObject {
    @SerializedName("breakfast")
    @Expose
    public ArrayList<FoodObject> breakfastList;

    @SerializedName("lunch")
    @Expose
    public ArrayList<FoodObject> lunchList;

    @SerializedName("dinner")
    @Expose
    public ArrayList<FoodObject> dinnerList;

    @SerializedName("snack")
    @Expose
    public ArrayList<FoodObject> snikerList;

    @SerializedName("supper")
    @Expose
    public ArrayList<FoodObject> supperList;

}
