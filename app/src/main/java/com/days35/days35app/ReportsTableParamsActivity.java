package com.days35.days35app;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.days35.days35app.objects.UserParamsObject;
import com.days35.days35app.objects.UserStatsObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.UserGetParams;
import com.days35.days35app.retrofit.UserPostParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 30.11.2016.
 */

public class ReportsTableParamsActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_table) LinearLayout linearTable;
    // Button
    @BindView(R.id.btn_send) Button btnSendStats;
    // Примитивы
    UserGetParams userGetParams;
    UserPostParams userPostParams;
    ArrayList<String> tableType=new ArrayList();
    ArrayList<EditText> tableWeek1=new ArrayList();
    ArrayList<EditText> tableWeek2=new ArrayList();
    ArrayList<EditText> tableWeek3=new ArrayList();
    ArrayList<EditText> tableWeek4=new ArrayList();
    ArrayList<EditText> tableWeek5=new ArrayList();
    int currentParamsWeek=0;
    String token;
    Optimization optimization;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports_table_params);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен
        token="Bearer "+ Paper.book().read("token");

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        userGetParams=retrofit.create(UserGetParams.class);
        userPostParams=retrofit.create(UserPostParams.class);

        // Получаем с сервера ранее отправленные параметры
        getUserParams();

    }

    // Переход на предыдущий экран
    public void backClick(View v){
        finish();
    }

    // Получаем все параметры с сервера
    public void getUserParams(){
        Call<UserStatsObject> getStatsSuccess = userGetParams.getUserParams(token);

        getStatsSuccess.enqueue(new Callback<UserStatsObject>() {
            @Override
            public void onResponse(Call<UserStatsObject> call, Response<UserStatsObject> response) {

                Log.d("ReportTable", String.valueOf(response.code()));
                if(response.code()==200){
                    currentParamsWeek=response.body().currentWeek;
                    if(response.body().isAvailable){
                        btnSendStats.setVisibility(View.VISIBLE);
                    }

                    // Создаём таблицу для тестов
                    createTestTable(response.body().currentWeek,response.body().isAvailable);

                    // Заполняем ячейки таблицы
                    setParamsTable(response.body().userParams);
                }

            }
            @Override
            public void onFailure(Call<UserStatsObject> call, Throwable t) {

            }
        });

    }

    // Заполняем таблицу полученными параметрами
    public void setParamsTable(List<UserParamsObject> userParamsList){

        for (int i=0; i<userParamsList.size();i++){
           switch (i){
               case 0: tableWeek1.get(0).setText(String.valueOf(userParamsList.get(i).weight));
                   tableWeek1.get(1).setText(String.valueOf(userParamsList.get(i).chest));
                   tableWeek1.get(2).setText(String.valueOf(userParamsList.get(i).waist));
                   tableWeek1.get(3).setText(String.valueOf(userParamsList.get(i).hips));
                   tableWeek1.get(4).setText(String.valueOf(userParamsList.get(i).feet));
                   tableWeek1.get(5).setText(String.valueOf(userParamsList.get(i).hand));
                   break;
               case 1: tableWeek2.get(0).setText(String.valueOf(userParamsList.get(i).weight));
                   tableWeek2.get(1).setText(String.valueOf(userParamsList.get(i).chest));
                   tableWeek2.get(2).setText(String.valueOf(userParamsList.get(i).waist));
                   tableWeek2.get(3).setText(String.valueOf(userParamsList.get(i).hips));
                   tableWeek2.get(4).setText(String.valueOf(userParamsList.get(i).feet));
                   tableWeek2.get(5).setText(String.valueOf(userParamsList.get(i).hand));
                   break;
               case 2: tableWeek3.get(0).setText(userParamsList.get(i).weight);
                   tableWeek3.get(1).setText(userParamsList.get(i).chest);
                   tableWeek3.get(2).setText(userParamsList.get(i).waist);
                   tableWeek3.get(3).setText(userParamsList.get(i).hips);
                   tableWeek3.get(4).setText(userParamsList.get(i).feet);
                   tableWeek3.get(5).setText(userParamsList.get(i).hand);
                   break;
               case 3: tableWeek4.get(0).setText(userParamsList.get(i).weight);
                   tableWeek4.get(1).setText(userParamsList.get(i).chest);
                   tableWeek4.get(2).setText(userParamsList.get(i).waist);
                   tableWeek4.get(3).setText(userParamsList.get(i).hips);
                   tableWeek4.get(4).setText(userParamsList.get(i).feet);
                   tableWeek4.get(5).setText(userParamsList.get(i).hand);
                   break;
               case 4: tableWeek5.get(0).setText(userParamsList.get(i).weight);
                   tableWeek5.get(1).setText(userParamsList.get(i).chest);
                   tableWeek5.get(2).setText(userParamsList.get(i).waist);
                   tableWeek5.get(3).setText(userParamsList.get(i).hips);
                   tableWeek5.get(4).setText(userParamsList.get(i).feet);
                   tableWeek5.get(5).setText(userParamsList.get(i).hand);
                   break;
           }
        }
    }

    // Создаём таблицу тестов
    public void createTestTable(int week,boolean isAvailable){

        // Создаём лист названий
        tableType.add(getResources().getString(R.string.reports_table_params_weight));
        tableType.add(getResources().getString(R.string.reports_table_params_breast));
        tableType.add(getResources().getString(R.string.reports_table_params_waist));
        tableType.add(getResources().getString(R.string.reports_table_params_hips));
        tableType.add(getResources().getString(R.string.reports_table_params_legs));
        tableType.add(getResources().getString(R.string.reports_table_params_hand));

        for (int i=0;i<tableType.size();i++) {
            View tableTest = View.inflate(this, R.layout.item_reports_params, null);
            TextView rawName=(TextView)tableTest.findViewById(R.id.raw_name);
            EditText rawText1=(EditText)tableTest.findViewById(R.id.raw_text1);
            rawText1.setEnabled(false);
            EditText rawText2=(EditText)tableTest.findViewById(R.id.raw_text2);
            rawText2.setEnabled(false);
            EditText rawText3=(EditText)tableTest.findViewById(R.id.raw_text3);
            rawText3.setEnabled(false);
            EditText rawText4=(EditText)tableTest.findViewById(R.id.raw_text4);
            rawText4.setEnabled(false);
            EditText rawText5=(EditText)tableTest.findViewById(R.id.raw_text5);
            rawText5.setEnabled(false);

            rawName.setText(tableType.get(i));

            optimization.OptimizationLinear((LinearLayout)tableTest);

            linearTable.addView(tableTest);

            // Добавляем поля для в вода в нужные массивы
            tableWeek1.add(rawText1);
            tableWeek2.add(rawText2);
            tableWeek3.add(rawText3);
            tableWeek4.add(rawText4);
            tableWeek5.add(rawText5);

        }

        // Определяем активную неделю
        if(isAvailable){
            ArrayList<EditText> currentWeekList=new ArrayList();
            switch (week){
                case 1: currentWeekList=tableWeek1;
                    break;
                case 2: currentWeekList=tableWeek2;
                    break;
                case 3: currentWeekList=tableWeek3;
                    break;
                case 4: currentWeekList=tableWeek4;
                    break;
                case 5: currentWeekList=tableWeek5;
                    break;

            }
            for (int i=0;i<currentWeekList.size();i++){
                currentWeekList.get(i).setEnabled(true);
            }
        }
    }

    // Отправляем на сервер параметры нужной недели
    public void sendParamsClick(View v){
        ArrayList<EditText> currentTableWeek=new ArrayList();

        switch (currentParamsWeek){
            case 1: currentTableWeek=tableWeek1;
                break;
            case 2: currentTableWeek=tableWeek2;
                break;
            case 3: currentTableWeek=tableWeek3;
                break;
            case 4: currentTableWeek=tableWeek4;
                break;
            case 5: currentTableWeek=tableWeek5;
                break;
        }

        // Проверка на заполнение всех полей
        for (int i=0;i<currentTableWeek.size();i++){
            EditText editText=currentTableWeek.get(i);
            if(editText.getText().toString().equals("")){
                createToast(getResources().getString(R.string.reports_send_error));
                return;
            }
        }

        // Отправляем на сервер параметры юзера на текущую неделю
        sendUserParams(currentTableWeek);
    }

    // Отправляем параметры юзера на сервер
    public void sendUserParams(ArrayList<EditText> currentTableWeek){
        Call<UserParamsObject> postStatsSuccess =
                userPostParams.postUserParams(token,
                        Integer.valueOf(currentTableWeek.get(0).getText().toString()),
                        Integer.valueOf(currentTableWeek.get(1).getText().toString()),
                        Integer.valueOf(currentTableWeek.get(2).getText().toString()),
                        Integer.valueOf(currentTableWeek.get(3).getText().toString()),
                        Integer.valueOf(currentTableWeek.get(4).getText().toString()),
                        Integer.valueOf(currentTableWeek.get(5).getText().toString()));

        postStatsSuccess.enqueue(new Callback<UserParamsObject>() {
            @Override
            public void onResponse(Call<UserParamsObject> call, Response<UserParamsObject> response) {

                Log.d("ReportTablePost", String.valueOf(response.code()));
                if(response.code()==200){
                    createToast(getResources().getString(R.string.reports_table_params_success));
                    btnSendStats.setVisibility(View.GONE);
                }

            }
            @Override
            public void onFailure(Call<UserParamsObject> call, Throwable t) {

            }
        });

    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}