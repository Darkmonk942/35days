package com.days35.days35app;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.days35.days35app.adapters.CommentsAdapter;
import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.objects.CommentObject;
import com.days35.days35app.objects.PostListObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.CreateComment;
import com.days35.days35app.retrofit.GetComments;
import com.days35.days35app.retrofit.LikePost;
import com.days35.days35app.retrofit.PostSharing;
import com.days35.days35app.sharing.FacebookSharing;
import com.days35.days35app.sharing.InstagramSharing;
import com.days35.days35app.sharing.VkSharing;
import com.days35.days35app.transform.RoundImageTransform;
import com.facebook.CallbackManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerSimple;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 17.10.2016.
 */

public class FullPost extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.inner_frame) FrameLayout innerFrame;
    @BindView(R.id.create_comment_frame) FrameLayout commentsFrame;
    @BindView(R.id.frame_bottom) FrameLayout bottomFrame;
    // LinearLayout
    @BindView(R.id.linear_content) LinearLayout linearContent;
    // ScrollView
    @BindView(R.id.scroll_view) ScrollView scrollView;
    // RecyclerView
    @BindView(R.id.list_comments) RecyclerView recyclerComments;
    // TextView
    @BindView(R.id.text_user_name) TextView userName;
    @BindView(R.id.text_date_post) TextView textDate;
    @BindView(R.id.post_text_inner) TextView postText;
    @BindView(R.id.like_count) TextView likeCount;
    @BindView(R.id.message_count) TextView messageCountText;
    @BindView(R.id.share_count) TextView shareCountText;
    // ImageView
    @BindView(R.id.ic_user) ImageView userIcon;
    @BindView(R.id.post_image) ImageView postImage;
    @BindView(R.id.ic_like) ImageView likeIcon;
    // EditText
    @BindView(R.id.comment_field) EditText commentField;
    // VideoPlayer
    @BindView(R.id.video_player) JCVideoPlayerSimple videoPlayer;
    // Примитивы
    CallbackManager callbackManager;
    Retrofit retrofit;
    AuthorizationObject userProfile;
    EndlessRecyclerViewScrollListener scrollListener;
    CreateComment createComment;
    GetComments getComments;
    LikePost likePost;
    PostSharing postSharing;
    PostListObject postListObject;
    CommentsAdapter adapter;
    String token;
    int currentPost=0;
    public static int currentPageCount=1;
    public static int totalPageCount=0;
    int tenPxHeight=0;
    boolean isLiked;
    ArrayList<CommentObject> allCommentsList=new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_full);
        ButterKnife.bind(this);

        currentPageCount=1;
        totalPageCount=0;

        // Оптимизация интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);
        optimization.Optimization(innerFrame);
        optimization.OptimizationLinear(linearContent);
        int height=Paper.book().read("height");
        tenPxHeight=height/108;

        // Получаем текущего пользователя
        userProfile=Paper.book("user").read("userInfo",new AuthorizationObject());

        // Получаем токен
        token="Bearer "+ Paper.book().read("token");

        // Колбек от Фесбук
        callbackManager = CallbackManager.Factory.create();

        // Подключаем ретрофит и формируем запросы
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        createComment = retrofit.create(CreateComment.class);
        getComments = retrofit.create(GetComments.class);
        likePost=retrofit.create(LikePost.class);
        postSharing=retrofit.create(PostSharing.class);

        // Получаем нужный пост
        currentPost=Integer.parseInt(getIntent().getStringExtra("post"));
        postListObject= NewsFeedActivity.allNewsList.get(currentPost);

        // Меняем кол-во лайков, коментариев и шарингов
        likeCount.setText(String.valueOf(postListObject.likeCount));
        messageCountText.setText(String.valueOf(postListObject.commentCount));
        shareCountText.setText(String.valueOf(postListObject.shareCount));

        // Определяем лайкал ли пользователь пост
        isLiked=postListObject.isLiked;
        if(isLiked){
           likeIcon.setImageResource(R.drawable.ic_like_red);
        }

        // Подгружаем иконку
        Glide.with(this)
                .load(postListObject.userPost.userPic)
                .fitCenter()
                .transform(new RoundImageTransform(this))
                .into(userIcon);

        // Получаем остальную информацию
        userName.setText(postListObject.userPost.firstName+" "+postListObject.userPost.lastName);
        textDate.setText(postListObject.date);

        // В зависимости от типа поста подгружаем нужную инфу
        switch (postListObject.postType){
            case 1:
                postText.setVisibility(View.VISIBLE);
                postText.setText(postListObject.postText);
                videoPlayer.invalidate();
                break;
            case 2:
                postImage.setVisibility(View.VISIBLE);
                videoPlayer.invalidate();
                Glide.with(this)
                        .load(postListObject.postPic)
                        .fitCenter()
                        .into(postImage);
                break;
            case 3:
                videoPlayer.setVisibility(View.VISIBLE);
                videoPlayer.setUp(postListObject.postVideo
                        , JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL);
                break;
        }

        // Создаём адаптер и подгружаем комментарии
        createAdapter();
        scrollView.setScrollY(0);

        // Получаем комментарии
        getAllComments(1);

    }

    // Кнопка назад (в ленту постов)
    public void backClick(View v){
        finish();

    }

    // Переход на экран информации о пользователе создавшем пост
    public void userProfileClick(View v){
        startActivity(new Intent(this,UserProfileActivity.class)
                .putExtra("currentUser",false)
                .putExtra("userId",postListObject.userPost.userId));
    }

    // Открываем поле для комментариев
    public void openCommentField(View v){
        if(commentsFrame.getVisibility()== View.GONE){
            commentsFrame.setVisibility(View.VISIBLE);
        }else{
            commentsFrame.setVisibility(View.GONE);
        }
    }

    // Получаем комментарии с сервера
    public void getAllComments(final int page){

        Call<List<CommentObject>> getCommentsSuccess = getComments.getComments(token,
                String.valueOf(postListObject.id),
                page,
                20);

        getCommentsSuccess.enqueue(new Callback<List<CommentObject>>() {
            @Override
            public void onResponse(Call<List<CommentObject>> call, Response<List<CommentObject>> response) {

                if(response.code()==200){
                    currentPageCount=page;
                    if(page==1){
                        scrollListener.resetState();
                    }
                    Log.d("Гуд","Коменты");
                    for (int i=0;i<response.body().size();i++){
                        CommentObject commentObject=response.body().get(i);
                        allCommentsList.add(commentObject);
                    }

                    // Узнаём окончание постов
                    totalPageCount = Integer.valueOf(response.headers().get("X-Pagination-Page-Count"));
                    Log.d("Текущее количество", String.valueOf(totalPageCount));

                    // Обновляем адаптер
                    adapter.notifyDataSetChanged();

                }else{
                    createToast(getResources().getString(R.string.error_common));
                }

            }
            @Override
            public void onFailure(Call<List<CommentObject>> call, Throwable t) {

            }
        });
    }


    // Добавляем комментарий на сервер
    public void addCommentClick(View v){

        String commentText=commentField.getText().toString();
        commentsFrame.setVisibility(View.GONE);
        Log.d("Гуд", String.valueOf(postListObject.id));

        Call<Void> getCreatePostSuccess = createComment.sendComment(token,
                postListObject.id,commentText);

        getCreatePostSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if(response.code()==200){
                    Log.d("Гуд","Гуд");
                    commentField.setText("");
                    // Обновляем комменты
                    allCommentsList.clear();
                    currentPageCount=1;
                    totalPageCount=0;
                    getAllComments(1);
                    // Меняем кол-во комментариев
                    int commentCount= Integer.parseInt(messageCountText.getText().toString())+1;
                    messageCountText.setText(String.valueOf(commentCount));
                    NewsFeedActivity.allNewsList.get(currentPost).commentCount=commentCount;
                }else{
                    createToast(getResources().getString(R.string.error_common));
                }

            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    // Обработка лайка текущего поста
    public void addLikeToPost(View v){
        Call<Void> getLikeSuccess = likePost.sendLike(token, String.valueOf(postListObject.id));

        getLikeSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("лайк", String.valueOf(response.code()));

                if(response.code()==200){
                    // Узнаём текущее кол-во лайков
                    int likes=Integer.valueOf(likeCount.getText().toString());
                    if(isLiked){
                        likeIcon.setImageResource(R.drawable.ic_post_like);
                        likeCount.setText(String.valueOf(likes-1));
                        isLiked=false;
                        likeCount.setTextColor(getResources().getColor(R.color.colorGreyPostText));
                    }else{
                        likeIcon.setImageResource(R.drawable.ic_like_red);
                        likeCount.setText(String.valueOf(likes+1));
                        isLiked=true;
                        likeCount.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }

                    // Меняем кол-во лайков и цвет иконки лайка
                    NewsFeedActivity.allNewsList.get(currentPost).isLiked=isLiked;
                    NewsFeedActivity.allNewsList.get(currentPost).likeCount=Integer.valueOf(likeCount.getText().toString());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    // Обработка шаринга текущего поста (выбор соцсети)
    public void sharingPost(View v){

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.frame_alert_sharing);
        dialog.setTitle("35 Days");

        FrameLayout instagramFrame=(FrameLayout)dialog.findViewById(R.id.frame_alert_instagram);
        if(postListObject.postType==1){
            instagramFrame.setVisibility(View.GONE);
        }

        dialog.show();
    }

    // Клик в Диалоге при выборе соц-сети для шаринга
    public void alertShareClick(View v){
        switch (v.getId()){
            case R.id.text_alert_vk:
                shareCurrentPost("vk");
                break;
            case R.id.text_alert_fb:
                shareCurrentPost("facebook");
                break;
            case R.id.text_alert_instagram:
                shareCurrentPost("instagram");
                break;
        }
    }

    // Отправляем расшаренный материал, в зависимости от соцсети и типа поста
    public void shareCurrentPost(String type){
        switch (type){
            case "vk":
                VkSharing vkSharing=new VkSharing();
                FragmentManager fragmentManager = getFragmentManager();

                switch (postListObject.postType){
                    case 1: vkSharing.vkTextShare(token,postListObject.id,this,
                            fragmentManager,1,"","35 Дней",postSharing,shareCountText);
                        break;
                    case 2:
                        vkSharing.vkTextShare(token,postListObject.id,this,
                                fragmentManager,2,postListObject.postPic,"35 Дней",postSharing,shareCountText);
                        break;
                    case 3: vkSharing.vkTextShare(token,postListObject.id,this,
                            fragmentManager,3,postListObject.postVideo,"35 Дней",postSharing,shareCountText);
                        break;
                }
                break;
            case "facebook":
                FacebookSharing facebookSharing=new FacebookSharing();

                switch (postListObject.postType) {
                    case 1:
                        facebookSharing.facebookSharing(this,callbackManager,token,postListObject.id,"",
                                postSharing,shareCountText,1);
                        break;
                    case 2:
                        facebookSharing.facebookSharing(this,callbackManager,token,postListObject.id,postListObject.postPic,
                                postSharing,shareCountText,2);
                        break;
                    case 3:
                        facebookSharing.facebookSharing(this,callbackManager,token,postListObject.id,postListObject.postVideo,
                                postSharing,shareCountText,3);
                        break;
                }
                break;
            case "instagram":

                InstagramSharing instagramSharing=new InstagramSharing();
                switch (postListObject.postType) {
                    case 2:
                        instagramSharing.startSharing(this,callbackManager,token,postListObject.id,postListObject.postPic,
                                postSharing,shareCountText,2);
                        break;
                    case 3:
                        instagramSharing.startSharing(this,callbackManager,token,postListObject.id,postListObject.postVideo,
                                postSharing,shareCountText,3);
                        break;

                }

                break;
        }

    }

    // Создаём адаптер для подгрузки комментариев
    public void createAdapter(){

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerComments.setLayoutManager(llm);
        adapter = new CommentsAdapter(allCommentsList, this,userProfile.id,
                retrofit,token,messageCountText,currentPost);
        recyclerComments.setAdapter(adapter);

        scrollListener = new EndlessRecyclerViewScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.d("Текущая страница", String.valueOf(page));

                if(currentPageCount!=page+1&&page+1<=totalPageCount) {
                    getAllComments(page+1);
                }
            }
        };
        recyclerComments.addOnScrollListener(scrollListener);

    }


    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
