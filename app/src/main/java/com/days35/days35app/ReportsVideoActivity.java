package com.days35.days35app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.days35.days35app.objects.reports.ReportVideoBodyObject;
import com.days35.days35app.objects.reports.ReportVideoObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.CreateVideoReport;
import com.days35.days35app.retrofit.GetReportsVideo;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.VideoPicker;
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenVideo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerSimple;
import io.paperdb.Paper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 29.11.2016.
 */

public class ReportsVideoActivity extends AppCompatActivity implements VideoPickerCallback {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.frame_video_post1) FrameLayout videoFrame1;
    @BindView(R.id.frame_video_post2) FrameLayout videoFrame2;
    @BindView(R.id.frame_video_post3) FrameLayout videoFrame3;
    @BindView(R.id.frame_video_post4) FrameLayout videoFrame4;
    // TextView
    @BindView(R.id.text_week1) TextView textWeek1;
    @BindView(R.id.text_week2) TextView textWeek2;
    @BindView(R.id.text_week3) TextView textWeek3;
    @BindView(R.id.text_week4) TextView textWeek4;
    // ImageView
    @BindView(R.id.ic_add_video1) ImageView addVideoIcon1;
    @BindView(R.id.ic_add_video2) ImageView addVideoIcon2;
    @BindView(R.id.ic_add_video3) ImageView addVideoIcon3;
    @BindView(R.id.ic_add_video4) ImageView addVideoIcon4;
    @BindView(R.id.ic_thumb1) ImageView icThumb1;
    @BindView(R.id.ic_thumb2) ImageView icThumb2;
    @BindView(R.id.ic_thumb3) ImageView icThumb3;
    @BindView(R.id.ic_thumb4) ImageView icThumb4;
    @BindView(R.id.ic_play1) ImageView icPlay1;
    @BindView(R.id.ic_play2) ImageView icPlay2;
    @BindView(R.id.ic_play3) ImageView icPlay3;
    @BindView(R.id.ic_play4) ImageView icPlay4;
    // VideoView
    @BindView(R.id.video_player1) JCVideoPlayerSimple videoView1;
    @BindView(R.id.video_player2) JCVideoPlayerSimple videoView2;
    @BindView(R.id.video_player3) JCVideoPlayerSimple videoView3;
    @BindView(R.id.video_player4) JCVideoPlayerSimple videoView4;
    // Button
    @BindView(R.id.btn_send) Button btnSend;
    // Примитивы
    CreateVideoReport createVideoReport;
    GetReportsVideo getReportsVideo;
    ProgressDialog alertDialog;
    String token;
    String videoPath1="";
    String videoPath2="";
    String videoPath3="";
    String videoPath4="";
    VideoPicker videoPicker;
    int currentWeek=1;
    int currentTrainWeek=0;
    boolean isUploadAvailable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports_video);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        videoPicker = new VideoPicker(this);
        videoPicker.shouldGenerateMetadata(true);
        videoPicker.shouldGeneratePreviewImages(true);
        videoPicker.setVideoPickerCallback(this);

        // Получаем Токен
        token = "Bearer " + Paper.book().read("token");

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Формируем запрос на добавление видео отчёта на сервер
        createVideoReport=retrofit.create(CreateVideoReport.class);

        // Формируем запрос на получение всех видео отчётов с сервера
        getReportsVideo=retrofit.create(GetReportsVideo.class);

        // Отправляем запрос на получение видео отчётов
        getAllVideoReports();
    }

    // Переход на предыдущий экран
    public void backClick(View v){
        finish();
    }

    // Клики по неделям
    public void weekClick(View v){

        // Убираем функционал при клике на уже выделенный элемент
        if(currentWeek==Integer.valueOf(v.getTag().toString())){
            return;
        }

        // Снимаем выделение со всех недель
        textWeek1.setBackgroundResource(R.color.colorGreyPost);
        textWeek1.setTextColor(getResources().getColor(R.color.colorDarkGrey));
        textWeek2.setBackgroundResource(R.color.colorGreyPost);
        textWeek2.setTextColor(getResources().getColor(R.color.colorDarkGrey));
        textWeek3.setBackgroundResource(R.color.colorGreyPost);
        textWeek3.setTextColor(getResources().getColor(R.color.colorDarkGrey));
        textWeek4.setBackgroundResource(R.color.colorGreyPost);
        textWeek4.setTextColor(getResources().getColor(R.color.colorDarkGrey));

        // Выделяем выбранную неделю
        TextView text=(TextView)v;
        text.setBackgroundResource(R.color.colorPrimary);
        text.setTextColor(getResources().getColor(R.color.white));

        // Сохраняем выбранную неделю
        currentWeek=Integer.valueOf(v.getTag().toString());

        // Проверяем возможность загрузки
        if(isUploadAvailable){
            if(currentTrainWeek==currentWeek){
                btnSend.setVisibility(View.VISIBLE);
            }else {
                btnSend.setVisibility(View.GONE);
            }
        }else{
            btnSend.setVisibility(View.GONE);
        }
        if(currentWeek==1){
            btnSend.setVisibility(View.VISIBLE);
        }

        // Меняем видимость нужного видео фрейма
        videoFrame1.setVisibility(View.GONE);
        videoFrame2.setVisibility(View.GONE);
        videoFrame3.setVisibility(View.GONE);
        videoFrame4.setVisibility(View.GONE);

        switch (text.getTag().toString()){
            case "1": videoFrame1.setVisibility(View.VISIBLE);
                break;
            case "2": videoFrame2.setVisibility(View.VISIBLE);
                break;
            case "3": videoFrame3.setVisibility(View.VISIBLE);
                break;
            case "4": videoFrame4.setVisibility(View.VISIBLE);
                break;
        }
    }

    // Получаем весь список видео с сервера
    public void getAllVideoReports(){

        Call<ReportVideoObject> getReportsSuccess = getReportsVideo.getVideoReports(token);

        getReportsSuccess.enqueue(new Callback<ReportVideoObject>() {
            @Override
            public void onResponse(Call<ReportVideoObject> call, Response<ReportVideoObject> response) {
                Log.d("ВидеоОтчёты", String.valueOf(response.code()));
                if(response.code()==200){
                    // Подгружаем все данные по полученным видео на фреймы
                    handleVideoFromDB(response.body());

                    // текущая серверная неделя
                    currentTrainWeek=response.body().currentWeek;

                    // Возможность загрузки видео на сервер
                    isUploadAvailable=response.body().isAvailable;

                    Log.d("ВидеоОтчёты",String.valueOf(isUploadAvailable)+" " +String.valueOf(currentTrainWeek));

                    if(isUploadAvailable){
                        if(currentTrainWeek==1){
                            btnSend.setVisibility(View.VISIBLE);
                        }
                    }
                    btnSend.setVisibility(View.VISIBLE);
                }

            }
            @Override
            public void onFailure(Call<ReportVideoObject> call, Throwable t) {

            }
        });
    }

    // Распределяем все видео по фреймам
    public void handleVideoFromDB(ReportVideoObject reportVideoObject){
        ArrayList<ReportVideoBodyObject> allVideos=reportVideoObject.allVideos;
        btnSend.setClickable(true);
        for (int i=0;i<allVideos.size();i++){
            switch (i){
                case 0 : //icPlay1.setVisibility(View.VISIBLE);
                    videoView1.setVisibility(View.VISIBLE);
                    addVideoIcon1.setVisibility(View.GONE);
                    videoPath1=allVideos.get(i).reportVideoLink;
                    videoFrame1.setBackgroundColor(getResources().getColor(R.color.colorBlack));
                    break;
                case 1 : //icPlay2.setVisibility(View.VISIBLE);
                    videoView2.setVisibility(View.VISIBLE);
                    addVideoIcon2.setVisibility(View.GONE);
                    videoPath2=allVideos.get(i).reportVideoLink;
                    videoFrame2.setBackgroundColor(getResources().getColor(R.color.colorBlack));
                    break;
                case 2 : //icPlay3.setVisibility(View.VISIBLE);
                    videoView3.setVisibility(View.VISIBLE);
                    addVideoIcon3.setVisibility(View.GONE);
                    videoPath3=allVideos.get(i).reportVideoLink;
                    videoFrame3.setBackgroundColor(getResources().getColor(R.color.colorBlack));
                    break;
                case 3 : //icPlay4.setVisibility(View.VISIBLE);
                    videoView4.setVisibility(View.VISIBLE);
                    addVideoIcon4.setVisibility(View.GONE);
                    videoPath4=allVideos.get(i).reportVideoLink;
                    videoFrame4.setBackgroundColor(getResources().getColor(R.color.colorBlack));
                    break;

            }
        }

    }

    // Клик на фрейм для загрузки видео
    public void addVideo(View v){
        videoPicker.pickVideo();
    }

    // Отправляем видео на сервер
    public void sendVideo(View v){

        File videoFile=null;
        File picFile=null;
        String currentVideoPath="";

        switch (currentWeek){
            case 1: currentVideoPath=videoPath1;
                break;
            case 2: currentVideoPath=videoPath2;
                break;
            case 3: currentVideoPath=videoPath3;
                break;
            case 4: currentVideoPath=videoPath4;
                break;
        }

        if(currentVideoPath.equals("")){
            createToast(getResources().getString(R.string.error_reports_video));
            return;
        }

        videoFile=new File(currentVideoPath);

        RequestBody requestVideoFile = RequestBody.create(MediaType.parse("multipart/form-data"), videoFile);

        // Формируем тело для файлы
        MultipartBody.Part bodyVideo =
                MultipartBody.Part.createFormData("report_vid", videoFile.getName(), requestVideoFile);

        createProgressAlert();
        btnSend.setVisibility(View.GONE);
        Call<Void> sendVideoPost = createVideoReport.sendVideoReport(token,
                bodyVideo,
                "текст"
        );

        sendVideoPost.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                alertDialog.dismiss();

                if(response.code()==200){
                    createToast(getResources().getString(R.string.reports_task_success));

                }else{
                    btnSend.setVisibility(View.VISIBLE);
                    createToast(getResources().getString(R.string.error_common));
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("ВидеоОшибка1",t.toString());
                btnSend.setVisibility(View.VISIBLE);
                alertDialog.dismiss();
            }
        });
    }

    // Стартуем выбранное видео
    public void startVideoClick(View v){
        switch (v.getId()){
            case R.id.ic_play1:
                videoView1.setUp
                        (videoPath1, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                icPlay1.setVisibility(View.GONE);
                icThumb1.setVisibility(View.GONE);
                break;
            case R.id.ic_play2:
                videoView2.setUp
                        (videoPath2, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                icPlay2.setVisibility(View.GONE);
                icThumb2.setVisibility(View.GONE);
                break;
            case R.id.ic_play3:
                videoView3.setUp
                        (videoPath3, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                icPlay3.setVisibility(View.GONE);
                icThumb3.setVisibility(View.GONE);
                break;
            case R.id.ic_play4:
                videoView4.setUp
                        (videoPath4, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                icPlay4.setVisibility(View.GONE);
                icThumb4.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_VIDEO_DEVICE) {
                if (videoPicker == null) {
                    videoPicker = new VideoPicker(this);
                    videoPicker.setVideoPickerCallback(this);
                }
                videoPicker.submit(data);
            }
        }
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onVideosChosen(List<ChosenVideo> list) {

        switch (currentWeek){
            case 1:
                videoFrame1.setBackgroundColor(getResources().getColor(R.color.colorBlack));
                addVideoIcon1.setVisibility(View.GONE);
                videoPath1=list.get(0).getOriginalPath();
                videoView1.setVisibility(View.VISIBLE);
                videoView1.setUp
                        (videoPath1, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                break;
            case 2:
                videoFrame2.setBackgroundColor(getResources().getColor(R.color.colorBlack));
                addVideoIcon2.setVisibility(View.GONE);
                addVideoIcon2.setClickable(false);
                videoPath2=list.get(0).getOriginalPath();
                videoView2.setVisibility(View.VISIBLE);
                videoView2.setUp
                        (videoPath2, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                break;
            case 3:
                videoFrame3.setBackgroundColor(getResources().getColor(R.color.colorBlack));
                addVideoIcon3.setVisibility(View.GONE);
                addVideoIcon3.setClickable(false);
                videoPath3=list.get(0).getOriginalPath();
                videoView3.setVisibility(View.VISIBLE);
                videoView3.setUp
                        (videoPath3, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                break;
            case 4:
                videoFrame4.setBackgroundColor(getResources().getColor(R.color.colorBlack));
                addVideoIcon4.setVisibility(View.GONE);
                addVideoIcon4.setClickable(false);
                videoPath4=list.get(0).getOriginalPath();
                videoView4.setVisibility(View.VISIBLE);
                videoView4.setUp
                        (videoPath4, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }

    public void createProgressAlert(){
        alertDialog = ProgressDialog.show(this, "",
                "Идёт загрузка...", true);
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String s) {

    }
}
