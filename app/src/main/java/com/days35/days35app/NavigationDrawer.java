package com.days35.days35app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.retrofit.LogoutRequest;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DarKMonK on 13.10.2016.
 */

public class NavigationDrawer {
    PrimaryDrawerItem item1,item2,item3,item4,item5,item6,item7,item8,item9;
    Context context;
    Drawer result;

    public void NavigationDrawer(final Context context, Toolbar toolbar,int position){
        final AuthorizationObject userProfile= Paper.book("user").read("userInfo");
        this.context=context;

        // Создаём хедер для дроера
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity((Activity) context)
                .withHeaderBackground(R.drawable.bg_profile_gray)
                .withSelectionListEnabledForSingleProfile(false)
                .addProfiles(
                        new ProfileDrawerItem()
                                .withName(userProfile.firstName +" "+userProfile.lastName)
                                .withEmail(userProfile.email)
                                .withIcon(userProfile.userPic)
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        item1 =
                new PrimaryDrawerItem().withIdentifier(1).withName(R.string.main_screen_drawer).withIcon(R.drawable.ic_drawer_main_screen);
        item2 =
                new PrimaryDrawerItem().withIdentifier(2).withName(R.string.user_profile_drawer).withIcon(R.drawable.ic_drawer_profile);
        item3 =
                new PrimaryDrawerItem().withIdentifier(3).withName(R.string.trains_drawer).withIcon(R.drawable.ic_drawer_trains);
        item4 =
                new PrimaryDrawerItem().withIdentifier(4).withName(R.string.food_drawer).withIcon(R.drawable.ic_drawer_food);
        item5 =
                new PrimaryDrawerItem().withIdentifier(5).withName(R.string.reports_drawer).withIcon(R.drawable.ic_drawer_report);
        item6 =
                new PrimaryDrawerItem().withIdentifier(6).withName(R.string.voting_drawer).withIcon(R.drawable.ic_drawer_voting);
        item7 =
                new PrimaryDrawerItem().withIdentifier(7).withName(R.string.bonus_drawer).withIcon(R.drawable.ic_drawer_bonus);
        item8 =
                new PrimaryDrawerItem().withIdentifier(8).withName(R.string.settings_drawer).withIcon(R.drawable.ic_drawer_settings);
        item9 =
                new PrimaryDrawerItem().withIdentifier(9).withName(R.string.exit_drawer).withIcon(R.drawable.ic_drawer_exit);

        result = new DrawerBuilder()
                .withActivity((Activity)context)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                //.withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        item2,
                        item3,
                        item4,
                        item5,
                        item6,
                        item7,
                        item8,
                        item9
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        switch (position){
                            case 1: // Главный экран
                                if(!((Activity)context).getLocalClassName().equals("NewsFeedActivity")) {
                                    context.startActivity(new Intent(context, NewsFeedActivity.class));
                                    ((Activity) context).finish();
                                }else{
                                    result.closeDrawer();
                                }
                                break;
                            case 2: // Профиль пользователя
                                if(!((Activity)context).getLocalClassName().equals("UserProfileActivity")) {
                                    context.startActivity(new Intent(context, UserProfileActivity.class)
                                    .putExtra("currentUser",true));
                                    ((Activity) context).finish();
                                }else{
                                    result.closeDrawer();
                                }
                                break;
                            case 3: // Тренировки
                                if(!((Activity)context).getLocalClassName().equals("TrainListActivity")) {
                                    context.startActivity(new Intent(context, TrainListActivity.class));
                                    ((Activity) context).finish();
                                }else{
                                    result.closeDrawer();
                                }
                                break;
                            case 4: // Питание
                                if(!((Activity)context).getLocalClassName().equals("FoodListActivity")) {
                                    context.startActivity(new Intent(context, FoodListActivity.class));
                                    ((Activity) context).finish();
                                }else{
                                    result.closeDrawer();
                                }
                                break;
                            case 5: // Отчёты
                                if(!((Activity)context).getLocalClassName().equals("ReportsActivity")) {
                                    context.startActivity(new Intent(context, ReportsActivity.class));
                                    ((Activity) context).finish();
                                }else{
                                    result.closeDrawer();
                                }
                                break;
                            case 6: // Голосование
                                if(!((Activity)context).getLocalClassName().equals("VotingMainActivity")) {
                                    context.startActivity(new Intent(context, VotingMainActivity.class));
                                    ((Activity) context).finish();
                                }else{
                                    result.closeDrawer();
                                }
                                break;
                            case 7: // Бонусы
                                if(!((Activity)context).getLocalClassName().equals("BonusMainActivity")) {
                                    context.startActivity(new Intent(context, BonusMainActivity.class));
                                    ((Activity) context).finish();
                                }else{
                                    result.closeDrawer();
                                }
                                break;
                            case 8: // Настройки
                                if(!((Activity)context).getLocalClassName().equals("SettingsActivity")) {
                                    context.startActivity(new Intent(context, SettingsActivity.class));
                                    ((Activity) context).finish();
                                }else{
                                    result.closeDrawer();
                                }
                                break;
                            case 9: // Выход
                                userLogout();
                                break;
                        }
                        return true;
                    }
                })
                .build();

            result.setSelection(position);
    }

    // Логаут текущего пользователя
    public void userLogout(){
        String token="Bearer "+Paper.book().read("token","");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LogoutRequest logoutRequest=retrofit.create(LogoutRequest.class);

        Call<Void> userLogoutRequest = logoutRequest.userLogout(token);

        userLogoutRequest.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if(response.code()==200){
                    // Сбрасываем токены
                    Paper.book().write("token", "");
                    Paper.book().write("vkToken", "");
                    Paper.book().write("fbToken", "");
                    Paper.book().write("autoAuth","");

                    // Сбрасываем все данные пользователя
                    Paper.book().write("emailSaved","");
                    Paper.book().write("passwordSaved","");

                    Paper.book("user").write("userInfo", new AuthorizationObject());

                    // Переходим на главный экран
                    context.startActivity(new Intent(context,MainActivity.class));
                    ((Activity)context).finish();
                }

            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });

    }
}
