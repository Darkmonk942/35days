package com.days35.days35app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.LinkagePager;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.UserAddPhotosBefore;
import com.iceteck.silicompressorr.SiliCompressor;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import me.crosswall.lib.coverflow.core.LinkageCoverTransformer;
import me.crosswall.lib.coverflow.core.LinkagePagerContainer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 26.09.2016.
 */

public class UserAddPhotoActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.frame_back_photo) FrameLayout backPhotoFrame;
    @BindView(R.id.frame_front_photo) FrameLayout frontPhotoFrame;
    @BindView(R.id.frame_front2_photo) FrameLayout front2PhotoFrame;
    @BindView(R.id.frame_left_photo) FrameLayout leftPhotoFrame;
    @BindView(R.id.frame_right_photo) FrameLayout rightPhotoFrame;
    @BindView(R.id.frame_front1_text1) FrameLayout front1Text1;
    @BindView(R.id.frame_front1_text2) FrameLayout front1Text2;
    @BindView(R.id.frame_front1_text3) FrameLayout front1Text3;
    @BindView(R.id.frame_front2_text1) FrameLayout front2Text1;
    @BindView(R.id.frame_front2_text2) FrameLayout front2Text2;
    @BindView(R.id.frame_front2_text3) FrameLayout front2Text3;
    @BindView(R.id.frame_back_text1) FrameLayout backText1;
    @BindView(R.id.frame_back_text2) FrameLayout backText2;
    @BindView(R.id.frame_back_text3) FrameLayout backText3;
    @BindView(R.id.frame_left_text1) FrameLayout leftText1;
    @BindView(R.id.frame_left_text2) FrameLayout leftText2;
    @BindView(R.id.frame_left_text3) FrameLayout leftText3;
    @BindView(R.id.frame_right_text1) FrameLayout rightText1;
    @BindView(R.id.frame_right_text2) FrameLayout rightText2;
    @BindView(R.id.frame_right_text3) FrameLayout rightText3;
    FrameLayout lastFrame=null;
    // Button
    @BindView(R.id.btn_done) Button btnDone;
    // ViewPager
    @BindView(R.id.pager_container) LinkagePagerContainer mContainer;
    //ImageView
    @BindView(R.id.img_front1) ImageView imgFront1;
    @BindView(R.id.ic_add_photo1) ImageView icAddPhoto1;
    @BindView(R.id.ic_add_photo2) ImageView icAddPhoto2;
    @BindView(R.id.ic_add_photo3) ImageView icAddPhoto3;
    @BindView(R.id.ic_add_photo4) ImageView icAddPhoto4;
    @BindView(R.id.ic_add_photo5) ImageView icAddPhoto5;
    ImageView currentImage=null;
    ImageView currentAddPhotoIcon=null;
    ImageView currentBottomImage=null;
    // Примитивы
    ProgressDialog alertDialog;
    int totalCompress=350;
    int height,width;
    Optimization optimization;
    int allPhotosSampleFemale[]={R.drawable.img_photoshape_front1,R.drawable.img_photoshape_front2,R.drawable.img_photoshape_back,
                            R.drawable.img_photoshape_left,R.drawable.img_photoshape_right};
    int allPhotosSampleMale[]={R.drawable.img_photoshape_front1_male,R.drawable.img_photoshape_front2_male,
            R.drawable.img_photoshape_back_male, R.drawable.img_photoshape_left_male,R.drawable.img_photoshape_right_male};
    String currentSex="male";
    LinkagePager pager;
    UserAddPhotosBefore userAddPhotosBefore;
    String token;
    int currentPhotoType;
    String photoFrontPath;
    String photoFront1Path;
    String photoBackPath;
    String photoLeftPath;
    String photoRightPath;
    public static final int REQUEST_CODE_PICKER=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_photo);
        ButterKnife.bind(this);

        // Стартовая позиция
        lastFrame=frontPhotoFrame;
        currentImage=imgFront1;
        currentAddPhotoIcon=icAddPhoto1;

        // Получаем токен из базы И узнаём пол
        token="Bearer "+Paper.book().read("token");

        AuthorizationObject authorizationObject=Paper.book("user").read("userInfo",new AuthorizationObject());
        if(authorizationObject.sex==2){
            currentSex="female";
        }

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Запрос на добавление всех фотографий пользователя
        userAddPhotosBefore = retrofit.create(UserAddPhotosBefore.class);

        // Крепим базу к адаптеру
        pager = mContainer.getViewPager();
        PagerAdapter adapter = new MyPagerAdapter();
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(allPhotosSampleFemale.length);
        pager.setClickable(false);

        final LinkagePager bindingPager = (LinkagePager) findViewById(R.id.pager);
        bindingPager.setAdapter(adapter);
        bindingPager.setOffscreenPageLimit(allPhotosSampleFemale.length);
        bindingPager.setLinkagePager(pager);


        pager.setPageTransformer(false,new LinkageCoverTransformer(0.15f,0f,0.2f,0f));

        bindingPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        });

        pager.addOnPageChangeListener(new LinkagePager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float v, int i1) {
                switch (position) {
                    case 0:
                        if(!lastFrame.equals(frontPhotoFrame)) {
                            lastFrame.setVisibility(View.GONE);
                            frontPhotoFrame.setVisibility(View.VISIBLE);
                            lastFrame = frontPhotoFrame;
                            currentAddPhotoIcon=icAddPhoto1;
                            currentPhotoType=0;
                        }
                        break;
                    case 1:
                        if(!lastFrame.equals(front2PhotoFrame)) {
                            lastFrame.setVisibility(View.GONE);
                            front2PhotoFrame.setVisibility(View.VISIBLE);
                            lastFrame = front2PhotoFrame;
                            currentAddPhotoIcon=icAddPhoto2;
                            currentPhotoType=1;
                        }
                        break;
                    case 2:
                        if(!lastFrame.equals(backPhotoFrame)) {
                            lastFrame.setVisibility(View.GONE);
                            backPhotoFrame.setVisibility(View.VISIBLE);
                            lastFrame = backPhotoFrame;
                            currentAddPhotoIcon=icAddPhoto3;
                            currentPhotoType=2;
                        }
                        break;
                    case 3:
                        if(!lastFrame.equals(leftPhotoFrame)) {
                            lastFrame.setVisibility(View.GONE);
                            leftPhotoFrame.setVisibility(View.VISIBLE);
                            lastFrame = leftPhotoFrame;
                            currentAddPhotoIcon=icAddPhoto4;
                            currentPhotoType=3;
                        }
                        break;
                    case 4:
                        if(!lastFrame.equals(rightPhotoFrame)) {
                            lastFrame.setVisibility(View.GONE);
                            rightPhotoFrame.setVisibility(View.VISIBLE);
                            lastFrame = rightPhotoFrame;
                            currentAddPhotoIcon=icAddPhoto5;
                            currentPhotoType=4;
                        }
                        break;
                }
            }

            @Override
            public void onPageSelected(int i) {
                Log.d("Листнер2", String.valueOf(i));
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        // Оптимизация элементов интерфейса
        optimization=new Optimization();
        optimization();
    }

    // Переход на предыдущий экран
    public void backClick(View v){
        // Обнуляем сохранённые данные
        // Сбрасываем токен
        Paper.book().write("token", "");
        // Сбрасываем все данные пользователя
        Paper.book().write("emailSaved","");
        Paper.book().write("passwordSaved","");

        Paper.book("user").delete("userInfo");
        Paper.book().delete("autoAuth");


        startActivity(new Intent(this,MainActivity.class));
    }

    // Пропустить текущий экран
    public void skipScreenClick(View v){
        startActivity(new Intent(this,NewsFeedActivity.class));
        finish();
    }

    // Добавление фотографии пользователем
    public void addBodyPhoto(View v){
        currentImage=(ImageView)v;

        ImagePicker.create(UserAddPhotoActivity.this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);

            // Уменьшаем вес фото

            // По тагу определяем с какого ракурса фото
            switch (currentPhotoType){
                case 0:
                    photoFrontPath=images.get(0).getPath();
                    break;
                case 1:
                    photoFront1Path=images.get(0).getPath();
                    break;
                case 2:
                    photoBackPath=images.get(0).getPath();
                    break;
                case 3:
                    photoLeftPath=images.get(0).getPath();
                    break;
                case 4:
                    photoRightPath=images.get(0).getPath();
                    break;
            }

            File file=null;
            try {
                file = new File(images.get(0).getPath());
                Log.d("Вес", String.valueOf(file.length()/1024));
            }catch (Exception e){}

            // Добавляем выбранное фото пользователя на верхнюю плашку
            Glide.with(getApplicationContext())
                    .load(images.get(0).getPath())
                    .bitmapTransform(new RoundedCornersTransformation(this,30,20))
                    .into(currentImage);

            currentAddPhotoIcon.setVisibility(View.GONE);

            // Добавление фото на нижнюю плашку
            FrameLayout frame=(FrameLayout) pager.getChildAt(pager.getCurrentItem());
            FrameLayout innerFrame=(FrameLayout)frame.getChildAt(0);
            ImageView image=(ImageView)innerFrame.getChildAt(0);
            ImageView imageSample=(ImageView)innerFrame.getChildAt(1);
            imageSample.setVisibility(View.GONE);

            Glide.with(getApplicationContext())
                    .load(images.get(0).getPath())
                    .bitmapTransform(new RoundedCornersTransformation(this,30,20))
                    .into(image);


            // Проверка на добавление всех фоток
            if(photoFront1Path!=null&photoFrontPath!=null&photoBackPath!=null&photoLeftPath!=null&photoRightPath!=null){
                btnDone.setVisibility(View.VISIBLE);
            }
        }
    }

    // Отправляем все фото на сервер
    public void uploadPhotoToServer(View v){
        // Уменьшаем размер всех фотографий
        String allPhotoPath[]={photoFrontPath,photoFront1Path,photoBackPath,photoLeftPath,photoRightPath};

        for (int i=0;i<allPhotoPath.length;i++) {
            compressAllPhoto(allPhotoPath[i],i);
        }

        // Получаем изображения, которые добавил пользователь
        File fileImageFront = new File(photoFrontPath);
        File fileImageFront1 = new File(photoFront1Path);
        File fileImageBack=new File(photoBackPath);
        File fileImageLeft=new File(photoLeftPath);
        File fileImageRight=new File(photoRightPath);

        // Добавляем файлы в реквест
        RequestBody requestFileFront = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageFront);
        RequestBody requestFileFront1 = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageFront1);
        RequestBody requestFileBack = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageBack);
        RequestBody requestFileLeft = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageLeft);
        RequestBody requestFileRight = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageRight);

        // Формируем тело для каждого файла
        MultipartBody.Part bodyFront =
                MultipartBody.Part.createFormData("front1", fileImageFront.getName(), requestFileFront);
        MultipartBody.Part bodyFront1 =
                MultipartBody.Part.createFormData("front2", fileImageFront1.getName(), requestFileFront1);
        MultipartBody.Part bodyBack =
                MultipartBody.Part.createFormData("back", fileImageBack.getName(), requestFileBack);
        MultipartBody.Part bodyLeft =
                MultipartBody.Part.createFormData("left", fileImageLeft.getName(), requestFileLeft);
        MultipartBody.Part bodyRight =
                MultipartBody.Part.createFormData("right", fileImageRight.getName(), requestFileRight);

        // Прогресс бар отправки информации
        createProgressAlert(false);

        // Отправляем файл на сервер
        Call<Void> resultImageUpload = userAddPhotosBefore.uploadImage(token,
                                        bodyFront,bodyFront1,bodyBack,bodyLeft,bodyRight);

        resultImageUpload.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("Запрос1", String.valueOf(response.code()));

                createProgressAlert(true);
                if(response.code()==200){
                    startActivity(new Intent(UserAddPhotoActivity.this,MainActivity.class));
                    Log.d("Картинка", "Всё залито");
                    finish();
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                createToast("Произошла ошибка");
                Log.d("КартинкаОшибка1",t.toString());
            }
        });
    }

    // Ужимаем все фотографии до нужного размера
    public void compressAllPhoto(String userImagePath,int i){

            String filePath = SiliCompressor.with(this).compress(userImagePath);
            switch (i){
                case 0: photoFrontPath=filePath;
                    break;
                case 1: photoFront1Path=filePath;
                    break;
                case 2: photoBackPath=filePath;
                    break;
                case 3: photoLeftPath=filePath;
                    break;
                case 4: photoRightPath=filePath;
                    break;
            }
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Конструктор Прогресса загрузки
    public void createProgressAlert(boolean isVisible){
        if(isVisible){
            alertDialog.dismiss();
        }else {
            alertDialog = ProgressDialog.show(this, "",
                    "Идёт загрузка...", true);
        }
    }

    // Оптимизация интерфейса
    public void optimization(){
        height=Paper.book().read("height",0);
        width=Paper.book().read("width",0);
        int tenPxHeight=height/192;
        int tenPxWidth=width/108;

        optimization.Optimization(mainFrame);
        optimization.Optimization(front1Text1);
        optimization.Optimization(front1Text2);
        optimization.Optimization(front1Text3);
        optimization.Optimization(front2Text1);
        optimization.Optimization(front2Text2);
        optimization.Optimization(front2Text3);
        optimization.Optimization(backText1);
        optimization.Optimization(backText2);
        optimization.Optimization(backText3);
        optimization.Optimization(leftText1);
        optimization.Optimization(leftText2);
        optimization.Optimization(leftText3);
        optimization.Optimization(rightText1);
        optimization.Optimization(rightText2);
        optimization.Optimization(rightText3);

        FrameLayout.LayoutParams pagerParams=new FrameLayout.LayoutParams(tenPxWidth*40,tenPxHeight*80);
        pagerParams.gravity= Gravity.CENTER;
        pager.setLayoutParams(pagerParams);
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class MyPagerAdapter extends PagerAdapter {
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View view = LayoutInflater.from(UserAddPhotoActivity.this).inflate(R.layout.item_photo_page,null);
            optimization.Optimization((FrameLayout)view);

            ImageView imageView = (ImageView) view.findViewById(R.id.item_image);
            TextView textNumber=(TextView)view.findViewById(R.id.text_sample_number);

            // Ставим шаблон относительно пола
            if(currentSex.equals("male")){
                imageView.setImageResource(allPhotosSampleMale[position]);
            }else {
                imageView.setImageResource(allPhotosSampleFemale[position]);
            }
            textNumber.setText(String.valueOf(position+1));
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }

        @Override
        public int getCount() {
            return allPhotosSampleFemale.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }
    }
}
