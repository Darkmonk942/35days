package com.days35.days35app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.CreatePhotoPost;
import com.days35.days35app.retrofit.CreateTextPost;
import com.days35.days35app.retrofit.CreateVideoPost;
import com.days35.days35app.transform.RoundImageTransform;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.VideoPicker;
import com.kbeanie.multipicker.api.callbacks.VideoPickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenVideo;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 13.10.2016.
 */

public class CreatPostActivity extends AppCompatActivity implements VideoPickerCallback {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.frame_photo_post) FrameLayout framePhotoPost;
    @BindView(R.id.frame_text_post) FrameLayout frameTextPost;
    @BindView(R.id.frame_video_post) FrameLayout frameVideoPost;
    // ImageView
    @BindView(R.id.ic_user) ImageView userIcon;
    @BindView(R.id.img_user_photo_place) ImageView userPhotoPlace;
    @BindView(R.id.ic_add_photo) ImageView icAddPhoto;
    @BindView(R.id.ic_add_video) ImageView icAddVideo;
    // TextView
    @BindView(R.id.text_user_name) TextView userName;
    // VideoView
    @BindView(R.id.video_view) VideoView videoView;
    // EditText
    @BindView(R.id.text_post_header) EditText postHeaderField;
    @BindView(R.id.text_post_body) EditText postBodyField;
    // Примитивы
    public static final int REQUEST_CODE_PICKER=1;
    VideoPicker videoPicker;
    String userImagePath="";
    String userVideoPath="";
    ProgressDialog alertDialog;
    CreateTextPost createTextPost;
    CreatePhotoPost createPhotoPost;
    CreateVideoPost createVideoPost;
    String postType;
    String token="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен из базы
        token="Bearer "+ Paper.book().read("token");

        videoPicker = new VideoPicker(this);

        // Получаем текущего пользователя
        AuthorizationObject userProfile=Paper.book("user").read("userInfo");

        // Подгружаем юзернейм и фотку
        userName.setText(userProfile.firstName +" "+userProfile.lastName);

        Glide.with(this)
                .load(userProfile.userPic)
                .fitCenter()
                .transform(new RoundImageTransform(this))
                .into(userIcon);

        // Подключаем Ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Узнаём тип поста
        postType=getIntent().getStringExtra("postType");

        // Выводим нужный фрейм
        switch (postType){
            case "text": frameTextPost.setVisibility(View.VISIBLE);
                break;
            case "photo": framePhotoPost.setVisibility(View.VISIBLE);
                break;
            case "video":
                frameVideoPost.setVisibility(View.VISIBLE);
                break;
        }

        createTextPost = retrofit.create(CreateTextPost.class);
        createPhotoPost=retrofit.create(CreatePhotoPost.class);
        createVideoPost=retrofit.create(CreateVideoPost.class);

    }

    // Обработка кнопки назад
    public void backClick(View v){
        finish();
    }

    // Отправка поста на сервер относительно его типа
    public void sendPost(View v){
        switch (postType){
            case "text": sendTextPostFinal();
                break;
            case "photo": sendPhotoPostFinal();
                break;
            case "video": sendVideoPostFinal();
                break;
        }
    }

    // Добавление фото для поста
    public void addPhoto(View v){
        // ImagePicker
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code

    }

    // Добавляем видео
    public void addVideo(View v){
        videoPicker = new VideoPicker(this);
        videoPicker.shouldGenerateMetadata(true);
        videoPicker.shouldGeneratePreviewImages(true);
        videoPicker.setVideoPickerCallback(this);
        videoPicker.pickVideo();
    }

    // Отправка текстового поста на сервер
    public void sendTextPostFinal(){
        String headerText=postHeaderField.getText().toString();
        String bodyText=postBodyField.getText().toString();
        // Проверка на заполнение всех полей
        if(headerText.equals("")){
            createToast(getResources().getString(R.string.create_post_error_header));
            return;
        }
        if(bodyText.equals("")){
            createToast(getResources().getString(R.string.create_post_error_body));
            return;
        }


        // Отправляем пост на сервер
        createProgressAlert();
        Call<Void> sendPost = createTextPost.sendTextPost(
                token,
                bodyText,
                headerText);

        // Обработка полученной информации
        sendPost.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                Log.d("Создание поста", String.valueOf(response.code()));
                alertDialog.dismiss();
                // Неудачный запрос
                if(response.code()!=200){
                    createToast(getResources().getString(R.string.error_common));
                    return;
                }

                // Пост создан
                createToast(getResources().getString(R.string.create_post_add_success));
                finish();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("Создание поста", t.toString());
                createToast(getResources().getString(R.string.error_common));
                alertDialog.dismiss();
            }
        });
    }

    // Отправка фото поста на сервер
    public void sendPhotoPostFinal(){
        String headerText=postHeaderField.getText().toString();

        // Проверки на заполнение хедера и загрузку картинки
        if(headerText.equals("")){
            createToast(getResources().getString(R.string.create_post_error_header));
            return;
        }
        if(userImagePath.equals("")){
            createToast(getResources().getString(R.string.create_post_error_img));
            return;
        }
        //File creating from selected URL
        File file=null;
        try {
            file = new File(userImagePath);

            // Компрессия фото

        }catch (Exception e){
            Log.d("КартинкаОшибка",e.toString());
            return;
        }

        // Получаем картинку
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // Формируем тело для файлы
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("post_pic", file.getName(), requestFile);

        // Отправляем файл на сервер
        createProgressAlert();
        Call<Void> sendPhotoPost = createPhotoPost.sendPhotoPost(token,body,headerText);

        sendPhotoPost.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                alertDialog.dismiss();
                if(response.code()!=200){
                    createToast(getResources().getString(R.string.error_common));
                    return;
                }

                // Пост создан
                createToast(getResources().getString(R.string.create_post_add_success));
                finish();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("КартинкаОшибка1",t.toString());
                alertDialog.dismiss();
            }
        });
    }

    // Отправка видео поста на сервер
    public void sendVideoPostFinal(){
        String headerText=postHeaderField.getText().toString();

        // Проверки на заполнение хедера и загрузку картинки
        if(headerText.equals("")){
            createToast(getResources().getString(R.string.create_post_error_header));
            return;
        }
        if(userVideoPath.equals("")){
            createToast(getResources().getString(R.string.create_post_error_img));
            return;
        }

        //Получаем видео относительно пути
        File fileVideo = new File(userVideoPath);

        // Добавляем видео в мультипарт
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileVideo);

        // Формируем тело для файлы
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("post_vid", fileVideo.getName(), requestFile);

        // Отправляем файл на сервер
        createProgressAlert();
        Call<Void> sendVideoPost = createVideoPost.sendVideoPost(token,body,headerText);

        sendVideoPost.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                alertDialog.dismiss();
                Log.d("ВидеоОшибка", String.valueOf(response.code()));
                if(response.code()!=200){
                    createToast(getResources().getString(R.string.error_common));
                    return;
                }

                // Пост создан
                createToast(getResources().getString(R.string.create_post_add_success));
                finish();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("ВидеоОшибка1",t.toString());
                alertDialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);

            // Добавляем выбранное фото пользователя
            Glide.with(getApplicationContext())
                    .load(images.get(0).getPath())
                    .fitCenter()
                    .into(userPhotoPlace);
            userImagePath=images.get(0).getPath();
            icAddPhoto.setVisibility(View.GONE);
            return;
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_VIDEO_DEVICE) {
                if (videoPicker == null) {
                    videoPicker = new VideoPicker(this);
                    videoPicker.setVideoPickerCallback(this);
                }
                videoPicker.submit(data);
            }
        }
    }

    // Конструктор Прогресса загрузки
    public void createProgressAlert(){
        alertDialog = ProgressDialog.show(this, "",
                "Идёт загрузка...", true);
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onVideosChosen(List<ChosenVideo> list) {
        icAddVideo.setVisibility(View.GONE);
        frameVideoPost.setClickable(false);

        userVideoPath=list.get(0).getOriginalPath();
        videoView.setVideoPath(list.get(0).getOriginalPath());

        videoView.setMediaController(null);
        videoView.requestFocus(0);

        MediaController mediaController=new MediaController(this){
            @Override
            public void hide() {
                //Do not hide.
            }
        };

        videoView.setMediaController(mediaController);
        videoView.start();
    }

    @Override
    public void onError(String s) {

    }
}
