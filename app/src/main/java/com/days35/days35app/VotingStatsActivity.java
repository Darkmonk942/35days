package com.days35.days35app;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.objects.voting.VotingTableAllStats;
import com.days35.days35app.objects.voting.VotingTableStat;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetVotingEndAllStat;
import com.days35.days35app.retrofit.GetVotingEndUserStat;
import com.days35.days35app.transform.RoundImageTransform;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 14.12.2016.
 */

public class VotingStatsActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_top) LinearLayout linearTop;
    // ImageView
    @BindView(R.id.user_img) ImageView userImg;
    // TextView
    @BindView(R.id.user_place_text) TextView userPlaceText;
    @BindView(R.id.user_name_text) TextView userNameText;
    @BindView(R.id.user_score_text) TextView userScoreText;
    // Примитивы
    Optimization optimization;
    AuthorizationObject userProfile;
    GetVotingEndUserStat getVotingEndUserStat;
    GetVotingEndAllStat getVotingEndAllStat;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voting_stats);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен из базы
        token = "Bearer " + Paper.book().read("token");

        userProfile=Paper.book("user").read("userInfo",new AuthorizationObject());

        // Подключаем ретрофит и формируем запросы
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Запрос на получение инфы о текущем пользователе
        getVotingEndUserStat=retrofit.create(GetVotingEndUserStat.class);
        getVotingEndAllStat=retrofit.create(GetVotingEndAllStat.class);

        // Подгружаем данные пользователя
        getUserStats();

    }

    // Переход на предыдущий экран голосования
    public void backClick(View v){
        finish();
    }

    // Отправляем запрос на получение инфы о пользователе в рейтинге
    public void getUserStats(){
        Call<VotingTableStat> getStatsSuccess = getVotingEndUserStat.getVotingUserStat(token);

        getStatsSuccess.enqueue(new Callback<VotingTableStat>() {
            @Override
            public void onResponse(Call<VotingTableStat> call, Response<VotingTableStat> response) {
                Log.d("ТренировкиСтаты", String.valueOf(response.code()));
                if(response.code()==200){
                    // Обработка запроса
                    setUserStats(response.body());

                    // Получаем всех пользователей
                    getAllUsersStats();
                }

            }
            @Override
            public void onFailure(Call<VotingTableStat> call, Throwable t) {

            }
        });
    }

    // отправляем запрос на получение всех пользователей голосования
    public void getAllUsersStats(){
        Call<List<VotingTableAllStats>> getAllStatsSuccess = getVotingEndAllStat.getVotingAllStat(token);

        getAllStatsSuccess.enqueue(new Callback<List<VotingTableAllStats>>() {
            @Override
            public void onResponse(Call<List<VotingTableAllStats>> call, Response<List<VotingTableAllStats>> response) {
                Log.d("ТренировкиСтаты", String.valueOf(response.code()));
                if(response.code()==200){
                    // Обработка запроса
                    createTopUsers(response.body());
                }

            }
            @Override
            public void onFailure(Call<List<VotingTableAllStats>> call, Throwable t) {

            }
        });
    }

    // Меняем полученную инфу в UI
    public void setUserStats(VotingTableStat votingTableStat){

        // Иконка пользователя
        Glide.with(this)
                .load(votingTableStat.userPic)
                .fitCenter()
                .transform(new RoundImageTransform(this))
                .into(userImg);

        // Рейтинговая информация
        userPlaceText.setText(String.valueOf(votingTableStat.place));
        userNameText.setText(votingTableStat.firstName+" "+votingTableStat.lastName);
        userScoreText.setText(String.valueOf(votingTableStat.vote)+" "+getResources().getString(R.string.voting_votes_text));

    }

    // Создаём список всех игроков в топе
    public void createTopUsers(List<VotingTableAllStats> allStats){
        for (int i=0;i<allStats.size();i++){
            View layout=View.inflate(this,R.layout.item_voting_top,null);

            TextView userPlace=(TextView)layout.findViewById(R.id.user_place_text);
            TextView userName=(TextView)layout.findViewById(R.id.user_name_text);
            TextView userScore=(TextView)layout.findViewById(R.id.user_score_text);

            ImageView userImage=(ImageView)layout.findViewById(R.id.user_img);

            userPlace.setText(String.valueOf(i+1));
            userName.setText(allStats.get(i).userStats.firstName+" "+allStats.get(i).userStats.lastName);
            userScore.setText(String.valueOf(allStats.get(i).vote)+" "+getResources().getString(R.string.voting_votes_text));

            if(i<3){
                userPlace.setTextColor(getResources().getColor(R.color.colorPrimary));
                userName.setTextColor(getResources().getColor(R.color.colorPrimary));
                userScore.setTextColor(getResources().getColor(R.color.colorPrimary));
            }

            // подгружаем фотку юзера
            Glide.with(getApplicationContext())
                    .load(allStats.get(i).userStats.userPic)
                    .fitCenter()
                    .transform(new RoundImageTransform(this))
                    .into(userImage);

            // Добавляем весь фрейм в список
            linearTop.addView(layout);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
