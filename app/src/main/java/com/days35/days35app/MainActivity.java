package com.days35.days35app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.objects.RegistrationObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.PostFacebookEnter;
import com.days35.days35app.retrofit.PostVkEnter;
import com.days35.days35app.retrofit.RestorePassword;
import com.days35.days35app.retrofit.UserAuthorization;
import com.days35.days35app.retrofit.UserRegistration;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.model.VKScopes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnKeyListener {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.button_vk) FrameLayout btnVk;
    @BindView(R.id.button_facebook) FrameLayout btnFacebook;
    @BindView(R.id.frame_common_sign) FrameLayout frameCommonSign;
    @BindView(R.id.frame_sign_in) FrameLayout frameSignIn;
    @BindView(R.id.frame_sign_up) FrameLayout frameSignUp;
    @BindView(R.id.frame_lost_password) FrameLayout frameLostPassword;
    //ImageView
    @BindView(R.id.image_main_logo) ImageView imageLogo;
    @BindView(R.id.checkbox_terms) ImageView checkBoxTerms;
    // ProgressBar
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    // EditText
    @BindView(R.id.input_email) EditText emailRegistrationField;
    @BindView(R.id.input_password) EditText passwordRegistrationField;
    @BindView(R.id.input_email_enter) EditText emailEnterField;
    @BindView(R.id.input_password_enter) EditText passwordEnterField;
    @BindView(R.id.input_email_lost_password) EditText passwordRestoreField;
    // Примитивы
    private  String[] vkScope = new String[]{VKScopes.EMAIL,VKScopes.PHOTOS,VKScopes.VIDEO,VKScopes.WALL};
    ProgressDialog alertDialog;
    CallbackManager callbackManager;
    PostVkEnter postVkEnter;
    PostFacebookEnter postFacebookEnter;
    UserRegistration userRegistration;
    UserAuthorization userAuth;
    RestorePassword restorePassword;
    boolean startBGState=true;
    boolean termsUseCheck=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Получаем параметры экрана
        getScreenParams();

        // Оптимизация элементов интерфейса
        Optimization optimization=new Optimization();
        optimization.Optimization(mainFrame);

        // Колбек от Фесбук Логина
        callbackManager = CallbackManager.Factory.create();

        // Обработка EditText
        emailEnterField.setOnKeyListener(this);
        passwordEnterField.setOnKeyListener(this);
        emailRegistrationField.setOnKeyListener(this);
        passwordRegistrationField.setOnKeyListener(this);

        // Генерим ключик
        try {
            PackageInfo info =     getPackageManager().getPackageInfo("com.days35.days35app",     PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign= Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("codeSha", sign); }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {

        }

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        userRegistration = retrofit.create(UserRegistration.class);
        userAuth=retrofit.create(UserAuthorization.class);
        restorePassword=retrofit.create(RestorePassword.class);
        postVkEnter=retrofit.create(PostVkEnter.class);
        postFacebookEnter=retrofit.create(PostFacebookEnter.class);

        btnFacebook.setVisibility(View.GONE);
        btnVk.setVisibility(View.GONE);
        frameCommonSign.setVisibility(View.GONE);


        // Проверка на быстрый вход (ВК, Фейсбук, Логин+пароль)
        switch (Paper.book().read("autoAuth","")){

            case "":  // Отсутствует автовход
                btnFacebook.setVisibility(View.VISIBLE);
                btnVk.setVisibility(View.VISIBLE);
                frameCommonSign.setVisibility(View.VISIBLE);
                break;
            case "vk": // Автовход через ВК
                String vkToken = Paper.book().read("vkToken", "");
                if (!vkToken.equals("")) {
                    Log.d("вкТокен", vkToken);
                    createProgressAlert(false);
                    vkEnterRequest(vkToken);
                }else{
                    btnFacebook.setVisibility(View.VISIBLE);
                    btnVk.setVisibility(View.VISIBLE);
                    frameCommonSign.setVisibility(View.VISIBLE);
                }
                break;
            case "facebook": // Автовход через Фейсбук
                String fbToken = Paper.book().read("fbToken", "");
                if(!fbToken.equals("")){
                    String fbUserId = Paper.book().read("fbUserId", "");
                    createProgressAlert(false);
                    fbEnterRequest(fbToken,fbUserId);
                }else {
                    btnFacebook.setVisibility(View.VISIBLE);
                    btnVk.setVisibility(View.VISIBLE);
                    frameCommonSign.setVisibility(View.VISIBLE);
                }
                break;
            case "common": // Вход через логин/пароль
                automationAuth();
                break;

        }

    }

    // Обработка автоматического логина в приложение
    public void automationAuth(){
        // Запускаем авторизацию
        String savedEmail=Paper.book().read("emailSaved","");
        String savedPassword=Paper.book().read("passwordSaved","");

        userAuth(savedEmail,savedPassword,true);

    }

    // Открываем экран регистрации нового пользователя
    public void registrationClick(View v){
        frameSignUp.setVisibility(View.VISIBLE);
        changeBG();
    }

    // ЧекБокс Пользовательского соглашения
    public void termsUseClick(View v){
        if(termsUseCheck){
            checkBoxTerms.setImageResource(R.drawable.ic_checkbox_empty);
            termsUseCheck=false;
        }else{
            checkBoxTerms.setImageResource(R.drawable.ic_checkbox_fill);
            termsUseCheck=true;
        }
    }

    // Открываем экран обычного входа для пользователя
    public void commonEnterClick(View v){
        frameSignIn.setVisibility(View.VISIBLE);
        changeBG();

    }

    // Открываем экран восстановления пароля
    public void lostPasswordClick(View v){
        frameSignIn.setVisibility(View.GONE);
        frameLostPassword.setVisibility(View.VISIBLE);
    }

    // Вход через ВК
    public void vkClickEnter(View v){
        VKSdk.login(MainActivity.this,vkScope);
    }

    // Вход через Фейсбук
    public void facebookClickEnter(View v){

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>()
                {
                    @Override
                    public void onSuccess(final LoginResult loginResult)
                    {
                        Log.d("Фб инфа Токен",loginResult.getAccessToken().getToken());
                        Log.d("Фб инфа id",loginResult.getAccessToken().getUserId());
                        // Запрос на сервер для авторизации / регистрации
                       fbEnterRequest(loginResult.getAccessToken().getToken(),
                               loginResult.getAccessToken().getUserId());

                    }

                    @Override
                    public void onCancel()
                    {
                        Log.d("фбТокен","кенсел");
                    }

                    @Override
                    public void onError(FacebookException exception)
                    {
                        // App code
                        Log.d("фбТокен",exception.toString());
                    }
                });

    }

    // Выход из фрейма регистрации и входа
    public void exitBtnClick(View v){
        changeBG();
    }

    // Изменяем задний фон и логотип при появлении других окон
    public void changeBG(){
        if(startBGState){
            mainFrame.setBackgroundResource(R.drawable.bg_main_screen_gray);
            imageLogo.setImageResource(R.drawable.ic_35days_main_logo_white);
            frameCommonSign.setVisibility(View.GONE);
            btnFacebook.setVisibility(View.GONE);
            btnVk.setVisibility(View.GONE);
            startBGState=false;
        }else{
            mainFrame.setBackgroundResource(R.drawable.bg_splash_screen);
            imageLogo.setImageResource(R.drawable.ic_35days_main_logo);
            frameCommonSign.setVisibility(View.VISIBLE);
            btnFacebook.setVisibility(View.VISIBLE);
            btnVk.setVisibility(View.VISIBLE);
            frameSignIn.setVisibility(View.GONE);
            frameSignUp.setVisibility(View.GONE);
            frameLostPassword.setVisibility(View.GONE);
            startBGState=true;
            //hideKeyboard();
        }
    }

    // Обработка кнопки назад
    @Override
    public void onBackPressed() {
        if(startBGState){
            super.onBackPressed();
        }else{
            changeBG();
        }
    }

    // Убираем клавиатуру программно
    public void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        InputMethodManager immCheck = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (immCheck.isAcceptingText()){
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    // Регистрация пользователя через обычную регистрацию
    public void registrationEndClick(View v){
        final String emailRequest=emailRegistrationField.getText().toString();
        final String passwordRequest=passwordRegistrationField.getText().toString();

        // Проверка на введенные данные в поле email
        if(emailRequest.contains(" ")||!emailRequest.contains("@")||!emailRequest.contains(".")){
            createToast(getResources().getString(R.string.error_check_email_field));
            return;
        }

        // Проверка на введенные данные в поле password
        if(passwordRequest.contains(" ")){
            createToast(getResources().getString(R.string.error_password_spacing));
            return;
        }
        if(passwordRequest.length()<6){
            createToast(getResources().getString(R.string.error_password_lack_symbols));
            return;
        }
        if(passwordRequest.length()>20){
            createToast(getResources().getString(R.string.error_password_more_symbols));
            return;
        }

        // Проверка на соглашения с условиями
        if(!termsUseCheck){
            createToast(getResources().getString(R.string.error_check_terms));
            return;
        }

        // Диалог про начало регистрации
        createProgressAlert(false);

        // Отправляем запрос на регистрацию
        Call<RegistrationObject> getRegistrationSuccess = userRegistration.createUser(
                emailRequest,
                passwordRequest);

        // Обработка полученной информации
        getRegistrationSuccess.enqueue(new Callback<RegistrationObject>() {
            @Override
            public void onResponse(Call<RegistrationObject> call, Response<RegistrationObject> response) {

                createProgressAlert(true);
                if(response.code()!=200){
                    createToast(getResources().getString(R.string.error_email_used));
                    return;
                }

                // Сохраняем токен и эмейл пользователя
                AuthorizationObject authObject=new AuthorizationObject();
                authObject.email=emailRegistrationField.getText().toString();

                Paper.book().write("token", response.body().token);
                Paper.book("user").write("userInfo", authObject);

                // Сохраняем логин и пароль для быстрого входа
                Paper.book().write("emailSaved",emailRequest);
                Paper.book().write("passwordSaved",passwordRequest);

                // После удачной регистрации, производим логин в приложение
                automationAuth();

            }

            @Override
            public void onFailure(Call<RegistrationObject> call, Throwable t) {
                createToast(getResources().getString(R.string.error_common));
            }
        });
    }

    // Регистрация через Вк
    public void vkEnterRequest(final String vkToken){
        Call<AuthorizationObject> getAuthSuccess = postVkEnter.postVkToken(vkToken);

        getAuthSuccess.enqueue(new Callback<AuthorizationObject>() {
            @Override
            public void onResponse(Call<AuthorizationObject> call, Response<AuthorizationObject> response) {
                createProgressAlert(true);

                if(response.code()==200){
                    Log.d("токен",response.body().token);
                    // Сохраняем данные для быстрого входа
                    Paper.book().write("autoAuth","vk");

                    // Сохраняем данные с сервера (инфо о пользователе)
                    Paper.book().write("token", response.body().token);
                    Paper.book("user").write("userInfo", response.body());

                    // Сохраняем ВКТокен
                    Paper.book().write("vkToken",vkToken);

                    // Переходим на экран добавления информации
                    // Если не пройден первый этап авторизации (фото и инфо)
                    // То перебрасывай пользователя на экран добавления иконки и инфы
                    switch (response.body().isProfileFull){
                        case 0:
                            startActivity(new Intent(MainActivity.this, UserAddInfoActivity.class)
                                    .putExtra("enterType","social"));
                            finish();
                            break;
                        case 1:
                            startActivity(new Intent(MainActivity.this, UserAddPhotoActivity.class));
                            finish();
                            break;
                        case 2:
                            startActivity(new Intent(MainActivity.this, NewsFeedActivity.class));
                            finish();
                            break;
                    }

                }

            }

        @Override
        public void onFailure(Call<AuthorizationObject> call, Throwable t) {
            Log.d("АвторизацияОшибка", t.toString());
            createProgressAlert(true);
                frameCommonSign.setVisibility(View.VISIBLE);
                btnVk.setVisibility(View.VISIBLE);
                btnFacebook.setVisibility(View.VISIBLE);

        }
    });

    }

    // Регистрация через Facebook
    public void fbEnterRequest(final String fbToken,final String userId){
        Call<AuthorizationObject> getAuthSuccess = postFacebookEnter.postFBToken(fbToken,userId);

        getAuthSuccess.enqueue(new Callback<AuthorizationObject>() {
            @Override
            public void onResponse(Call<AuthorizationObject> call, Response<AuthorizationObject> response) {
                createProgressAlert(true);

                if(response.code()==200) {
                    Log.d("ФбВходПрвоерка", String.valueOf(response.body().isProfileFull));
                    // Сохраняем автоматический вход и тип
                    Paper.book().write("autoAuth","facebook");

                    // Сохраняем данные с сервера (инфо о пользователе)
                    Paper.book().write("token", response.body().token);
                    Paper.book("user").write("userInfo", response.body());

                    // Сохраняем ВКТокен
                    Paper.book().write("fbToken", fbToken);
                    Paper.book().write("fbUserId", userId);

                    // Переходим на экран добавления информации
                    // Если не пройден первый этап авторизации (фото и инфо)
                    // То перебрасывай пользователя на экран добавления иконки и инфы
                    switch (response.body().isProfileFull){
                        case 0:
                            startActivity(new Intent(MainActivity.this, UserAddInfoActivity.class)
                                    .putExtra("enterType","social"));
                            finish();
                            break;
                        case 1:
                            startActivity(new Intent(MainActivity.this, UserAddPhotoActivity.class));
                            finish();
                            break;
                        case 2:
                            startActivity(new Intent(MainActivity.this, NewsFeedActivity.class));
                            finish();
                            break;
                    }
                }

                }

            @Override
            public void onFailure(Call<AuthorizationObject> call, Throwable t) {
                createProgressAlert(true);
                Log.d("АвторизацияОшибка", t.toString());
                frameCommonSign.setVisibility(View.VISIBLE);
                btnVk.setVisibility(View.VISIBLE);
                btnFacebook.setVisibility(View.VISIBLE);

            }
        });
    }

    // Обработка нажатия на проверку Аутентификации
    public void authorizationEndClick(View v) {
        // Проверка на введенные данные в полях
        String emailRequest=emailEnterField.getText().toString();
        String passwordRequest=passwordEnterField.getText().toString();

        // Проверка на введенные данные в поле email
        if(emailRequest.contains(" ")||!emailRequest.contains("@")||!emailRequest.contains(".")){
            createToast(getResources().getString(R.string.error_check_email_field));
            return;
        }

        // Проверка на введенные данные в поле password
        if(passwordRequest.contains(" ")){
            createToast(getResources().getString(R.string.error_password_spacing));
            return;
        }
        if(passwordRequest.length()<6){
            createToast(getResources().getString(R.string.error_password_lack_symbols));
            return;
        }
        if(passwordRequest.length()>20){
            createToast(getResources().getString(R.string.error_password_more_symbols));
            return;
        }

        // Запускаем авторизацию пользователя после проверок полей
        userAuth(emailRequest,passwordRequest,false);
    }

        // Авторизация пользователя
        public void userAuth(final String email,final String password,final boolean fastAuth){

            createProgressAlert(false);
            // Отправляем запрос на авторизацию
            Call<AuthorizationObject> getAuthSuccess = userAuth.authUser(
                    email,
                    password
                    );
            getAuthSuccess.enqueue(new Callback<AuthorizationObject>() {
                @Override
                public void onResponse(Call<AuthorizationObject> call, Response<AuthorizationObject> response) {

                    // Скрываем Прогресс диалог
                    Log.d("Входик", String.valueOf(response.code()));
                    createProgressAlert(true);
                    if(response.code()!=200){

                        if(!fastAuth) {
                            createToast(getResources().getString(R.string.error_check_input_info));
                        }else{
                            frameCommonSign.setVisibility(View.VISIBLE);
                            btnVk.setVisibility(View.VISIBLE);
                            btnFacebook.setVisibility(View.VISIBLE);
                        }
                        return;
                    }

                    // Сохраняем данные и входим в приложение
                    AuthorizationObject authObject = response.body();
                    authObject.email=email;

                    // Сохраняем параметры быстрого входа
                    Paper.book().write("autoAuth","common");

                    // Сохраняем токен
                    Paper.book().write("token", response.body().token);
                    Log.d("Токен",response.body().token);

                    // Сохраняем логин-пароль
                    Paper.book().write("emailSaved",email);
                    Paper.book().write("passwordSaved",password);

                    // Сохраняем все данные пользователя
                    Paper.book("user").write("userInfo",authObject);

                    // Если не пройден первый этап авторизации (фото и инфо)
                    // То перебрасывай пользователя на экран добавления иконки и инфы
                    switch (authObject.isProfileFull){
                        case 0:
                            startActivity(new Intent(MainActivity.this, UserAddInfoActivity.class)
                                    .putExtra("enterType","common"));
                            finish();
                            break;
                        case 1:
                            startActivity(new Intent(MainActivity.this, UserAddPhotoActivity.class));
                            finish();
                            break;
                        case 2:
                            startActivity(new Intent(MainActivity.this, NewsFeedActivity.class));
                            finish();
                            break;
                    }

                }

                @Override
                public void onFailure(Call<AuthorizationObject> call, Throwable t) {
                    Log.d("АвторизацияОшибка", t.toString());
                    if(fastAuth){
                        createProgressAlert(true);
                        frameCommonSign.setVisibility(View.VISIBLE);
                        btnVk.setVisibility(View.VISIBLE);
                        btnFacebook.setVisibility(View.VISIBLE);
                    }
                }
            });
    }

    // Обработка запроса восстановления пароля
    public void lostPasswordEndClick(View v){
    String restorePasswordMail=passwordRestoreField.getText().toString();

        // Валидация эмейла
        if(restorePasswordMail.contains("@")&restorePasswordMail.contains(".")){

            // Делаем запрос на восстановление пароля
            // Отправляем запрос на регистрацию
            Call<Void> getRestorePassword = restorePassword.restorePassword(
                    restorePasswordMail);

            // Обработка полученной информации по восстановлению пароля
            getRestorePassword.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                    if(response.code()==200){
                        createToast(getResources().getString(R.string.restore_password_true));
                    }else {
                        createToast(getResources().getString(R.string.restore_password_false));
                    }

                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    createToast(getResources().getString(R.string.error_common));
                }
            });


        }else{
            // Ошибка валидации
            createToast(getResources().getString(R.string.error_check_input_info));
        }

    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Вкл/выкл Прогрессбар
    // Конструктор Прогресса загрузки
    public void createProgressAlert(boolean isVisible){
        if(isVisible&&alertDialog!=null){
            alertDialog.dismiss();
        }else {
            alertDialog = ProgressDialog.show(this, "",
                    "Идёт загрузка...", true);
        }
    }

    // Получаем параметры экрана
    public void getScreenParams(){
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display =wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height=size.y;
        int width=size.x;

        // Сохраняем параметры экрана
        if(Paper.book().read("height",0)==0){
            Paper.book().write("height",height);
            Paper.book().write("width",width);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken vkAcccess) {
                Log.d("photoMax",vkAcccess.accessToken );

                // Отправляем данные на сервер для авторизации
                vkEnterRequest(vkAcccess.accessToken);

               }
            @Override
            public void onError(VKError error) {
                Log.d("photoMax",error.toString() );
            }})) {
            super.onActivityResult(requestCode, resultCode, data);
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == EditorInfo.IME_ACTION_DONE) {

            View view = this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            return true;
        }
        return false;
    }
}
