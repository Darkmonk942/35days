package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetUserInfo;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 05.01.2017.
 */

public class SettingsActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // ToolBar
    @BindView(R.id.toolbar_menu) Toolbar toolbarMenu;
    // Switcher
    @BindView(R.id.switch_push) Switch switchPush;
    // TextView
    @BindView(R.id.immunity_text) TextView immunityText;
    @BindView(R.id.status_text) TextView premiumStatus;
    // Примитивы
    GetUserInfo getUserInfo;
    boolean isPushEnable=true;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        // Оптимизация интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Добавляем Дроер
        NavigationDrawer navigationDrawer=new NavigationDrawer();
        navigationDrawer.NavigationDrawer(this,toolbarMenu,8);

        // Получаем Токен
        token = "Bearer " + Paper.book().read("token");

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Запрос на получение инфы о юзере
        getUserInfo=retrofit.create(GetUserInfo.class);

        // Получаем сохранённые настройки
        isPushEnable= Paper.book().read("pushEnable",true);

        switchPush.setChecked(isPushEnable);

        switchPush.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Paper.book().write("pushEnable",b);
            }
        });

        // Подгружаем информацию о пользователе
        getUserSettings();

    }

    // Получаем инфу о пользователе
    public void getUserSettings(){
        AuthorizationObject userProfile=Paper.book("user").read("userInfo",new AuthorizationObject());

        Call<AuthorizationObject> getInfoSuccess = getUserInfo.getUserInfo(token, String.valueOf(userProfile.id));

        getInfoSuccess.enqueue(new Callback<AuthorizationObject>() {
            @Override
            public void onResponse(Call<AuthorizationObject> call, Response<AuthorizationObject> response) {

                if(response.code()==200) {
                    premiumStatus.setText(response.body().premiumName);
                    immunityText.setText(String.valueOf(response.body().immunityCount));
                }

            }
            @Override
            public void onFailure(Call<AuthorizationObject> call, Throwable t) {

            }
        });
    }

    // Отправка в почтовик для связи с админами
    public void sendMailClick(View v){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Тема письма");
        intent.putExtra(Intent.EXTRA_TEXT, "Текст");
        intent.setData(Uri.parse("mailto:admin@35days.com"));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    // Отправка в браузер для покупки пакетов
    public void participateClick(View v){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(getResources().getString(R.string.url_landing_link)));
        startActivity(browserIntent);
    }

    // Переход на страницу в ВК
    public void vkClick(View v){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(getResources().getString(R.string.url_vk_link)));
        startActivity(browserIntent);
    }

    // Переход на страницу в ФБ
    public void facebookClick(View v){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(getResources().getString(R.string.url_facebook_link)));
        startActivity(browserIntent);
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
