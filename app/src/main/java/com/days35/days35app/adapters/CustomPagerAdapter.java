package com.days35.days35app.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.days35.days35app.R;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 02.12.2016.
 */

public class CustomPagerAdapter extends PagerAdapter {

    private Context mContext;
    private int gender;
    private ArrayList<ImageView> allImageShapeBefore;
    private ArrayList<ImageView> allImageShapeAfter;
    private ArrayList<ImageView> allImageBefore;
    private ArrayList<ImageView> allImageAfter;
    private ArrayList<TextView> allTextReport;

    public CustomPagerAdapter(Context context, int gender,
                              ArrayList<ImageView> allImageShapeBefore,
                              ArrayList<ImageView> allImageShapeAfter,
                              ArrayList<ImageView> allImageBefore,
                              ArrayList<ImageView> allImageAfter, ArrayList<TextView> allTextReport) {
        mContext = context;
        this.gender=gender;
        this.allImageShapeBefore=allImageShapeBefore;
        this.allImageShapeAfter=allImageShapeAfter;
        this.allImageBefore=allImageBefore;
        this.allImageAfter=allImageAfter;
        this.allTextReport=allTextReport;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        View layout=null;
        if(position==0) {
            layout = View.inflate(mContext, R.layout.adapter_reports_photo1, null);
            allImageBefore.add((ImageView)layout.findViewById(R.id.image_before_front1));
            allImageBefore.add((ImageView)layout.findViewById(R.id.image_before_front2));
            allImageBefore.add((ImageView)layout.findViewById(R.id.image_before_back));
            allImageBefore.add((ImageView)layout.findViewById(R.id.image_before_left));
            allImageBefore.add((ImageView)layout.findViewById(R.id.image_before_right));

            ImageView imgBeforeFront1Back=(ImageView)layout.findViewById(R.id.image_shape_front1);
            ImageView imgBeforeFront2Back=(ImageView)layout.findViewById(R.id.image_shape_front2);
            ImageView imgBeforeBackBack=(ImageView)layout.findViewById(R.id.image_shape_back);
            ImageView imgBeforeLeftBack=(ImageView)layout.findViewById(R.id.image_shape_left);
            ImageView imgBeforeRightBack=(ImageView)layout.findViewById(R.id.image_shape_right);

            allImageShapeBefore.add(imgBeforeFront1Back); allImageShapeBefore.add(imgBeforeFront2Back);
            allImageShapeBefore.add(imgBeforeBackBack); allImageShapeBefore.add(imgBeforeLeftBack);
            allImageShapeBefore.add(imgBeforeRightBack);

            // меняем картинки в зависимости от пола пользователя
            checkGenderImage(imgBeforeFront1Back,imgBeforeFront2Back,
                    imgBeforeBackBack,imgBeforeLeftBack,imgBeforeRightBack);

            collection.addView(layout);

            allTextReport.add((TextView)layout.findViewById(R.id.text_before));
        }else{

            layout = View.inflate(mContext, R.layout.adapter_reports_photo2, null);
            ImageView imgAfterFront1=(ImageView)layout.findViewById(R.id.image_after_front1);
            ImageView imgAfterFront2=(ImageView)layout.findViewById(R.id.image_after_front2);
            ImageView imgAfterBack=(ImageView)layout.findViewById(R.id.image_after_back);
            ImageView imgAfterLeft=(ImageView)layout.findViewById(R.id.image_after_left);
            ImageView imgAfterRight=(ImageView)layout.findViewById(R.id.image_after_right);
            allTextReport.add((TextView)layout.findViewById(R.id.text_after));

            ImageView imgAfterFront1Back=(ImageView)layout.findViewById(R.id.image_after_front1_back);
            ImageView imgAfterFront2Back=(ImageView)layout.findViewById(R.id.image_after_front2_back);
            ImageView imgAfterBackBack=(ImageView)layout.findViewById(R.id.image_after_back_back);
            ImageView imgAfterLeftBack=(ImageView)layout.findViewById(R.id.image_after_left_back);
            ImageView imgAfterRightBack=(ImageView)layout.findViewById(R.id.image_after_right_back);

            allImageShapeAfter.add(imgAfterFront1); allImageShapeAfter.add(imgAfterFront2);
            allImageShapeAfter.add(imgAfterBack); allImageShapeAfter.add(imgAfterLeft);
            allImageShapeAfter.add(imgAfterRight);

            // Сохраняем все ImageView в лист
            allImageAfter.add(imgAfterFront1Back); allImageAfter.add(imgAfterFront2Back);
            allImageAfter.add(imgAfterBackBack); allImageAfter.add(imgAfterLeftBack);
            allImageAfter.add(imgAfterRightBack);

            // меняем картинки в зависимости от пола пользователя
            checkGenderImage(imgAfterFront1,imgAfterFront2,imgAfterBack,imgAfterLeft,imgAfterRight);

            collection.addView(layout);
        }
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    // В зависимости от пола подгружаем нужные картинки
    public void checkGenderImage(ImageView imgAfterFront1,ImageView imgAfterFront2,
                                 ImageView imgAfterBack,ImageView imgAfterLeft,ImageView imgAfterRight){

        if(gender==2){
            imgAfterFront1.setImageResource(R.drawable.img_photoshape_front1);
            imgAfterFront2.setImageResource(R.drawable.img_photoshape_front2);
            imgAfterBack.setImageResource(R.drawable.img_photoshape_back);
            imgAfterLeft.setImageResource(R.drawable.img_photoshape_left);
            imgAfterRight.setImageResource(R.drawable.img_photoshape_right);
        }else{
            imgAfterFront1.setImageResource(R.drawable.img_photoshape_front1_male);
            imgAfterFront2.setImageResource(R.drawable.img_photoshape_front2_male);
            imgAfterBack.setImageResource(R.drawable.img_photoshape_back_male);
            imgAfterLeft.setImageResource(R.drawable.img_photoshape_left_male);
            imgAfterRight.setImageResource(R.drawable.img_photoshape_right_male);
        }
    }

}
