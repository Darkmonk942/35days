package com.days35.days35app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.FullPost;
import com.days35.days35app.R;
import com.days35.days35app.objects.PostListObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.transform.RoundImageTransform;

import java.util.List;

import io.paperdb.Paper;

/**
 * Created by DarKMonK on 17.10.2016.
 */

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.PostsViewHolder> implements View.OnClickListener{
    public Context context;
    private Optimization optimization;
    List<PostListObject> postObjectList;

    public static class PostsViewHolder extends RecyclerView.ViewHolder {
        //LinearLayout
        LinearLayout linear;
        // CardView
        CardView postCard;
        // TextView
        TextView userName;
        TextView postDate;
        TextView postText;
        TextView likeCount;
        TextView messageCount;
        TextView shareCount;
        // Button
        Button fullPost;
        // ImageView
        ImageView userIcon;
        ImageView postImage;
        ImageButton popUpBtn;

        PostsViewHolder(View itemView) {
            super(itemView);
            linear=(LinearLayout) itemView.findViewById(R.id.linear_content);

            postCard = (CardView)itemView.findViewById(R.id.card_post);

            userName = (TextView)itemView.findViewById(R.id.text_user_name);
            postDate = (TextView)itemView.findViewById(R.id.text_date_post);
            postText = (TextView)itemView.findViewById(R.id.post_text_inner);
            likeCount = (TextView)itemView.findViewById(R.id.like_count);
            messageCount = (TextView)itemView.findViewById(R.id.message_count);
            shareCount = (TextView)itemView.findViewById(R.id.share_count);

            fullPost=(Button)itemView.findViewById(R.id.btn_full_post);

            userIcon=(ImageView)itemView.findViewById(R.id.ic_user);
            postImage=(ImageView)itemView.findViewById(R.id.post_image);
            popUpBtn=(ImageButton) itemView.findViewById(R.id.ic_popup);

            //videoPlayer=(ExposureVideoPlayer) itemView.findViewById(R.id.evp);

        }

    }




    public NewsFeedAdapter(List<PostListObject> postObjectList,Context context){

        this.postObjectList = postObjectList;
        this.context=context;
        optimization=new Optimization();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public PostsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_newsfeed__post, viewGroup, false);
        PostsViewHolder pvh = new PostsViewHolder(v);



        // Оптимизация Элементов
        optimization.Optimization((FrameLayout)v);
        optimization.OptimizationLinear(pvh.linear);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PostsViewHolder postsViewHolder, int i) {
       PostListObject postListObject=postObjectList.get(i);
        // Имя и дата создания поста
        postsViewHolder.userName.setText(postListObject.userPost.firstName+" "+
                postListObject.userPost.lastName);
        postsViewHolder.postDate.setText(String.valueOf(postListObject.date));

        // Добавляем кол-во лайков, сообщений и шарингов
        postsViewHolder.likeCount.setText(String.valueOf(postListObject.likeCount));
        postsViewHolder.messageCount.setText(String.valueOf(postListObject.commentCount));
        postsViewHolder.shareCount.setText(String.valueOf(postListObject.shareCount));

        postsViewHolder.fullPost.setTag(i);

        // Узнаём тип поста
        switch (postListObject.postType){
            case 1:
                postsViewHolder.postText.setVisibility(View.VISIBLE);
                postsViewHolder.postImage.setVisibility(View.GONE);
                postsViewHolder.postText.setText(postListObject.postText);
                break;
            case 2:
                postsViewHolder.postImage.setVisibility(View.VISIBLE);
                postsViewHolder.postText.setVisibility(View.GONE);
                Glide.with(context)
                        .load(postListObject.postPic)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(postsViewHolder.postImage);
                Log.d("Ссылка",postListObject.postPic);
                break;
            case 3:
                Log.d("Трёха","Трёха");
                postsViewHolder.postImage.setVisibility(View.GONE);
                postsViewHolder.postText.setVisibility(View.GONE);
                break;

        }

        // Добавляем выбранное фото пользователя
        Glide.with(context)
                .load(postListObject.userPost.userPic)
                .fitCenter()
                .transform(new RoundImageTransform(context))
                .into(postsViewHolder.userIcon);

        // Переход на полный пост
        postsViewHolder.fullPost.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent fullPostIntent=new Intent(context, FullPost.class);
        context.startActivity(fullPostIntent);
        Paper.book().write("currentPost",postObjectList.get(Integer.valueOf(v.getTag().toString())));
    }

    @Override
    public int getItemCount() {
        return postObjectList.size();
    }
}