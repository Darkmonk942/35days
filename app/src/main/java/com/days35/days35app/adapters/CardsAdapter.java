package com.days35.days35app.adapters;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.days35.days35app.R;
import com.days35.days35app.objects.voting.VotingMainObject;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 03.01.2017.
 */

public class CardsAdapter extends ArrayAdapter<VotingMainObject> {
    private final ArrayList<VotingMainObject> cards;
    private final LayoutInflater layoutInflater;
    Context context;

    public CardsAdapter(Context context, ArrayList<VotingMainObject> cards) {
        super(context, -1);
        this.context=context;
        this.cards = cards;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VotingMainObject votingMainObject = cards.get(position);

        View coreFrame = layoutInflater.inflate(R.layout.item_card, parent, false);
        ViewPager viewPager=(ViewPager)coreFrame.findViewById(R.id.view_pager);

        ImageView pagerDot1=(ImageView)coreFrame.findViewById(R.id.img_dot1);
        ImageView pagerDot2=(ImageView)coreFrame.findViewById(R.id.img_dot2);
        ImageView pagerDot3=(ImageView)coreFrame.findViewById(R.id.img_dot3);


        TextView weightBefore=(TextView)coreFrame.findViewById(R.id.text_weight_before);
        TextView weightAfter=(TextView)coreFrame.findViewById(R.id.text_weight_after);
        TextView chestBefore=(TextView)coreFrame.findViewById(R.id.text_chest_before);
        TextView chestAfter=(TextView)coreFrame.findViewById(R.id.text_chest_after);
        TextView waistBefore=(TextView)coreFrame.findViewById(R.id.text_waist_before);
        TextView waistAfter=(TextView)coreFrame.findViewById(R.id.text_waist_after);
        TextView hipsBefore=(TextView)coreFrame.findViewById(R.id.text_hips_before);
        TextView hipsAfter=(TextView)coreFrame.findViewById(R.id.text_hips_after);
        TextView legsBefore=(TextView)coreFrame.findViewById(R.id.text_legs_before);
        TextView legsAfter=(TextView)coreFrame.findViewById(R.id.text_legs_after);
        TextView handBefore=(TextView)coreFrame.findViewById(R.id.text_hands_before);
        TextView handAfter=(TextView)coreFrame.findViewById(R.id.text_hands_after);

        // Создаём пейджер
        createViewPager(viewPager,pagerDot1,pagerDot2,pagerDot3,votingMainObject);

        // Меняем информацию о параметрах юзера
        setUserParams(votingMainObject,weightBefore,chestBefore,waistBefore,
                hipsBefore,legsBefore,handBefore,weightAfter,chestAfter,
                waistAfter,hipsAfter,legsAfter,handAfter);

        return coreFrame;
    }

    @Override
    public VotingMainObject getItem(int position) {
        return cards.get(position);
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    // Создаём адаптер и пейджер
    public void createViewPager(ViewPager viewPager,final ImageView pagerDot1,
                                final ImageView pagerDot2,final ImageView pagerDot3,
                                VotingMainObject votingMainObject){

        // Создаём ViewPager с адаптером
        viewPager.setAdapter(new VotingAdapter(context,downloadPics(votingMainObject)));
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position){
                    case 0: pagerDot1.setBackgroundResource(R.drawable.shape_circle_fill_red);
                        pagerDot2.setBackgroundResource(R.drawable.shape_circle_red);
                        pagerDot3.setBackgroundResource(R.drawable.shape_circle_red);
                        break;
                    case 1: pagerDot1.setBackgroundResource(R.drawable.shape_circle_red);
                        pagerDot2.setBackgroundResource(R.drawable.shape_circle_fill_red);
                        pagerDot3.setBackgroundResource(R.drawable.shape_circle_red);
                        break;
                    case 2: pagerDot1.setBackgroundResource(R.drawable.shape_circle_red);
                        pagerDot2.setBackgroundResource(R.drawable.shape_circle_red);
                        pagerDot3.setBackgroundResource(R.drawable.shape_circle_fill_red);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    // Меняем параметры тела пользователя
    public void setUserParams(VotingMainObject currentUser,
                              TextView weightBefore, TextView chestBefore,
                              TextView waistBefore, TextView hipsBefore,
                              TextView legsBefore, TextView handBefore,
                              TextView weightAfter, TextView chestAfter,
                              TextView waistAfter, TextView hipsAfter,
                              TextView legsAfter, TextView handAfter){

        weightBefore.setText(currentUser.userStatsList.get(0).weight);
        chestBefore.setText(currentUser.userStatsList.get(0).chest);
        waistBefore.setText(currentUser.userStatsList.get(0).waist);
        hipsBefore.setText(currentUser.userStatsList.get(0).hips);
        legsBefore.setText(currentUser.userStatsList.get(0).feet);
        handBefore.setText(currentUser.userStatsList.get(0).hand);

        weightAfter.setText(currentUser.userStatsList.get(1).weight);
        chestAfter.setText(currentUser.userStatsList.get(1).chest);
        waistAfter.setText(currentUser.userStatsList.get(1).waist);
        hipsAfter.setText(currentUser.userStatsList.get(1).hips);
        legsAfter.setText(currentUser.userStatsList.get(1).feet);
        handAfter.setText(currentUser.userStatsList.get(1).hand);
    }

    // Подгружаем фотки пользователя
    public ArrayList<String> downloadPics(VotingMainObject currentUser){
        ArrayList<String> allLinks=new ArrayList();

        // Составляем лист из ссылок на фото пользователя
        allLinks.add(currentUser.userPics.frontBefore);
        allLinks.add(currentUser.userPics.frontAfter);
        allLinks.add(currentUser.userPics.leftBefore);
        allLinks.add(currentUser.userPics.leftAfter);
        allLinks.add(currentUser.userPics.backBefore);
        allLinks.add(currentUser.userPics.backAfter);

        return allLinks;
    }
}