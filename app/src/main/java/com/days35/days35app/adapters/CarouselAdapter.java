package com.days35.days35app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.days35.days35app.R;
import com.days35.days35app.objects.CarouselItem;

import java.util.ArrayList;

/**
 * Created by DarKMonK on 26.09.2016.
 */

public class CarouselAdapter extends BaseAdapter {
    private ArrayList<CarouselItem> mData = new ArrayList<>(0);
    private Context mContext;

    public CarouselAdapter(Context context) {
        mContext = context;
    }

    public void setData(ArrayList<CarouselItem> data) {
        mData = data;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int pos) {
        return mData.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_photo_page, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.item_text);
            viewHolder.image = (ImageView) rowView.findViewById(R.id.item_image);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();

        holder.image.setImageResource(mData.get(position).imageId);
        holder.text.setText(mData.get(position).name);

        return rowView;
    }


    static class ViewHolder {
        public TextView text;
        public ImageView image;
    }
}
