package com.days35.days35app.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.R;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by DarKMonK on 14.12.2016.
 */

public class VotingAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<ImageView> allImageShape=new ArrayList();
    private ArrayList<String> allLinks=new ArrayList();

    public VotingAdapter(Context context, ArrayList<String> allLinks) {
        this.allLinks=allLinks;
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        View layout=null;
        Log.d("Адаптер","Создание");
        switch (position){
            case 0:
                layout = View.inflate(mContext, R.layout.adapter_voting_img, null);
                ImageView imageBefore1=(ImageView)layout.findViewById(R.id.img_before);
                ImageView imageAfter1=(ImageView)layout.findViewById(R.id.img_after);
                downloadImg(imageBefore1,0);
                downloadImg(imageAfter1,1);

                collection.addView(layout);
                break;
            case 1:
                layout = View.inflate(mContext, R.layout.adapter_voting_img, null);
                ImageView imageBefore2=(ImageView)layout.findViewById(R.id.img_before);
                ImageView imageAfter2=(ImageView)layout.findViewById(R.id.img_after);
                downloadImg(imageBefore2,2);
                downloadImg(imageAfter2,3);

                collection.addView(layout);
                break;
            case 2:
                layout = View.inflate(mContext, R.layout.adapter_voting_img, null);
                ImageView imageBefore3=(ImageView)layout.findViewById(R.id.img_before);
                ImageView imageAfter3=(ImageView)layout.findViewById(R.id.img_after);
                downloadImg(imageBefore3,4);
                downloadImg(imageAfter3,5);

                collection.addView(layout);
                break;
        }
        Log.d("АдаптерКолво",String.valueOf(allImageShape.size()));

        return layout;
    }

    // Подгружаем фотки
    public void downloadImg(ImageView image,int position){

          Glide.with(mContext)
                  .load(allLinks.get(position))
                  .bitmapTransform(new RoundedCornersTransformation(mContext, 15, 15))
                  .diskCacheStrategy(DiskCacheStrategy.NONE)
                  .skipMemoryCache(true)
                  .into(image);

    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
