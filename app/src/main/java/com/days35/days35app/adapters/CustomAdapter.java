package com.days35.days35app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.R;
import com.days35.days35app.objects.PostListObject;

import java.util.ArrayList;



/**
 * Created by DarKMonK on 24.10.2016.
 */



public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

    ArrayList<PostListObject> postObjectList;
    private int[] mDataSetTypes;

    public static final int text = 0;
    public static final int photo = 1;
    public static final int video = 2;
    Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public class TextViewHolder extends ViewHolder {
        TextView postText;

        public TextViewHolder(View v) {
            super(v);
            this.postText = (TextView) v.findViewById(R.id.post_text_inner);
        }
    }

    public class PhotoViewHolder extends ViewHolder {
        ImageView image;

        public PhotoViewHolder(View v) {
            super(v);
            this.image = (ImageView) v.findViewById(R.id.post_image);
        }
    }

    public class VideoViewHolder extends ViewHolder {

        public VideoViewHolder(View v) {
            super(v);
            //this.videoPlayer = (ExposureVideoPlayer) v.findViewById(R.id.evp);
        }
    }


    public CustomAdapter(ArrayList<PostListObject> postObjectList, int[] dataSetTypes,Context context) {
        this.postObjectList = postObjectList;
        this.context=context;
        mDataSetTypes = dataSetTypes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v;
        if (viewType == text) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_newsfeed__post, viewGroup, false);

            return new TextViewHolder(v);
        } else if (viewType == photo) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_newsfeed__post, viewGroup, false);
            return new PhotoViewHolder(v);
        } else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_newsfeed_video, viewGroup, false);
            return new VideoViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (viewHolder.getItemViewType() == text) {
            TextViewHolder holder = (TextViewHolder) viewHolder;
            holder.postText.setText(postObjectList.get(position).postText);
        }
        else if (viewHolder.getItemViewType() == photo) {
            PhotoViewHolder holder = (PhotoViewHolder) viewHolder;
            Glide.with(context)
                    .load(postObjectList.get(position).postPic)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(holder.image);
        }
        else {

        }
    }

    @Override
    public int getItemCount() {
        return postObjectList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mDataSetTypes[position];
    }
}

