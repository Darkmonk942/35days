package com.days35.days35app.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.days35.days35app.FullPost;
import com.days35.days35app.NewsFeedActivity;
import com.days35.days35app.R;
import com.days35.days35app.objects.CommentObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.DeleteComment;
import com.days35.days35app.transform.RoundImageTransform;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by DarKMonK on 21.10.2016.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder> implements View.OnClickListener{
    public Context context;
    private Optimization optimization;
    int userId;
    DeleteComment deleteComment;
    String token;
    RecyclerView recyclerView;
    TextView messageTextCount;
    int currentPost;

    public static class CommentsViewHolder extends RecyclerView.ViewHolder {
        //LinearLayout
        LinearLayout linear;
        // FrameLayout
        FrameLayout commentFrame;
        // ImageView
        ImageView userIcon;
        ImageView deleteIc;
        // TextView
        TextView userName;
        TextView commentText;
        TextView dateText;

        CommentsViewHolder(View itemView) {
            super(itemView);
            linear=(LinearLayout) itemView.findViewById(R.id.linear_content);

            commentFrame=(FrameLayout) itemView.findViewById(R.id.comment_frame);

            userIcon=(ImageView) itemView.findViewById(R.id.user_icon);
            deleteIc=(ImageView)itemView.findViewById(R.id.ic_delete);

            userName=(TextView) itemView.findViewById(R.id.text_user_name);
            commentText=(TextView) itemView.findViewById(R.id.text_comment);
            dateText=(TextView) itemView.findViewById(R.id.text_date);


        }

    }

    List<CommentObject> commentsObjectList;

    public CommentsAdapter(List<CommentObject> commentsObjectList, Context context,
                           int userId, Retrofit retrofit,String token, TextView messageTextCount,
                           int currentPost){

        this.commentsObjectList = commentsObjectList;
        this.userId=userId;
        this.token=token;
        this.messageTextCount=messageTextCount;
        this.currentPost=currentPost;

        optimization=new Optimization();

        deleteComment=retrofit.create(DeleteComment.class);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView=recyclerView;
    }

    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.context = viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_comments, viewGroup, false);
        CommentsViewHolder pvh = new CommentsViewHolder(v);

        // Оптимизация Элементов
        optimization.Optimization((FrameLayout)v);
        optimization.OptimizationLinear(pvh.linear);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final CommentsViewHolder postsViewHolder, int i) {
        final CommentObject commentObject=commentsObjectList.get(i);
        // Имя и дата создания поста
        postsViewHolder.userName.setText(commentObject.userInfo.firstName+" "+
                commentObject.userInfo.lastName);
        postsViewHolder.dateText.setText(dateComment(commentObject.createdAt));
        postsViewHolder.commentText.setText(commentObject.commentText);

        // Проверка на возможность удаления поста
        if(userId==commentObject.userInfo.userId){
            postsViewHolder.deleteIc.setVisibility(View.VISIBLE);
            postsViewHolder.deleteIc.setTag(commentObject.id);
            postsViewHolder.deleteIc.setOnClickListener(this);
        }

        // Подгружаем фото пользователя
        Glide.with(context)
                .load(commentObject.userInfo.userPic)
                .fitCenter()
                .transform(new RoundImageTransform(context))
                .into(postsViewHolder.userIcon);

        postsViewHolder.userIcon.setOnClickListener(this);

    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()){
            case R.id.ic_delete: // Удаляем комент
                // Диалог для конфирма удаления
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                alertDialogBuilder
                        .setMessage(context.getResources().getString(R.string.delete_text_comment))
                        .setCancelable(true)
                        .setPositiveButton(context.getResources().getString(R.string.delete_text_yes),new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // Удаляем выбранный комент
                                deleteSelectedComment(v.getTag().toString());
                            }
                        })
                        .setNegativeButton(context.getResources().getString(R.string.delete_text_no),new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

                alertDialogBuilder.show();
                break;
            case R.id.user_icon: // Переход на страничку пользователя
                break;
        }
    }

    // Удаление выбранного комментария
    public void deleteSelectedComment(final String id){
        Call<Void> getDeleteSuccess = deleteComment.deleteComment(token,id);


        getDeleteSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if(response.code()==200){

                    // Меняем кол-во комментариев
                    Log.d("Тестируем", messageTextCount.getText().toString());
                    int commentCount= Integer.valueOf(messageTextCount.getText().toString())-1;
                    Log.d("Тестируем", String.valueOf(commentCount));
                    messageTextCount.setText(String.valueOf(commentCount));
                    NewsFeedActivity.allNewsList.get(currentPost).commentCount=commentCount;

                    // Находим нужный комментарий
                    for (int i=0;i<commentsObjectList.size();i++){
                        if(commentsObjectList.get(i).id==Integer.valueOf(id)){
                            commentsObjectList.remove(i);
                            FullPost.currentPageCount=1;
                            FullPost.totalPageCount=0;
                            recyclerView.getAdapter().notifyDataSetChanged();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return commentsObjectList.size();
    }

    public String dateComment(String unixTime){
        String finalDate="";
        long commentTime=Long.valueOf(unixTime);

        // Получаем текущее время и дни от юникстайма
        long currentUnixTime = System.currentTimeMillis() / 1000L;
        long currentDay=currentUnixTime/86400;
        long commentDay=commentTime/86400;

        // Получаем разницу времени
        long oddsTime=currentUnixTime-commentTime;

        // Узнаём точную дату
        Date date = new Date(commentTime*1000L);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        finalDate=format.format(date);

        if(oddsTime>=0) {
            if (oddsTime < 60) {
                finalDate = "только что";
            }
            if (oddsTime >= 60 && oddsTime < 3600) {
                finalDate = "менее часа назад";
            }
            if (oddsTime >= 3600 && commentDay == currentDay) {
                finalDate = "сегодня";
            }
            if (oddsTime > 3600 && currentDay - commentDay == 1) {
                finalDate = "вчера";
            }
        }

        return finalDate;
    }

}
