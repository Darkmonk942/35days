package com.days35.days35app;

/**
 * Created by DarKMonK on 05.01.2017.
 */

public interface OnCustomEventListener {
    void onEvent();
}

