package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.objects.TrainDayObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetTrainDays;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 23.10.2016.
 */

public class TrainInnerListActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_list) LinearLayout linearList;
    // TextView
    @BindView(R.id.text_header) TextView headerText;
    Optimization optimization;
    Retrofit retrofit;
    GetTrainDays getTrainDays;
    String token;
    String currentWeek;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_inner_list);
        ButterKnife.bind(this);

        // Оптимизация интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем Токен
        token = "Bearer " + Paper.book().read("token");

        // Получаем текущую неделю
        currentWeek=getIntent().getStringExtra("week");
        headerText.setText(currentWeek+" "+getResources().getString(R.string.trains_week));

        // Подключаем ретрофит и формируем запросы
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getTrainDays=retrofit.create(GetTrainDays.class);

        // Получаем список тренировок на каждый день
        getDaysFromBD();

    }

    // Выход в общий список тренировок
    public void backClick(View v){
        finish();
    }

    // Получаем все дни текущей недели с сервера
    public void getDaysFromBD(){
        Call<List<TrainDayObject>> getDaysTrainingSuccess = getTrainDays.getDayTraining(token, currentWeek);

        getDaysTrainingSuccess.enqueue(new Callback<List<TrainDayObject>>() {
            @Override
            public void onResponse(Call<List<TrainDayObject>> call, Response<List<TrainDayObject>> response) {
                Log.d("ТренировкиДни", String.valueOf(response.code()));
                if(response.code()==200){
                    createTrainList(response.body());
                }

            }
            @Override
            public void onFailure(Call<List<TrainDayObject>> call, Throwable t) {

            }
        });
    }

    // Создаём список тренировок
    public void createTrainList(List<TrainDayObject> daysList){

        for (int i=0;i<daysList.size();i++){
            View trainFrame=View.inflate(this,R.layout.item_inner_train,null);

            LinearLayout linearTrain=(LinearLayout)trainFrame.findViewById(R.id.linear_train);
            ImageView imageTrain=(ImageView)trainFrame.findViewById(R.id.train_image);
            TextView dayText=(TextView)trainFrame.findViewById(R.id.train_day);
            TextView trainText=(TextView)trainFrame.findViewById(R.id.train_name);
            TextView timeText=(TextView)trainFrame.findViewById(R.id.train_time);
            Button btnStart=(Button)trainFrame.findViewById(R.id.btn_start);

            dayText.setText(daysList.get(i).dayText);
            trainText.setText(daysList.get(i).titleText);
            timeText.setText(daysList.get(i).timeText+" min");
            // Сохраняем в таг название тренировки и её id
            Pair<String,String> extraParams=new Pair(String.valueOf(daysList.get(i).id),daysList.get(i).titleText);
            btnStart.setTag(extraParams);

            Glide.with(this)
                    .load(daysList.get(i).dayPic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageTrain);

            // Блокируем неактивные дни
            /*
            if(!daysList.get(i).isActive){
                btnStart.setBackgroundColor(getResources().getColor(R.color.grey));
                btnStart.setClickable(false);
            }*/

            // Оптимизация фрейма
            optimization.Optimization((FrameLayout)trainFrame);
            optimization.OptimizationLinear(linearTrain);

            linearList.addView(trainFrame);
        }
    }

    // Переход на отдельную тренировку
    public void currentTrainClick(View v){
        Pair<String,String> extraPair=(Pair<String, String>)v.getTag();
        startActivity(new Intent(this,TrainVideoActivity.class)
                .putExtra("trainType","trains")
                .putExtra("day",extraPair.first)
                .putExtra("trainName",extraPair.second));
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
