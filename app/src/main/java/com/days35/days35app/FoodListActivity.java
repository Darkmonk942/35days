package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.days35.days35app.optimization.Optimization;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 28.10.2016.
 */

public class FoodListActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame)
    FrameLayout mainFrame;
    // ToolBar
    @BindView(R.id.toolbar_menu)
    Toolbar toolbarMenu;
    // LinearLayout
    @BindView(R.id.linear_list)
    LinearLayout linearList;
    // Примитивы
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);
        optimization.OptimizationLinear(linearList);

        // Получаем токен из базы
        token = "Bearer " + Paper.book().read("token");

        // Добавляем Дроер
        NavigationDrawer navigationDrawer=new NavigationDrawer();
        navigationDrawer.NavigationDrawer(this,toolbarMenu,4);
    }

    // Получаем с сервера список тренировок
    public void getAllTrains(){

    }

    // Заполняем всю информацию о тренировках
    public void setTrainsInfo(){

    }

    // Переход на экран списка тренировок текущей недели
    public void foodListScreenClick(View v){
        startActivity(new Intent(this,FoodInnerListActivity.class).putExtra("week",v.getTag().toString()));
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
