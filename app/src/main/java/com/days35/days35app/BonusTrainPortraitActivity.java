package com.days35.days35app;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.days35.days35app.objects.bonus.BonusTrainingObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetBonusTrainingVideo;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerSimple;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.days35.days35app.Days35App.tenPxHeight;

/**
 * Created by DarKMonK on 07.01.2017.
 */

public class BonusTrainPortraitActivity extends AppCompatActivity{
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_list) LinearLayout linearList;
    // ScrollView
    @BindView(R.id.scroll_view) ScrollView scrollView;
    // TextView
    @BindView(R.id.text_header) TextView headerText;
    @BindView(R.id.text_time_count) TextView timeCountText;
    // Примитивы
    GetBonusTrainingVideo getBonusTrainingVideo;
    Optimization optimization;

    Timer myTimer;
    TimerTask timerTask;
    Handler handlerTimer=new Handler();

    ArrayList<JCCustomPlayer> allTrainsVideo=new ArrayList();
    ArrayList<ProgressBar> allTrainsProgress=new ArrayList();
    ArrayList<Pair<ImageView,JCCustomPlayer>> allVideos=new ArrayList();
    List<BonusTrainingObject> allTrains=new ArrayList();

    BonusTrainingObject currentBonusTraining;

    int currentTime=0;
    int totalTime=40;
    int currentTrainTask=0;

    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bonus_portrait_train);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Подключаем ретрофит и формируем запросы
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getBonusTrainingVideo=retrofit.create(GetBonusTrainingVideo.class);

        // Получаем токен
        token = "Bearer " + Paper.book().read("token");

        // Тип тренирвоки
        String bonusTrainingType=getIntent().getStringExtra("id");

        // Подгружаем список видео
        getAllTrains(bonusTrainingType);

    }

    // Переход на список тренеровок на неделю
    public void backClick(View v){
        finish();
    }

    // Подкачиваем список видосов
    public void getAllTrains(String trainType){
        Call<List<BonusTrainingObject>> getTrainingsSuccess = getBonusTrainingVideo.getTrainings(token,trainType);

        getTrainingsSuccess.enqueue(new Callback<List<BonusTrainingObject>>() {
            @Override
            public void onResponse(Call<List<BonusTrainingObject>> call, Response<List<BonusTrainingObject>> response) {
                Log.d("Бонусы", String.valueOf(response.code()));
                if(response.code()==200){

                    allTrains=(ArrayList)response.body();
                    // Подгружаем все данные по полученным видео на фреймы
                    createVideoList(response.body());

                    // Отображаем первую тренирвоку
                    currentBonusTraining=allTrains.get(0);
                }

            }
            @Override
            public void onFailure(Call<List<BonusTrainingObject>> call, Throwable t) {

            }
        });

    }

    // Создаём список видосов
    public void createVideoList(List<BonusTrainingObject> allTrainsList){

        for (int i=0;i<allTrainsList.size();i++){
            View trainFrame = View.inflate(this, R.layout.item_train_days, null);

            JCCustomPlayer videoPlayer=(JCCustomPlayer)trainFrame.findViewById(R.id.video_player);
            TextView trainText=(TextView)trainFrame.findViewById(R.id.text_train);
            TextView trainTextCount=(TextView)trainFrame.findViewById(R.id.text_train_count);
            ProgressBar progressBar=(ProgressBar)trainFrame.findViewById(R.id.progress_bar);
            ImageView startImage=(ImageView)trainFrame.findViewById(R.id.img_play);
            FrameLayout statsFrame=(FrameLayout)trainFrame.findViewById(R.id.frame_stats);

            statsFrame.setVisibility(View.GONE);

            // Ссылка на видео
            startImage.setTag(allTrainsList.get(i).trainingVid);

            // Собираем в листы все данные
            allTrainsVideo.add(videoPlayer);
            allTrainsProgress.add(progressBar);

            Pair<ImageView,JCCustomPlayer> videoPart=new Pair(startImage,videoPlayer);
            allVideos.add(videoPart);

            optimization.OptimizationLinear((LinearLayout) trainFrame);

            linearList.addView(trainFrame);
        }
    }

    // начало воспроизведения видео
    public void startVideoClick(View v){
        ImageView image=(ImageView)v;

        // Перебираем все видео
        for (int i=0;i<allVideos.size();i++){
            if(image.equals(allVideos.get(i).first)){
                currentTrainTask=i;
                image.setVisibility(View.GONE);
                allVideos.get(i).second.setUp(image.getTag().toString(), JCVideoPlayerSimple.SCREEN_LAYOUT_LIST);
                allVideos.get(i).second.startButton.callOnClick();
                allVideos.get(i).second.setOnStopListener(new OnCustomEventListener() {
                    @Override
                    public void onEvent() {
                        startTimer(allTrainsProgress.get(currentTrainTask));
                    }
                });

            }
        }
    }

    // Таймер для просчёта времени тренировки
    public void startTimer(final ProgressBar bar){
        myTimer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handlerTimer.post(new Runnable() {
                    public void run() {
                        currentTime = currentTime + 1;
                        bar.setProgress(currentTime*10);
                        totalTime=totalTime-1;

                        // Переводим время в стринг формат
                        if(totalTime<10){
                            timeCountText.setText("00:0" + totalTime);
                        }else {
                            timeCountText.setText("00:" + totalTime);
                        }
                        if(currentTime==10){
                                // Запускаем следующее видео
                                if(currentTrainTask!=allVideos.size()-1){
                                    currentTrainTask=currentTrainTask+1;
                                    allVideos.get(currentTrainTask).first.setClickable(true);
                                    allVideos.get(currentTrainTask).first.callOnClick();

                                    // Крутим скрол до нового видео
                                    scrollView.smoothScrollTo(0, currentTrainTask*tenPxHeight*107);
                                }

                            currentTime=0;
                            myTimer.cancel();
                            myTimer.purge();


                        }
                    }
                });
            }};

        myTimer.schedule(timerTask, 0, 1000);
    }

    @Override
    public void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
        if(myTimer!=null) {
            myTimer.cancel();
        }
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
