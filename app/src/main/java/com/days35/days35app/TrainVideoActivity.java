package com.days35.days35app;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.objects.TrainTasksObject;
import com.days35.days35app.objects.TrainingContainer;
import com.days35.days35app.objects.reports.TrainTasksBodyObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.CreateReportTest;
import com.days35.days35app.retrofit.GetAllTrainsInDay;
import com.days35.days35app.retrofit.GetBonusTrainingTrainsType;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerSimple;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.days35.days35app.R.layout.item_training_stats;
import static io.paperdb.Paper.book;

/**
 * Created by DarKMonK on 26.10.2016.
 */

public class TrainVideoActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.frame_final_spinners) FrameLayout frameFinalSpinners;
    FrameLayout testFrame;
    // LinearLayout
    @BindView(R.id.linear_list) LinearLayout linearList;
    LinearLayout firstFrame;
    // ScrollView
    @BindView(R.id.scroll_view) ScrollView scrollView;
    // TextView
    @BindView(R.id.text_header) TextView headerText;
    @BindView(R.id.text_time_count) TextView timeCountText;
    @BindView(R.id.total_time_header) TextView headerTotalTime;
    @BindView(R.id.text_pause_count) TextView pauseCountText;
    @BindView(R.id.text_start_train) TextView startTrainText;
    // Spinner
    @BindView(R.id.spinner_task1) Spinner spinnerFinal1;
    @BindView(R.id.spinner_task2) Spinner spinnerFinal2;
    @BindView(R.id.spinner_task3) Spinner spinnerFinal3;
    @BindView(R.id.spinner_task4) Spinner spinnerFinal4;
    @BindView(R.id.spinner_task5) Spinner spinnerFinal5;
    @BindView(R.id.spinner_task6) Spinner spinnerFinal6;
    // Примитивы
    Timer myTimer,pauseTimer;
    TimerTask timerTask,pauseTimerTask;
    Handler handlerTimer=new Handler();
    int totalScroll=0;
    int currentTime=0;
    float progressCell=0f;
    int totalTime=0;
    Retrofit retrofit;
    int currentTestTask=0;
    int currentTrainTask=0;
    String currentDayId;
    String totalVideo;
    String currentTrainType="trains";
    GetAllTrainsInDay getAllTrainsInDay;
    GetBonusTrainingTrainsType getBonusTrainingTrainsType;
    CreateReportTest createReportTest;
    ArrayList<View> allTrainsView=new ArrayList();
    ArrayList<TrainTasksBodyObject> allTrainsList=new ArrayList();
    ArrayList<TrainingContainer> allTrainsFrame=new ArrayList();
    ArrayList<Spinner> allTrainsSpinner=new ArrayList();
    ArrayList<Spinner> allFinalSpinner=new ArrayList();
    ArrayList<Integer> allSpinnersCount=new ArrayList();
    Optimization optimization;
    boolean isTestingTrain;
    String token;
    int tenPxHeight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_videos);
        ButterKnife.bind(this);

        //scrollView.setOnTouchListener(new OnTouch());

        // Оптимизация интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);
        int height= book().read("height");
        tenPxHeight=height/192;

        // Получаем текущий вид тренировки
        currentTrainType=getIntent().getStringExtra("trainType");
        currentDayId=getIntent().getStringExtra("day");
        headerText.setText(getIntent().getStringExtra("trainName"));


        // Получаем токен из базы
        token = "Bearer " + Paper.book().read("token");

        // Подключаем ретрофит и формируем запросы
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getAllTrainsInDay=retrofit.create(GetAllTrainsInDay.class);
        createReportTest=retrofit.create(CreateReportTest.class);
        getBonusTrainingTrainsType=retrofit.create(GetBonusTrainingTrainsType.class);

        // Инициализация финальных спиннеров
        allFinalSpinner.add(spinnerFinal1); allFinalSpinner.add(spinnerFinal2);
        allFinalSpinner.add(spinnerFinal3); allFinalSpinner.add(spinnerFinal4);
        allFinalSpinner.add(spinnerFinal5); allFinalSpinner.add(spinnerFinal6);

        for (int i=0;i<allFinalSpinner.size();i++){
            spinnerInit(allFinalSpinner.get(i),i,6);
        }

        // Получаем список тренировок в зависимости от Типа
        if(currentTrainType.equals("trains")) {
            getAllTrains();
        }else{
            getAllBonusTrains();
        }
    }

    // Переход на список тренеровок на неделю
    public void backClick(View v){
        finish();
    }

    // Получаем весь список тренировок на текущий день
    public void getAllTrains(){
        Call<TrainTasksObject> getAllTasksSuccess = getAllTrainsInDay.getAllTasks(token, currentDayId);

        getAllTasksSuccess.enqueue(new Callback<TrainTasksObject>() {
            @Override
            public void onResponse(Call<TrainTasksObject> call, Response<TrainTasksObject> response) {
                Log.d("Тренировки На день", String.valueOf(response.code()));
                if(response.code()==200){
                    createUIList(response.body());

                    if(response.body().isTesting){
                        frameFinalSpinners.setVisibility(View.VISIBLE);
                    }
                }

            }
            @Override
            public void onFailure(Call<TrainTasksObject> call, Throwable t) {

            }
        });
    }

    // Получаем весь список тренировок на текущий день
    public void getAllBonusTrains(){
        Call<List<TrainTasksBodyObject>> getAllTasksSuccess = getBonusTrainingTrainsType.getTrainings(token, currentDayId);

        getAllTasksSuccess.enqueue(new Callback<List<TrainTasksBodyObject>>() {
            @Override
            public void onResponse(Call<List<TrainTasksBodyObject>> call, Response<List<TrainTasksBodyObject>> response) {
                Log.d("Тренировки На день", String.valueOf(response.code()));
                if(response.code()==200){
                    TrainTasksObject trainTasksObject =new TrainTasksObject();
                    trainTasksObject.allVideos=response.body();
                    trainTasksObject.isTesting=false;
                    createUIList(trainTasksObject);
                }

            }
            @Override
            public void onFailure(Call<List<TrainTasksBodyObject>> call, Throwable t) {
                Log.d("Тренировки На день", t.toString());
            }
        });
    }

    // Создаём лист видео тренировок
    public void createUIList(TrainTasksObject allTasks){

        int totalTestTrains=0;
        isTestingTrain=allTasks.isTesting;

        for (int i=0;i<allTasks.allVideos.size();i++) {
            View trainFrame = View.inflate(this, R.layout.item_trains_sliding, null);

            TextView trainText=(TextView)trainFrame.findViewById(R.id.text_train);
            TextView trainTextCount=(TextView)trainFrame.findViewById(R.id.text_train_count);
            ImageView trainImage=(ImageView)trainFrame.findViewById(R.id.train_image);
            ImageView shadowImage=(ImageView)trainFrame.findViewById(R.id.img_shadow);
            ImageView startImage=(ImageView)trainFrame.findViewById(R.id.train_image_start);
            JCCustomPlayer videoPlayer=(JCCustomPlayer) trainFrame.findViewById(R.id.video_player);
            ProgressBar progressBar=(ProgressBar) trainFrame.findViewById(R.id.progress_bar);

            // Настройки плеера
            videoPlayer.backButton.setVisibility(View.GONE);
            videoPlayer.tinyBackImageView.setVisibility(View.GONE);
            videoPlayer.fullscreenButton.setVisibility(View.GONE);
            videoPlayer.currentTimeTextView.setVisibility(View.GONE);
            videoPlayer.loadingProgressBar.setVisibility(View.GONE);
            videoPlayer.totalTimeTextView.setVisibility(View.GONE);
            videoPlayer.progressBar.setVisibility(View.GONE);

            videoPlayer.widthRatio=1;
            videoPlayer.heightRatio=1;

            startImage.setTag(i);

            // Слушатель на окончание видоса
            setOnCompletion(videoPlayer);

            // Отдельный объект тренировок
            TrainTasksBodyObject currentTrainObject=allTasks.allVideos.get(i);
            allTrainsList.add(currentTrainObject);

            // Время тренировки
            trainText.setText(getStringTime(currentTrainObject.trainingTime));
            trainText.setTag(getStringTime(currentTrainObject.trainingTime));

            // Контейнер для фрейма с видео, плейсхолдером и прогрессом
            allTrainsFrame.add(new TrainingContainer(videoPlayer,progressBar
                    ,trainImage,startImage,shadowImage,trainText,currentTrainObject.trainingVid));

            // Подсчитываем общее время
            totalTime=totalTime+currentTrainObject.trainingTime;

            //trainText.setText(allTasks.allVideos.get(i).trainingTitle+"\n"+allTasks.allVideos.get(i).trainingTime);
            trainTextCount.setText(String.valueOf(i+1)+"/"+String.valueOf(allTasks.allVideos.size()));

            // Собираем в листы все данные
            optimization.OptimizationLinear((LinearLayout) trainFrame);
            allTrainsView.add(trainFrame);

            linearList.addView(trainFrame);
            firstFrame=(LinearLayout) trainFrame;

            Log.d("Тестовый видос",String.valueOf(currentTrainObject.isTestTrain));
            if(currentTrainObject.isTestTrain) {
                // Если это тестовое видео, то добавляем фрейм со спинерами
                int spinnerType=item_training_stats;
                switch (totalTestTrains){
                    case 0: spinnerType=R.layout.item_training_stats;
                        break;
                    case 1: spinnerType=R.layout.item_training_stats2;
                        break;
                    case 2: spinnerType=R.layout.item_training_stats3;
                        break;
                    case 3: spinnerType=R.layout.item_training_stats4;
                        break;
                    case 4: spinnerType=R.layout.item_training_stats5;
                        break;
                    case 5: spinnerType=R.layout.item_training_stats6;
                        break;
                }

                View statsFrame = View.inflate(this, spinnerType, null);
                Spinner spinner1 = (Spinner) statsFrame.findViewById(R.id.spinner_task1);
                spinnerInit(spinner1, 0,totalTestTrains);
                spinner1.setEnabled(false);
                allTrainsSpinner.add(spinner1);
                Spinner spinner2 = (Spinner) statsFrame.findViewById(R.id.spinner_task2);
                spinnerInit(spinner2, 1,totalTestTrains);
                spinner2.setEnabled(false);
                allTrainsSpinner.add(spinner2);
                Spinner spinner3 = (Spinner) statsFrame.findViewById(R.id.spinner_task3);
                spinnerInit(spinner3, 2,totalTestTrains);
                spinner3.setEnabled(false);
                allTrainsSpinner.add(spinner3);
                Spinner spinner4 = (Spinner) statsFrame.findViewById(R.id.spinner_task4);
                spinnerInit(spinner4, 3,totalTestTrains);
                spinner4.setEnabled(false);
                allTrainsSpinner.add(spinner4);
                Spinner spinner5 = (Spinner) statsFrame.findViewById(R.id.spinner_task5);
                spinnerInit(spinner5, 4,totalTestTrains);
                spinner5.setEnabled(false);
                allTrainsSpinner.add(spinner5);
                Spinner spinner6 = (Spinner) statsFrame.findViewById(R.id.spinner_task6);
                spinnerInit(spinner6, 5,totalTestTrains);
                spinner6.setEnabled(false);
                allTrainsSpinner.add(spinner6);

                switch (totalTestTrains){
                    case 0: spinner1.setEnabled(true);
                        break;
                    case 1: spinner2.setEnabled(true);
                        break;
                    case 2: spinner3.setEnabled(true);
                        break;
                    case 3: spinner4.setEnabled(true);
                        break;
                    case 4: spinner5.setEnabled(true);
                        break;
                    case 5: spinner6.setEnabled(true);
                        break;
                }

                totalTestTrains=totalTestTrains+1;

                optimization.Optimization((FrameLayout)statsFrame);
                testFrame=(FrameLayout)statsFrame;

                linearList.addView(statsFrame);
            }

            // Подгружаем картинки
            Glide.with(TrainVideoActivity.this)
                    .load(currentTrainObject.trainingPic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(trainImage);

        }

        // Определяем полное время тренировки
        String trainTimeText=getStringTime(totalTime);
        timeCountText.setText(trainTimeText);
        timeCountText.setTag(totalTime);

        // Бейдж на видео по текущей тренировке
        totalVideo=String.valueOf(allTasks.allVideos.size());

    }

    // Слушатель на окончание видоса
    public void setOnCompletion(final JCCustomPlayer videoPlayer){
        // Колбек плеера
        videoPlayer.setOnStopListener(new OnCustomEventListener() {
            @Override
            public void onEvent() {
                videoPlayer.setVisibility(View.INVISIBLE);
                allTrainsFrame.get(currentTrainTask).trainImage.setVisibility(View.VISIBLE);
                allTrainsFrame.get(currentTrainTask).startImage.setVisibility(View.VISIBLE);
                startTimer(allTrainsFrame.get(currentTrainTask).progressBar);
            }
        });
    }

    // Старт тренировки
    public void startTrainClick(View v){
        v.setVisibility(View.GONE);
        allTrainsFrame.get(0).shadowImage.setVisibility(View.GONE);
        allTrainsFrame.get(0).trainImage.setVisibility(View.GONE);
        allTrainsFrame.get(0).videoPlayer.setVisibility(View.VISIBLE);
        allTrainsFrame.get(0).videoPlayer.setUp
                (allTrainsList.get(0).trainingVid, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
        allTrainsFrame.get(0).videoPlayer.startButton.callOnClick();
    }

    // Создаём параметры для Спинера
    public void spinnerInit(Spinner taskSpinner,int index,int totalTestTrain){
        String trainType="";

        switch (index){
            case 0: trainType=getResources().getString(R.string.reports_table_berpi);
                break;
            case 1: trainType=getResources().getString(R.string.reports_table_vipadi);
                break;
            case 2: trainType=getResources().getString(R.string.reports_table_otzhim);
                break;
            case 3: trainType=getResources().getString(R.string.reports_table_hyper);
                break;
            case 4: trainType=getResources().getString(R.string.reports_table_skalolazi);
                break;
            case 5: trainType=getResources().getString(R.string.reports_table_press);
                break;
        }

        String spinnerArray[]=new String[101];
        for (int i=0;i<spinnerArray.length;i++){
            spinnerArray[i]=trainType+": "+String.valueOf(i);
            if(i==0){
                spinnerArray[i]=trainType;
            }
        }

        // Проверка на активный спиннер
        int spinnerType=R.layout.item_spinner_test_disable;
        switch (totalTestTrain){
            case 0: if(index==0){
                spinnerType=R.layout.item_spinner_test;
            }
                break;
            case 1: if(index==1){
                spinnerType=R.layout.item_spinner_test;
            }
                break;
            case 2: if(index==2){
                spinnerType=R.layout.item_spinner_test;
            }
                break;
            case 3: if(index==3){
                spinnerType=R.layout.item_spinner_test;
            }
                break;
            case 4: if(index==4){
                spinnerType=R.layout.item_spinner_test;
            }
                break;
            case 5: if(index==5){
                spinnerType=R.layout.item_spinner_test;
            }
                break;
            case 6:
                spinnerType=R.layout.item_spinner_test;
                break;
        }

        ArrayAdapter<String> adapterAge = new ArrayAdapter<String>(this,
                spinnerType,
                spinnerArray);
        taskSpinner.setAdapter(adapterAge);
    }

    // Если это первая или последняя недели, то отправляем на сервер результаты тренировок
    public void sendResultsToServer(View v){

        if(!isTestingTrain){
            finish();
            return;
        }

        // Проверка на заполнение всех полей
        if(!checkFieldsValidation()){
            return;
        }

        Call<Void> createTestSuccess = createReportTest.sendReportTest(token,
                1,
                allFinalSpinner.get(0).getSelectedItemPosition(),
                allFinalSpinner.get(1).getSelectedItemPosition(),
                allFinalSpinner.get(2).getSelectedItemPosition(),
                allFinalSpinner.get(3).getSelectedItemPosition(),
                allFinalSpinner.get(4).getSelectedItemPosition(),
                allFinalSpinner.get(5).getSelectedItemPosition());

        createTestSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("ОтправкаТеста", String.valueOf(response.code()));
                if(response.code()==200){
                    createToast(getResources().getString(R.string.reports_task_success));
                    finish();
                }

            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    // начало воспроизведения видео
    public void startVideoClick(View v){

    }

    // Таймер для просчёта времени тренировки
    public void startTimer(final ProgressBar progressBar){
        final TrainTasksBodyObject currentVideoTask=allTrainsList.get(currentTrainTask);
        final int trainingTime=currentVideoTask.trainingTime;

        // Анимация старта тренирвоки
        startTextAnimation();

        // Подгружаем следующее видео, если оно не последнее
        if(currentTrainTask+1!=allTrainsFrame.size()) {
            allTrainsFrame.get(currentTrainTask + 1).videoPlayer.setUp
                    (allTrainsList.get(currentTrainTask + 1).trainingVid, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
        }

        myTimer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handlerTimer.post(new Runnable() {
                    public void run() {
                        currentTime = currentTime + 1;

                        // Прогресс относительно общего времени
                        progressCell=progressCell +100/currentVideoTask.trainingTime;
                        progressBar.setProgress(Math.round(progressCell));
                        totalTime=totalTime-1;

                        // Переводим время в стринг формат
                        timeCountText.setText(getStringTime(totalTime));

                        String leftTime=String.valueOf(currentVideoTask.trainingTime-currentTime);
                        if (leftTime.length()==1){
                            leftTime="0"+leftTime;
                        }

                        allTrainsFrame.get(currentTrainTask).trainTime.setText("00:"+leftTime);

                        if(currentTime==trainingTime){
                            progressBar.setProgress(100);
                            progressCell=0f;
                            currentTime=0;
                            myTimer.cancel();
                            myTimer.purge();

                            if(allTrainsList.get(currentTrainTask).isTestTrain){
                                totalScroll=totalScroll+testFrame.getMeasuredHeight();
                                startPauseTimer();
                                return;
                            }

                            currentTrainTask=currentTrainTask+1;

                            if(currentTrainTask==allTrainsFrame.size()) {
                                totalScroll=totalScroll+firstFrame.getMeasuredHeight();
                                ObjectAnimator.ofInt(scrollView, "scrollY", totalScroll)
                                        .setDuration(500)
                                        .start();

                                // Заполняем окончательные спинеры
                                for (int i=0;i<allFinalSpinner.size();i++){
                                    allFinalSpinner.get(i).setSelection(allTrainsSpinner.get(i+24).getSelectedItemPosition());
                                }
                                return;

                            }

                                // Подгружаем новое видео
                                allTrainsFrame.get(currentTrainTask).trainImage.setVisibility(View.GONE);
                                allTrainsFrame.get(currentTrainTask).startImage.setVisibility(View.GONE);
                                allTrainsFrame.get(currentTrainTask).videoPlayer.setVisibility(View.VISIBLE);
                                //allTrainsFrame.get(currentTrainTask).videoPlayer.setUp
                                        //(allTrainsList.get(currentTrainTask).trainingVid, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                                allTrainsFrame.get(currentTrainTask).videoPlayer.startButton.callOnClick();


                                // Узнаём величину для прокрутки барабана
                                totalScroll = totalScroll + firstFrame.getMeasuredHeight();

                                ObjectAnimator.ofInt(scrollView, "scrollY", totalScroll)
                                        .setDuration(500)
                                        .start();

                                allTrainsFrame.get(currentTrainTask).shadowImage.setVisibility(View.GONE);

                        }
                    }
                });
            }};

        myTimer.schedule(timerTask, 0, 1000);
    }

    public void startPauseTimer(){
        final TrainTasksBodyObject currentVideoTask=allTrainsList.get(currentTrainTask);
        pauseTimer = new Timer();
        pauseTimerTask = new TimerTask() {
            public void run() {
                handlerTimer.post(new Runnable() {
                    public void run() {
                        headerTotalTime.setText("ПЕРЕРЫВ");
                        timeCountText.setVisibility(View.GONE);
                        pauseCountText.setText(getStringTime(currentVideoTask.pauseTime));
                        pauseCountText.setVisibility(View.VISIBLE);

                        currentTime=currentTime+1;
                        String leftTime=String.valueOf(currentVideoTask.pauseTime-currentTime);

                        if(leftTime.length()==1){
                            leftTime="0"+leftTime;
                        }

                        pauseCountText.setText("00:"+leftTime);

                        // Окончание паузы
                        if(currentTime==currentVideoTask.pauseTime){
                            currentTime=0;
                            pauseTimer.cancel();
                            pauseTimer.purge();

                            pauseCountText.setVisibility(View.GONE);
                            timeCountText.setVisibility(View.VISIBLE);

                            currentTrainTask=currentTrainTask+1;

                            if(currentTrainTask==allTrainsFrame.size()) {
                                totalScroll=totalScroll+firstFrame.getMeasuredHeight();
                                ObjectAnimator.ofInt(scrollView, "scrollY", totalScroll)
                                        .setDuration(500)
                                        .start();

                                // Заполняем окончательные спинеры
                                for (int i=0;i<allFinalSpinner.size();i++){
                                    allFinalSpinner.get(i).setSelection(allTrainsSpinner.get(i+24).getSelectedItemPosition());
                                }
                                return;

                            }

                            // Подгружаем новое видео
                            allTrainsFrame.get(currentTrainTask).trainImage.setVisibility(View.GONE);
                            allTrainsFrame.get(currentTrainTask).startImage.setVisibility(View.GONE);
                            allTrainsFrame.get(currentTrainTask).videoPlayer.setVisibility(View.VISIBLE);
                            allTrainsFrame.get(currentTrainTask).videoPlayer.setUp
                                    (allTrainsList.get(currentTrainTask).trainingVid, JCVideoPlayerSimple.CURRENT_STATE_NORMAL);
                            allTrainsFrame.get(currentTrainTask).videoPlayer.startButton.callOnClick();

                            // Узнаём величину для прокрутки барабана
                            totalScroll=totalScroll+firstFrame.getMeasuredHeight();

                            ObjectAnimator.ofInt(scrollView, "scrollY", totalScroll)
                                    .setDuration(500)
                                    .start();

                            allTrainsFrame.get(currentTrainTask).shadowImage.setVisibility(View.GONE);

                            currentTestTask=currentTestTask+1;

                            switch (currentTestTask) {
                                case 1:
                                    allTrainsSpinner.get(6).setSelection(allTrainsSpinner.get(0).getSelectedItemPosition());
                                    break;
                                case 2:
                                    allTrainsSpinner.get(12).setSelection(allTrainsSpinner.get(6).getSelectedItemPosition());
                                    allTrainsSpinner.get(13).setSelection(allTrainsSpinner.get(7).getSelectedItemPosition());
                                    break;
                                case 3:
                                    allTrainsSpinner.get(18).setSelection(allTrainsSpinner.get(12).getSelectedItemPosition());
                                    allTrainsSpinner.get(19).setSelection(allTrainsSpinner.get(13).getSelectedItemPosition());
                                    allTrainsSpinner.get(20).setSelection(allTrainsSpinner.get(14).getSelectedItemPosition());
                                    break;
                                case 4:
                                    allTrainsSpinner.get(24).setSelection(allTrainsSpinner.get(18).getSelectedItemPosition());
                                    allTrainsSpinner.get(25).setSelection(allTrainsSpinner.get(19).getSelectedItemPosition());
                                    allTrainsSpinner.get(26).setSelection(allTrainsSpinner.get(20).getSelectedItemPosition());
                                    allTrainsSpinner.get(27).setSelection(allTrainsSpinner.get(21).getSelectedItemPosition());
                                    break;
                                case 5:
                                    allTrainsSpinner.get(30).setSelection(allTrainsSpinner.get(24).getSelectedItemPosition());
                                    allTrainsSpinner.get(31).setSelection(allTrainsSpinner.get(25).getSelectedItemPosition());
                                    allTrainsSpinner.get(32).setSelection(allTrainsSpinner.get(26).getSelectedItemPosition());
                                    allTrainsSpinner.get(33).setSelection(allTrainsSpinner.get(27).getSelectedItemPosition());
                                    allTrainsSpinner.get(34).setSelection(allTrainsSpinner.get(28).getSelectedItemPosition());
                                    break;
                            }

                        }
                    }
                });
            }};

        pauseTimer.schedule(pauseTimerTask, 0, 1000);
    }

    // Проверка валидности всех полей
    public boolean checkFieldsValidation(){

        for (int i=0;i<allFinalSpinner.size();i++){
            if(allFinalSpinner.get(i).getSelectedItemPosition()==0){
                createToast(getResources().getString(R.string.trains_video_error));
                return false;
            }
        }

        return true;
    }

    // Клик пользователя на предыдущие видео
    public void replayVideoClick(View v){
        v.setVisibility(View.GONE);

        int pickedTrain=Integer.valueOf(v.getTag().toString());

        allTrainsFrame.get(pickedTrain).videoPlayer.startButton.callOnClick();
        allTrainsFrame.get(pickedTrain).trainImage.setVisibility(View.GONE);

        // Останавливаем таймеры
        if(pauseTimer!=null){
            pauseTimer.cancel();
            pauseTimer.purge();
        }

        if(myTimer!=null){
            myTimer.cancel();
            myTimer.purge();
        }

        // Прерываем текущее видео/тренировки и перематываем назад
        if(allTrainsFrame.get(currentTrainTask).videoPlayer.currentState==JCVideoPlayer.CURRENT_STATE_PLAYING){
            allTrainsFrame.get(currentTrainTask).videoPlayer.startButton.performClick();
            allTrainsFrame.get(currentTrainTask).videoPlayer.progressBar.setProgress(0);
        }

        // Отматываем полосы прогресса и время тренировки
        allTrainsFrame.get(pickedTrain).progressBar.setProgress(0);
        allTrainsFrame.get(pickedTrain).trainTime.setText
                (allTrainsFrame.get(pickedTrain).trainTime.getTag().toString());

        // Восстанавливаем нужное общее время тренировки
        int newTotalTime=Integer.valueOf(timeCountText.getTag().toString());
        for (int i=0;i<allTrainsList.size();i++){
            if(i==pickedTrain){
                break;
            }else{
                newTotalTime=newTotalTime-allTrainsList.get(i).trainingTime;
            }

        }

        totalTime=newTotalTime;
        currentTime=0;
        progressCell=0f;

        timeCountText.setVisibility(View.VISIBLE);
        timeCountText.setText(getStringTime(totalTime));
        headerTotalTime.setText(getResources().getString(R.string.trains_sum_time));
        pauseCountText.setVisibility(View.GONE);

        allTrainsFrame.get(pickedTrain).videoPlayer.setVisibility(View.VISIBLE);
        allTrainsFrame.get(pickedTrain).videoPlayer.startButton.callOnClick();

        currentTrainTask=pickedTrain;

        // Следующие видео скрываем
        for (int i=pickedTrain+1;i<allTrainsFrame.size();i++){
            allTrainsFrame.get(i).startImage.setVisibility(View.GONE);
            allTrainsFrame.get(i).videoPlayer.setVisibility(View.GONE);
            allTrainsFrame.get(i).shadowImage.setVisibility(View.VISIBLE);
            allTrainsFrame.get(i).trainImage.setVisibility(View.VISIBLE);
            allTrainsFrame.get(i).progressBar.setProgress(0);
            allTrainsFrame.get(i).trainTime.setText(allTrainsFrame.get(i).trainTime.getTag().toString());
        }

        // Определяем текущий уровень прокрутки
        totalScroll=0;
        for (int i=0;i<pickedTrain;i++){
            totalScroll=totalScroll+firstFrame.getMeasuredHeight();

            if(allTrainsList.get(i).isTestTrain){
                totalScroll=totalScroll+testFrame.getMeasuredHeight();
            }
        }

    }

    // Узнаём текущий фрейм в котором проигрывается звук
    public void checkCurrentVideoFrame(ImageView startButton){
        for (int i=0;i<allTrainsFrame.size();i++){
            if(allTrainsFrame.get(i).videoPlayer.startButton.equals(startButton)){
                Log.d("startButton",String.valueOf(i)+" фрейм");
                allTrainsFrame.get(i).videoPlayer.startButton.callOnClick();
                return;
            }
        }
    }

    private class OnTouch implements View.OnTouchListener
    {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }

    // Перевод времени в текстовый формат
    public String getStringTime(int time){
        String minutes=String.valueOf(time/60);
        if(minutes.length()==1){
            minutes="0"+minutes;
        }
        String seconds=String.valueOf(time%60);
        if(seconds.length()==1){
            seconds="0"+seconds;
        }

        return minutes+":"+seconds;
    }

    // Анимация старта тренировки
    public void startTextAnimation(){
        // Анимация появления номера текущего раунда
        startTrainText.setVisibility(View.VISIBLE);
        AnimatorSet fadeAndUnfade = new AnimatorSet();
        ValueAnimator unfadeAnim = ObjectAnimator.ofFloat(startTrainText, "alpha", 0f, 1f).setDuration(700);
        ValueAnimator fadeAnim = ObjectAnimator.ofFloat(startTrainText, "alpha", 1f, 0f).setDuration(2000);
        fadeAndUnfade.play(fadeAnim).after(unfadeAnim);
        fadeAndUnfade.start();

        fadeAndUnfade.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                startTrainText.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
        if(myTimer!=null) {
            myTimer.cancel();
        }
        if(pauseTimer!=null) {
            pauseTimer.cancel();
        }
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
