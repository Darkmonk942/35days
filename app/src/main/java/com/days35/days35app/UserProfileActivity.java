package com.days35.days35app;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetUserInfo;
import com.days35.days35app.retrofit.PutAboutMeUser;
import com.days35.days35app.transform.RoundImageTransform;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 31.10.2016.
 */

public class UserProfileActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    @BindView(R.id.btn_back_frame) FrameLayout btnBackFrame;
    // ToolBar
    @BindView(R.id.toolbar_menu) Toolbar toolbarMenu;
    // ImageView
    @BindView(R.id.user_icon) ImageView userIcon;
    // TextView
    @BindView(R.id.text_email) TextView textEmail;
    @BindView(R.id.text_user_name) TextView textUserName;
    // EditText
    @BindView(R.id.edit_profile_field) EditText profileField;
    // Button
    @BindView(R.id.btn_save) Button btnSave;
    @BindView(R.id.btn_edit) Button btnEdit;
    // Примитивы
    GetUserInfo getUserInfo;
    PutAboutMeUser putAboutMeUser;
    boolean isCurrentUserProfile;
    String token;
    String userTextProfile="";
    AuthorizationObject userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Подключаем ретрофит и формируем запросы
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Запрос на получение пользовательской инфы, обновление aboutMe
        getUserInfo=retrofit.create(GetUserInfo.class);
        putAboutMeUser=retrofit.create(PutAboutMeUser.class);

        // Получаем токен из базы
        token = "Bearer " + Paper.book().read("token");

        // Проверка на просмотр пользователя / своего профиля
        isCurrentUserProfile=getIntent().getBooleanExtra("currentUser",true);

        if(isCurrentUserProfile) { // Профиль текущего пользователя
            // Добавляем Дроер, если это профиль текущего человека
            NavigationDrawer navigationDrawer = new NavigationDrawer();
            navigationDrawer.NavigationDrawer(this, toolbarMenu, 2);

            // Получаем с базы инфу о пользователе и добавляем на экран
            userProfile=Paper.book("user").read("userInfo",new AuthorizationObject());
            getUserInfo(userProfile);

        }else{
            // Профиль другого пользователя
            btnBackFrame.setVisibility(View.VISIBLE);

            // Убираем возможность редактировать
            btnEdit.setVisibility(View.GONE);

            // Получаем информацию о выбранном пользователе
            getAnotherUserInfo(getIntent().getIntExtra("userId",0));

        }

        profileField.setEnabled(false);

    }

    // Переход на предыдущий экран
    public void backClick(View v){
        finish();
    }

    // Получаем всю информацию о текущем пользователе
    public void getUserInfo(AuthorizationObject userProfile){

        // Подкачиваем иконку текущего юзера
        Glide.with(getApplicationContext())
                .load(userProfile.userPic)
                .fitCenter()
                .transform(new RoundImageTransform(this))
                .into(userIcon);

        // Меняем имя и эмейл
        textEmail.setText(userProfile.email);
        textUserName.setText(userProfile.firstName +" "+userProfile.lastName);

        // Добавляем текст в поле О себе
        profileField.setText(userProfile.aboutMe);
    }

    // Получаем информацию о не текущем пользователе
    public void getAnotherUserInfo(int userId){
        Call<AuthorizationObject> getUserRequest = getUserInfo.getUserInfo(token, String.valueOf(userId));

        getUserRequest.enqueue(new Callback<AuthorizationObject>() {
            @Override
            public void onResponse(Call<AuthorizationObject> call, Response<AuthorizationObject> response) {

                if(response.code()==200){
                    getUserInfo(response.body());
                }

            }
            @Override
            public void onFailure(Call<AuthorizationObject> call, Throwable t) {

            }
        });
    }

    // Включаем возможность редактирования профайла
    public void editProfileClick(View v){
        btnEdit.setVisibility(View.GONE);
        btnSave.setVisibility(View.VISIBLE);

        profileField.setEnabled(true);
    }

    // Сохраняем отредактированный профайл
    public void saveProfileClick(View v){
        btnEdit.setVisibility(View.VISIBLE);
        btnSave.setVisibility(View.GONE);

        profileField.setEnabled(false);

        // Отправляем на сервер изменение профайла
        sendProfileChangeRequest();
    }

    public void sendProfileChangeRequest(){
        Call<Void> setAboutMe = putAboutMeUser.setAboutMe(token,
                profileField.getText().toString(),
                String.valueOf(userProfile.id));

        setAboutMe.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if(response.code()==200){
                    createToast(getResources().getString(R.string.update_about_me));
                    userProfile.aboutMe=profileField.getText().toString();
                    Paper.book("user").write("userInfo",userProfile);
                }

            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
