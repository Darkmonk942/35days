package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.days35.days35app.objects.TrainWeekObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetTrainWeeks;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 20.10.2016.
 */

public class TrainListActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // ToolBar
    @BindView(R.id.toolbar_menu) Toolbar toolbarMenu;
    // LinearLayout
    @BindView(R.id.linear_list) LinearLayout linearList;
    // Примитивы
    Optimization optimization;
    Retrofit retrofit;
    GetTrainWeeks getTrainWeeks;
    String token;

    String currentWeek="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_list);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен из базы
        token = "Bearer " + Paper.book().read("token");

        // Добавляем Дроер
        NavigationDrawer navigationDrawer=new NavigationDrawer();
        navigationDrawer.NavigationDrawer(this,toolbarMenu,3);

        // Подключаем ретрофит и формируем запросы
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getTrainWeeks =retrofit.create(GetTrainWeeks.class);

        // Получаем список тренировок
        getAllTrains();

    }

    // Получаем с сервера список тренировок
    public void getAllTrains(){
        Call<List<TrainWeekObject>> getWeekTrainingSuccess = getTrainWeeks.getWeekTraining(token);

        getWeekTrainingSuccess.enqueue(new Callback<List<TrainWeekObject>>() {
            @Override
            public void onResponse(Call<List<TrainWeekObject>> call, Response<List<TrainWeekObject>> response) {
                Log.d("Тренировки", String.valueOf(response.code()));
                if(response.code()==200){
                    setTrainsInfo(response.body());
                }

            }
            @Override
            public void onFailure(Call<List<TrainWeekObject>> call, Throwable t) {

            }
        });
    }

    // Создаём список тренировок
    public void setTrainsInfo(List<TrainWeekObject> weekList){
        Log.d("Тренировки", String.valueOf(weekList.size()));
        for (int i=0;i<weekList.size();i++){

            View weekTrain=View.inflate(this,R.layout.item_trains_week,null);
            weekTrain.setTag(i+1);

            ImageView triangleImg=(ImageView)weekTrain.findViewById(R.id.img_triangle);
            TextView weekText=(TextView)weekTrain.findViewById(R.id.text_week);
            TextView dateText=(TextView)weekTrain.findViewById(R.id.text_date);
            TextView timeText=(TextView)weekTrain.findViewById(R.id.text_time);

            // Проверка на активную неделю
            if(weekList.get(i).isActive||true){
                currentWeek=String.valueOf(i+1);
                triangleImg.setBackgroundResource(R.drawable.img_triangle_red);
                weekTrain.setClickable(true);
            }

            // Заполняем все поля
            weekText.setText(weekList.get(i).weekTitle);
            dateText.setText(weekList.get(i).startDate);
            timeText.setText(weekList.get(i).weekText);

            // Оптимизация интерфейса
            optimization.Optimization((FrameLayout)weekTrain);

            linearList.addView(weekTrain);
        }

    }

    // Переход на экран списка тренировок текущей недели
    public void trainsListScreenClick(View v){

        // Проверка на доступность тренировки
        if (v.getTag().toString().equals(currentWeek)||true){
            startActivity(new Intent(this, TrainInnerListActivity.class).putExtra("week", v.getTag().toString()));
        }else {
            createToast(getResources().getString(R.string.error_week_not_available));
        }
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
