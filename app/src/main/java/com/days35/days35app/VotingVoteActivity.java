package com.days35.days35app;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.days35.days35app.adapters.VotingAdapter;
import com.days35.days35app.objects.voting.VotingMainObject;
import com.days35.days35app.objects.voting.VotingStartObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetVotingStart;
import com.days35.days35app.retrofit.PostVotingId;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 13.12.2016.
 */

public class VotingVoteActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // ViewPager
    @BindView(R.id.view_pager) ViewPager viewPager;
    // ImageView
    @BindView(R.id.img_dot1) ImageView pagerDot1;
    @BindView(R.id.img_dot2) ImageView pagerDot2;
    @BindView(R.id.img_dot3) ImageView pagerDot3;
    // Button
    @BindView(R.id.btn_yes) Button btnYes;
    @BindView(R.id.btn_no) Button btnNo;
    // TextView
    @BindView(R.id.text_weight_before) TextView weightBefore;
    @BindView(R.id.text_weight_after) TextView weightAfter;
    @BindView(R.id.text_chest_before) TextView chestBefore;
    @BindView(R.id.text_chest_after) TextView chestAfter;
    @BindView(R.id.text_waist_before) TextView waistBefore;
    @BindView(R.id.text_waist_after) TextView waistAfter;
    @BindView(R.id.text_hips_before) TextView hipsBefore;
    @BindView(R.id.text_hips_after) TextView hipsAfter;
    @BindView(R.id.text_legs_before) TextView legsBefore;
    @BindView(R.id.text_legs_after) TextView legsAfter;
    @BindView(R.id.text_hands_before) TextView handBefore;
    @BindView(R.id.text_hands_after) TextView handAfter;
    // Примитивы
    String token;
    Snackbar snackbar;
    VotingAdapter votingAdapter;
    GetVotingStart getVotingStart;
    PostVotingId postVotingId;
    ArrayList<String> allLinks=new ArrayList();
    ArrayList<VotingMainObject> allVoting=new ArrayList();
    ArrayList<ImageView> allImageShape=new ArrayList();
    ArrayList<Integer> pickedUsers=new ArrayList();
    int currentUser=0;
    int currentUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voting_vote);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен из базы
        token = "Bearer " + Paper.book().read("token");

        // Подключаем ретрофит и формируем запросы
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getVotingStart =retrofit.create(GetVotingStart.class);
        postVotingId=retrofit.create(PostVotingId.class);

        createViewPager();

        // Запрос на получение юзеров для голосования
        getVotingUser();

    }

    public void backClick(View v){
        finish();
    }

    // Подгружаем список пользователей для голосования
    public void getVotingUser(){
        Call<VotingStartObject> getAllVotingSuccess = getVotingStart.getVotingPlayers(token);

        getAllVotingSuccess.enqueue(new Callback<VotingStartObject>() {
            @Override
            public void onResponse(Call<VotingStartObject> call, Response<VotingStartObject> response) {
                Log.d("Голосование", String.valueOf(response.code()));
                if(response.code()==200){

                    // Сохраняем полученные объекты для голосования в список
                    allVoting=(ArrayList<VotingMainObject>) response.body().userObject;

                    // Возвращаем видимость кнопкам
                    btnNo.setVisibility(View.VISIBLE);
                    btnYes.setVisibility(View.VISIBLE);

                    // Подгружаем первого пользователя
                    getUserProfile();

                }

            }
            @Override
            public void onFailure(Call<VotingStartObject> call, Throwable t) {

            }
        });
    }

    // Отправляем на сервер айдишники тех, за кого проголосовали
    public void sendVotingId(){

        // Проверка на минимум 5 голосов
        if(pickedUsers.size()<5){
            showSnackbar(getResources().getString(R.string.voting_send_allert));
            return;
        }

        String allId="";
        // Создаём стринг из массива
        for (int i=0;i<pickedUsers.size();i++){
            if(i!=pickedUsers.size()-1) {
                allId = allId + String.valueOf(pickedUsers.get(i)) + ",";
            }else{
                allId = allId + String.valueOf(pickedUsers.get(i));
            }
        }

        // отправляем на сервер айдишники тех, за кого проголосовали
        Call<Void> postVotingSuccess = postVotingId.postVoting(token,allId);

        postVotingSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("ОтправкаГолосов", String.valueOf(response.code()));
                if(response.code()==200){
                    createToast(getResources().getString(R.string.voting_send_success));
                    finish();
                }

            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("ОтправкаГолосов", String.valueOf(t.getMessage()));
            }
        });
    }

    // Подгружаем инфу о следующем пользователе
    public void getUserProfile(){
        allLinks.clear();
        if(currentUser!=allVoting.size()){

            currentUserId=allVoting.get(currentUser).userId;

            // Составляем лист из ссылок на фото пользователя
            allLinks.add(allVoting.get(currentUser).userPics.frontBefore);
            allLinks.add(allVoting.get(currentUser).userPics.frontAfter);
            allLinks.add(allVoting.get(currentUser).userPics.leftBefore);
            allLinks.add(allVoting.get(currentUser).userPics.leftAfter);
            allLinks.add(allVoting.get(currentUser).userPics.backBefore);
            allLinks.add(allVoting.get(currentUser).userPics.backAfter);

            for (int i=0;i<allLinks.size();i++){
                Log.d("Картиночки",allLinks.get(i));
            }

            votingAdapter=(VotingAdapter)viewPager.getAdapter();

            // Получаем параметры тела
            weightBefore.setText(allVoting.get(currentUser).userStatsList.get(0).weight);
            chestBefore.setText(allVoting.get(currentUser).userStatsList.get(0).chest);
            waistBefore.setText(allVoting.get(currentUser).userStatsList.get(0).waist);
            hipsBefore.setText(allVoting.get(currentUser).userStatsList.get(0).hips);
            legsBefore.setText(allVoting.get(currentUser).userStatsList.get(0).feet);
            handBefore.setText(allVoting.get(currentUser).userStatsList.get(0).hand);

            weightAfter.setText(allVoting.get(currentUser).userStatsList.get(1).weight);
            chestAfter.setText(allVoting.get(currentUser).userStatsList.get(1).chest);
            waistAfter.setText(allVoting.get(currentUser).userStatsList.get(1).waist);
            hipsAfter.setText(allVoting.get(currentUser).userStatsList.get(1).hips);
            legsAfter.setText(allVoting.get(currentUser).userStatsList.get(1).feet);
            handAfter.setText(allVoting.get(currentUser).userStatsList.get(1).hand);

        }else{
            // Заканчиваем голосование
            btnYes.setVisibility(View.GONE);
            btnNo.setVisibility(View.GONE);
        }
    }

    // Обработка кнопки Да
    public void yesBtnClick(View v){

        pickedUsers.add(currentUserId);
        currentUser=currentUser+1;

        // Проверка на окончание голосования
        if(currentUser==allVoting.size()){
            btnNo.setVisibility(View.GONE);
            btnYes.setVisibility(View.GONE);

            sendVotingId();
            return;
        }
        getUserProfile();
        viewPager.getAdapter().notifyDataSetChanged();
    }

    // Обработка кнопки Нет
    public void noBtnClick(View v){
        currentUser=currentUser+1;

        // Проверка на окончание голосования
        if(currentUser==allVoting.size()){
            btnNo.setVisibility(View.GONE);
            btnYes.setVisibility(View.GONE);

            sendVotingId();
            return;
        }
        getUserProfile();
    }

    // Создаём адаптер и пейджер
    public void createViewPager(){

        // Создаём ViewPager с адаптером
        //viewPager.setAdapter(new VotingAdapter(this));
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position){
                    case 0: pagerDot1.setBackgroundResource(R.drawable.shape_circle_fill_red);
                        pagerDot2.setBackgroundResource(R.drawable.shape_circle_red);
                        pagerDot3.setBackgroundResource(R.drawable.shape_circle_red);
                        break;
                    case 1: pagerDot1.setBackgroundResource(R.drawable.shape_circle_red);
                        pagerDot2.setBackgroundResource(R.drawable.shape_circle_fill_red);
                        pagerDot3.setBackgroundResource(R.drawable.shape_circle_red);
                        break;
                    case 2: pagerDot1.setBackgroundResource(R.drawable.shape_circle_red);
                        pagerDot2.setBackgroundResource(R.drawable.shape_circle_red);
                        pagerDot3.setBackgroundResource(R.drawable.shape_circle_fill_red);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    // Снекбар
    public void showSnackbar(String alertText){
        snackbar = Snackbar
                .make(findViewById(android.R.id.content), alertText, Snackbar.LENGTH_INDEFINITE)
                .setAction("заново", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                        finish();
                    }
                });
        snackbar.show();
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
