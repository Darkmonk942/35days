package com.days35.days35app.sharing;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.days35.days35app.retrofit.PostSharing;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;
import com.vk.sdk.dialogs.VKShareDialog;
import com.vk.sdk.dialogs.VKShareDialogBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vk.sdk.VKUIHelper.getApplicationContext;

/**
 * Created by DarKMonK on 16.01.2017.
 */

public class VkSharing {
    PostSharing postSharing;
    String token;
    int postId;
    TextView shareText;
    public void vkTextShare(String token,int postId,Context context, final FragmentManager fragmentManager,
                            int type, final String link, final String postName,final PostSharing postSharing,
                            TextView shareText) {

        this.postSharing=postSharing;
        this.token=token;
        this.postId=postId;
        this.shareText=shareText;

        if(type==1){
            VKShareDialogBuilder builder = new VKShareDialogBuilder();
            builder.setText(postName);

            sendPost(builder,fragmentManager);
        }
        // Создание Поста с картинкой
        if(type==2) {
            Glide
                    .with(context)
                    .load(link)
                    .asBitmap()
                    .into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL,Target.SIZE_ORIGINAL) {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                            VKShareDialogBuilder builder = new VKShareDialogBuilder();
                            builder.setText(postName);
                            builder.setAttachmentImages(new VKUploadImage[]{
                                    new VKUploadImage(resource, VKImageParameters.pngImage())
                            });

                            sendPost(builder,fragmentManager);
                        }
                    });
        }

        if(type==3){
            // Заливаем видел в вк
            /*final VKRequest save = VKApi.video()
                    .save(VKParameters.from("wallpost",1,"owner_id", "", "name", "Видос", "description", "описание", "link", link));*/

            VKShareDialogBuilder builder = new VKShareDialogBuilder();
            builder.setText(postName);

            builder.setAttachmentLink("35 Дней",link);
            sendPost(builder,fragmentManager);
        }

    }

    // Отправка картинки в Вк
    public void sendPost(VKShareDialogBuilder builder,FragmentManager fragmentManager){

        builder.setShareDialogListener(new VKShareDialog.VKShareDialogListener() {
            @Override
            public void onVkShareComplete(int postId) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Успешно", Toast.LENGTH_SHORT);
                toast.show();

                // Отправляем на сервер шаринг +1
                sharePostCounter();
            }

            @Override
            public void onVkShareCancel() {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Cancel", Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onVkShareError(VKError error) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Error", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        builder.show(fragmentManager, "VK_SHARE_DIALOG");
    }

    // Отправка запроса на сервер
    public void sharePostCounter(){
        int count=Integer.valueOf(shareText.getText().toString());
        shareText.setText(String.valueOf(count+1));
        Call<Void> getShareSuccess = postSharing.postShare(token, String.valueOf(postId));

        getShareSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("шаринг", String.valueOf(response.code()));

                if(response.code()==200){

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}
