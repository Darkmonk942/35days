package com.days35.days35app.sharing;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.TextView;

import com.days35.days35app.retrofit.PostSharing;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DarKMonK on 27.01.2017.
 */

public class FacebookSharing {

    PostSharing postSharing;
    String token;
    int postId;
    TextView shareText;

    public void facebookSharing(Context context, CallbackManager callbackManager, String token,
                                int postId, final String imageUrl, PostSharing postSharing,
                                final TextView shareText,final int shareType){
        this.shareText=shareText;
        this.token=token;
        this.postSharing=postSharing;
        this.postId=postId;

        // Вызываем диалог шаринга
        ShareDialog shareDialog = new ShareDialog((Activity)context);
        shareDialog.registerCallback(callbackManager, new
                FacebookCallback<Sharer.Result>() {
                    @Override
                    public void onSuccess(Sharer.Result result) {

                        shareText.setText(String.valueOf(Integer.valueOf(shareText.getText().toString())+1));
                        sharePostCounter();
                    }

                    @Override
                    public void onCancel() {}
                    @Override
                    public void onError(FacebookException error) {}
                });
        if (ShareDialog.canShow(ShareLinkContent.class)) {

            ShareLinkContent linkContent=null;
            switch (shareType){
                case 1:
                    linkContent = new ShareLinkContent.Builder()
                            .setContentTitle("35 Дней")
                            .setContentDescription("Текст")
                            .build();
                    break;
                case 2:
                    linkContent = new ShareLinkContent.Builder()
                            .setContentTitle("35 Дней")
                            .setContentDescription("Текст")
                            .setImageUrl(Uri.parse(imageUrl))
                            .build();
                    break;
                case 3:
                    linkContent = new ShareLinkContent.Builder()
                            .setContentTitle("35 Дней")
                            .setContentDescription("Текст")
                            .setContentUrl(Uri.parse(imageUrl))
                            .build();
                    break;
            }

            shareDialog.show(linkContent);
        }

    }

    // Отправка запроса на сервер
    public void sharePostCounter(){
        int count=Integer.valueOf(shareText.getText().toString());
        shareText.setText(String.valueOf(count+1));
        Call<Void> getShareSuccess = postSharing.postShare(token, String.valueOf(postId));

        getShareSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("шаринг", String.valueOf(response.code()));

                if(response.code()==200){

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}
