package com.days35.days35app.sharing;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.days35.days35app.retrofit.PostSharing;
import com.facebook.CallbackManager;
import com.iceteck.silicompressorr.SiliCompressor;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DarKMonK on 13.03.2017.
 */

public class InstagramSharing {

    PostSharing postSharing;
    String token;
    int postId;
    TextView shareText;
    Context context;

    public void startSharing(final Context context, CallbackManager callbackManager, String token,
                                 int postId, final String imageUrl, PostSharing postSharing,
                                 final TextView shareText, final int shareType){

        this.shareText=shareText;
        this.token=token;
        this.postSharing=postSharing;
        this.postId=postId;
        this.context=context;

        // Шаринг картинки
        if(shareType==2) {
            Glide.with(context)
                    .load(imageUrl)
                    .downloadOnly(new SimpleTarget<File>() {
                        @Override
                        public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {

                            try {
                                String filePath = SiliCompressor.with(context).compress(resource.getAbsolutePath());
                                File media = new File(filePath);
                                Intent instagramIntent = new Intent(Intent.ACTION_SEND);
                                instagramIntent.setType("image/*");
                                instagramIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(media));
                                instagramIntent.setPackage("com.instagram.android");
                                context.startActivity(instagramIntent);

                                sharePostCounter();
                            }catch (Exception e){
                                createToast("Произошла ошибка. Возможно отсутствует приложение Instagram");
                            }
                        }
                    });
        }else{
            // Шаринг видео
            Glide.with(context)
                    .load(imageUrl)
                    .downloadOnly(new SimpleTarget<File>() {
                        @Override
                        public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {
                            //String filePath = SiliCompressor.with(context).compress(resource.getAbsolutePath());
                            File media = new File(resource.getAbsolutePath());
                            Intent instagramIntent = new Intent(Intent.ACTION_SEND);
                            instagramIntent.setType("video/*");
                            instagramIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(media));
                            instagramIntent.setPackage("com.instagram.android");
                            context.startActivity(instagramIntent);
                        }
                    });
        }
    }

    // Отправка запроса на сервер
    public void sharePostCounter(){
        int count=Integer.valueOf(shareText.getText().toString());
        shareText.setText(String.valueOf(count+1));
        Call<Void> getShareSuccess = postSharing.postShare(token, String.valueOf(postId));

        getShareSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("шаринг", String.valueOf(response.code()));

                if(response.code()==200){

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(context, toastText,
                Toast.LENGTH_SHORT).show();
    }
}
