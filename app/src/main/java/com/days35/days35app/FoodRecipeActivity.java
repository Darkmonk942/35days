package com.days35.days35app;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.objects.FoodObject;
import com.days35.days35app.objects.FoodRequestObject;
import com.days35.days35app.optimization.Optimization;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 31.10.2016.
 */

public class FoodRecipeActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.main_linear) LinearLayout mainLinear;
    @BindView(R.id.linear_content) LinearLayout contentLinear;
    @BindView(R.id.recipe_linear1) LinearLayout recipeLinear1;
    @BindView(R.id.recipe_linear2) LinearLayout recipeLinear2;
    @BindView(R.id.recipe_linear3) LinearLayout recipeLinear3;
    // TextView
    @BindView(R.id.text_header) TextView headerText;
    @BindView(R.id.text_food_main) TextView foodMainText;
    @BindView(R.id.text_food_type1) TextView foodTextType1;
    @BindView(R.id.text_food_type2) TextView foodTextType2;
    @BindView(R.id.text_food_type3) TextView foodTextType3;
    @BindView(R.id.text_food_recipe1) TextView foodTextRecipe1;
    @BindView(R.id.text_food_recipe2) TextView foodTextRecipe2;
    @BindView(R.id.text_food_recipe3) TextView foodTextRecipe3;
    @BindView(R.id.text_food_name1) TextView foodTextName1;
    @BindView(R.id.text_food_name2) TextView foodTextName2;
    @BindView(R.id.text_food_name3) TextView foodTextName3;
    // ImageView
    @BindView(R.id.img_food) ImageView imgFood;
    // Примитивы
    Optimization optimization;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_recipe);
        ButterKnife.bind(this);

        // Оптимизация интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);
        optimization.OptimizationLinear(mainLinear);
        optimization.OptimizationLinear(contentLinear);

        // Получаем текущую неделю
        headerText.setText(getIntent().getStringExtra("week")+" "+getResources().getString(R.string.trains_week));

        // Меняем информацию о рецептах
        setFoodInfo();
    }

    // Переход на предыдущий экран списка еды на неделю
    public void backClick(View v){
        finish();
    }

    // Заполняем UI инфой с базы
    public void setFoodInfo(){
        // Получаем всю базу еды
        FoodRequestObject foodRequestObject= Paper.book().read("foodRecipe",new FoodRequestObject());
        ArrayList<FoodObject> foodList=new ArrayList();
        // Берём все рецепты с базы в зависимости от типа еды
        switch (getIntent().getStringExtra("type")){
            case "1":
                foodList=foodRequestObject.breakfastList; // Завтрак
                foodMainText.setText("ЗАВТРАК");
                break;
            case "2":
                foodList=foodRequestObject.lunchList; // Ланч
                foodMainText.setText("ЛАНЧ");
                break;
            case "3":
                foodList=foodRequestObject.dinnerList; // Обед
                foodMainText.setText("ОБЕД");
                break;
            case "4":
                foodList=foodRequestObject.snikerList; // Сникер
                foodMainText.setText("СНИКЕР");
                break;
            case "5":
                foodList=foodRequestObject.supperList; // Ужин
                foodMainText.setText("УЖИН");
                break;
        }

        // Подгружаем картинку для текущего типа еды
        Glide.with(this)
                .load(foodList.get(0).foodPic)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(imgFood);

        // Меняем название видов еды, рецепты

        // Заполняем ингридиенты для первого рецепта
        FoodObject foodObject1=foodList.get(0);
        foodTextType1.setText(foodObject1.foodTitle);
        foodTextRecipe1.setText(foodObject1.foodText);
        foodTextName1.setText(foodObject1.foodName);
        if(foodObject1.ingredientsList!=null) {
            for (int i = 0; i < foodObject1.ingredientsList.size(); i++) {
                View ingridientsFrame = View.inflate(this, R.layout.item_food_ingridients, null);
                TextView nameText = (TextView) ingridientsFrame.findViewById(R.id.name);
                TextView countText = (TextView) ingridientsFrame.findViewById(R.id.count);

                nameText.setText(foodObject1.ingredientsList.get(i).name);
                countText.setText(foodObject1.ingredientsList.get(i).count);

                optimization.Optimization((FrameLayout) ingridientsFrame);
                recipeLinear1.addView(ingridientsFrame);
            }
        }

        // Заполняем ингридиенты для второго рецепта
        FoodObject foodObject2=foodList.get(1);
        foodTextType2.setText(foodObject2.foodTitle);
        foodTextRecipe2.setText(foodObject2.foodText);
        foodTextName2.setText(foodObject2.foodName);
        if(foodObject2.ingredientsList!=null) {
            for (int i = 0; i < foodObject2.ingredientsList.size(); i++) {
                View ingridientsFrame = View.inflate(this, R.layout.item_food_ingridients, null);
                TextView nameText = (TextView) ingridientsFrame.findViewById(R.id.name);
                TextView countText = (TextView) ingridientsFrame.findViewById(R.id.count);

                nameText.setText(foodObject2.ingredientsList.get(i).name);
                countText.setText(foodObject2.ingredientsList.get(i).count);

                optimization.Optimization((FrameLayout) ingridientsFrame);
                recipeLinear2.addView(ingridientsFrame);
            }
        }

        // Заполняем ингридиенты для второго рецепта
        FoodObject foodObject3=foodList.get(2);
        foodTextType3.setText(foodObject3.foodTitle);
        foodTextRecipe3.setText(foodObject3.foodText);
        foodTextName3.setText(foodObject3.foodName);
        if(foodObject3.ingredientsList!=null) {
            for (int i = 0; i < foodObject3.ingredientsList.size(); i++) {
                View ingridientsFrame = View.inflate(this, R.layout.item_food_ingridients, null);
                TextView nameText = (TextView) ingridientsFrame.findViewById(R.id.name);
                TextView countText = (TextView) ingridientsFrame.findViewById(R.id.count);

                nameText.setText(foodObject3.ingredientsList.get(i).name);
                countText.setText(foodObject3.ingredientsList.get(i).count);

                optimization.Optimization((FrameLayout) ingridientsFrame);
                recipeLinear3.addView(ingridientsFrame);
            }
        }

    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
