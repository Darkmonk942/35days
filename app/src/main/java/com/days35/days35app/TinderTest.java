package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.days35.days35app.adapters.CardsAdapter;
import com.days35.days35app.objects.voting.VotingMainObject;
import com.days35.days35app.objects.voting.VotingStartObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetVotingStart;
import com.days35.days35app.retrofit.PostVotingId;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.arjsna.swipecardlib.SwipeCardView;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 10.01.2017.
 */

public class TinderTest extends AppCompatActivity {
    //FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // SwipeCardView
    @BindView(R.id.frame_swipe) SwipeCardView swipeFrame;
    // Button
    @BindView(R.id.btn_yes) Button btnYes;
    @BindView(R.id.btn_no) Button btnNo;
    // Примитивы
    GetVotingStart getVotingStart;
    PostVotingId postVotingId;
    Snackbar snackbar;

    ArrayList<VotingMainObject> allVoting=new ArrayList();
    ArrayList<Integer> pickedUsers=new ArrayList();

    String token;
    int currentUser=0;
    int currentUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voting_vote);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен из базы
        token = "Bearer " + Paper.book().read("token");

        // Подключаем ретрофит и формируем запросы
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getVotingStart =retrofit.create(GetVotingStart.class);
        postVotingId=retrofit.create(PostVotingId.class);

        // Запрос на получение юзеров для голосования
        getVotingUser();

    }

    // Переход на предыдущий экран
    public void backClick(View v){
        finish();
    }

    // Получаем с сервера список для голосования
    public void getVotingUser(){
        Call<VotingStartObject> getAllVotingSuccess = getVotingStart.getVotingPlayers(token);

        getAllVotingSuccess.enqueue(new Callback<VotingStartObject>() {
            @Override
            public void onResponse(Call<VotingStartObject> call, Response<VotingStartObject> response) {
                Log.d("Голосование", String.valueOf(response.code()));
                if(response.code()==200){

                    // Сохраняем полученные объекты для голосования в список
                    allVoting=(ArrayList<VotingMainObject>) response.body().userObject;

                    // Возвращаем видимость кнопкам
                    btnNo.setVisibility(View.VISIBLE);
                    btnYes.setVisibility(View.VISIBLE);
                    swipeFrame.setVisibility(View.VISIBLE);

                    // Создаём SwipeCardView
                    createSwipeController();

                    // Подгружаем первого пользователя

                }

            }
            @Override
            public void onFailure(Call<VotingStartObject> call, Throwable t) {

            }
        });
    }

    // Создаём SwipeCardView
    private void createSwipeController(){

        CardsAdapter arrayAdapter = new CardsAdapter(this, allVoting );

        swipeFrame.setAdapter(arrayAdapter);

        swipeFrame.setFlingListener(new SwipeCardView.OnCardFlingListener() {
            @Override
            public void onCardExitLeft(Object dataObject) {
                dislikeUser(); // Дизлайк пользователя в голосовании
            }

            @Override
            public void onCardExitRight(Object dataObject) {
                likeUser(); // лайк пользователя в голосовании
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {

            }

            @Override
            public void onCardExitTop(Object dataObject) {

            }

            @Override
            public void onCardExitBottom(Object dataObject) {

            }
        });
    }

    // Обработка кнопки Да
    public void yesBtnClick(View v){
        swipeFrame.throwRight();
    }

    // Обработка кнопки Нет
    public void noBtnClick(View v){
        swipeFrame.throwLeft();
    }

    // Лайк пользователя
    public void likeUser(){
        pickedUsers.add(allVoting.get(currentUser).userId);
        currentUser=currentUser+1;

        if(allVoting.size()-currentUser==2){
            swipeFrame.setMaxVisible(2);
        }
        if(allVoting.size()-currentUser==1){
            swipeFrame.setMaxVisible(1);
        }

        // Проверка на окончание голосования
        if(currentUser==allVoting.size()){
            btnNo.setVisibility(View.GONE);
            btnYes.setVisibility(View.GONE);

            sendVotingId();
        }
    }

    // Дизлайк пользователя
    public void dislikeUser(){
        currentUser=currentUser+1;

        if(allVoting.size()-currentUser==2){
            swipeFrame.setMaxVisible(2);
        }
        if(allVoting.size()-currentUser==1){
            swipeFrame.setMaxVisible(1);
        }

        // Проверка на окончание голосования
        if(currentUser==allVoting.size()){
            btnNo.setVisibility(View.GONE);
            btnYes.setVisibility(View.GONE);

            sendVotingId();
            return;
        }
    }

    // Отправляем на сервер айдишники тех, за кого проголосовали
    public void sendVotingId(){

        // Проверка на минимум 5 голосов
        if(pickedUsers.size()<5){
            showSnackbar(getResources().getString(R.string.voting_send_allert));
            return;
        }

        String allId="";
        // Создаём стринг из массива
        for (int i=0;i<pickedUsers.size();i++){
            if(i!=pickedUsers.size()-1) {
                allId = allId + String.valueOf(pickedUsers.get(i)) + ",";
            }else{
                allId = allId + String.valueOf(pickedUsers.get(i));
            }
        }

        // отправляем на сервер айдишники тех, за кого проголосовали
        Call<Void> postVotingSuccess = postVotingId.postVoting(token,allId);

        postVotingSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("ОтправкаГолосов", String.valueOf(response.code()));
                if(response.code()==200){
                    createToast(getResources().getString(R.string.voting_send_success));
                    startActivity(new Intent(TinderTest.this,VotingStatsActivity.class));
                    finish();
                }

            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("ОтправкаГолосов", String.valueOf(t.getMessage()));
            }
        });
    }

    // Снекбар
    public void showSnackbar(String alertText){
        snackbar = Snackbar
                .make(findViewById(android.R.id.content), alertText, Snackbar.LENGTH_INDEFINITE)
                .setAction("заново", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                        finish();
                    }
                });
        snackbar.show();
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}


