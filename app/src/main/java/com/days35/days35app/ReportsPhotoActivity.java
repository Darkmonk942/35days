package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.days35.days35app.adapters.CustomPagerAdapter;
import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.objects.ReportPicsObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetReportsPhoto;
import com.days35.days35app.retrofit.UserAddPhotoAfter;
import com.days35.days35app.retrofit.UserAddPhotosBefore;
import com.iceteck.silicompressorr.SiliCompressor;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 30.11.2016.
 */

public class ReportsPhotoActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // ViewPager
    @BindView(R.id.view_pager) ViewPager viewPager;
    // Button
    @BindView(R.id.btn_send) Button sendBtn;
    // ProgressBar
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    //ImageView
    ImageView currentBeforeImageView;
    ImageView currentImageView;
    // Примитивы
    public static final int REQUEST_CODE_PICKER=1;
    ArrayList<ImageView> allImageShapeBefore=new ArrayList();
    ArrayList<ImageView> allImageShapeAfter=new ArrayList();
    ArrayList<ImageView> allImageBefore=new ArrayList();
    ArrayList<ImageView> allImageAfter=new ArrayList();
    ArrayList<TextView> allTextReport=new ArrayList();
    String imgPathAfter[]=new String[5];
    String imgPathBefore[]=new String[5];
    GetReportsPhoto getReportsPhoto;
    UserAddPhotoAfter userAddPhotoAfter;
    UserAddPhotosBefore userAddPhotosBefore;
    boolean isPhotoAfterUploadAvailable;
    boolean isPhotoBeforeUploadAvailable;
    String token;
    int gender=0;
    int currentPhotoType=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports_photo);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем Токен
        token = "Bearer " + Paper.book().read("token");

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Запрос на получение всех фотографий пользователя
        getReportsPhoto = retrofit.create(GetReportsPhoto.class);

        // Запрос на добавление фото после
        userAddPhotoAfter=retrofit.create(UserAddPhotoAfter.class);
        userAddPhotosBefore=retrofit.create(UserAddPhotosBefore.class);

        // Получаем пол текущего пользователя
        AuthorizationObject authObject=Paper.book("user").read("userInfo", new AuthorizationObject());
        gender=authObject.sex;

        // Подключение адаптера под ViewPager
        viewPager.setAdapter(new CustomPagerAdapter(this,gender,allImageShapeBefore,
                allImageShapeAfter,allImageBefore,allImageAfter,
                allTextReport));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0){
                    currentPhotoType=0;
                    if(isPhotoBeforeUploadAvailable) {
                        sendBtn.setVisibility(View.VISIBLE);
                    }else{
                        sendBtn.setVisibility(View.GONE);
                    }
                    //sendBtn.setVisibility(View.VISIBLE);
                }else{
                    currentPhotoType=1;
                    if(isPhotoAfterUploadAvailable) {
                        sendBtn.setVisibility(View.VISIBLE);
                    }else{
                        sendBtn.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Подгружаем Загруженные ранее фотки пользователей до
        getAllUserPhoto();

    }

    // Переход на предыдущий экран
    public void backClick(View v){
        finish();
    }

    // Получаем фотки загруженные пользователем при регистрации
    public void getAllUserPhoto(){
        Call<ReportPicsObject> getPhotoSuccess = getReportsPhoto.getReportsPhoto(token);

        getPhotoSuccess.enqueue(new Callback<ReportPicsObject>() {
            @Override
            public void onResponse(Call<ReportPicsObject> call, Response<ReportPicsObject> response) {

                if(response.code()==200) {
                    // Загружаем фотки До
                    downloadPhotoBefore(response.body());

                    // Загружаем фотки после
                    downloadPhotoAfter(response.body());

                    // Добавляем тексты
                    allTextReport.get(1).setText(response.body().afterMessage);
                    allTextReport.get(0).setText(response.body().beforeMessage);

                    // Проверяем возможность загружать фотки на сервер
                    isPhotoAfterUploadAvailable=response.body().isAfterUploadAvailable;
                    isPhotoBeforeUploadAvailable=response.body().isBeforeUploadAvailable;

                    if(isPhotoBeforeUploadAvailable){
                        sendBtn.setVisibility(View.VISIBLE);
                    }
                }

            }
            @Override
            public void onFailure(Call<ReportPicsObject> call, Throwable t) {

            }
        });
    }

    // Подгружаем фотки на экран
    public void downloadPhotoBefore(ReportPicsObject photoBeforeObject){

        // Фото до Фронт1
        downloadDirectPhoto(photoBeforeObject.frontBefore1,allImageBefore.get(0),0,0);
        // Фото до Фронт2
        downloadDirectPhoto(photoBeforeObject.frontBefore2,allImageBefore.get(1),0,1);
        // Фото до сзади
        downloadDirectPhoto(photoBeforeObject.backBefore,allImageBefore.get(2),0,2);
        // Фото до слева
        downloadDirectPhoto(photoBeforeObject.leftBefore,allImageBefore.get(3),0,3);
        // Фото до справа
        downloadDirectPhoto(photoBeforeObject.rightBefore,allImageBefore.get(4),0,4);

    }

    // Прогружаем полученные фото после
    public void downloadPhotoAfter(ReportPicsObject photoAfterObject){

        // Фото после Фронт1
        downloadDirectPhoto(photoAfterObject.frontAfter1,allImageAfter.get(0),1,0);
        // Фото после Фронт2
        downloadDirectPhoto(photoAfterObject.frontAfter2,allImageAfter.get(1),1,1);
        // Фото после сзади
        downloadDirectPhoto(photoAfterObject.backAfter,allImageAfter.get(2),1,2);
        // Фото после слева
        downloadDirectPhoto(photoAfterObject.leftAfter,allImageAfter.get(3),1,3);
        // Фото после справа
        downloadDirectPhoto(photoAfterObject.rightAfter,allImageAfter.get(4),1,4);

    }

    // Подгрузка фоток
    public void downloadDirectPhoto(String link,ImageView imgPlace,int type,int index){
        if(link!=null&&!link.equals("")) {
            Glide.with(getApplicationContext())
                    .load(link)
                    .bitmapTransform(new RoundedCornersTransformation(this, 30, 20))
                    .into(imgPlace);

            if(type==0){
                allImageShapeBefore.get(index).setVisibility(View.GONE);
            }else{
                allImageShapeAfter.get(index).setVisibility(View.GONE);
            }
        }
    }

    // Выбираем фотку До
    public void setPhotoBefore(View v){
        currentImageView=(ImageView)v;

        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    // Выбираем фотку После
    public void setPhoto(View v){

        currentImageView=(ImageView)v;
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    // Клик на кнопку для отправки фоток после на сервер
    public void sendPhotoAfterClick(View v){

        sendPhotoAfter(currentPhotoType);
    }

    // Отправляем на сервер загруженные фото после
    public void sendPhotoAfter(final int type){

        // Проверка на кол-во загруженных фоток
        /*for (int i=0;i<imgPathAfter.length;i++){
            if(imgPathAfter[i]==null){
                createToast(getResources().getString(R.string.reports_photo_error));
                return;
            }
        }*/

        progressBar.setVisibility(View.VISIBLE);
        sendBtn.setVisibility(View.GONE);

        // Ужимаем фотки
        String imageMas[];
        if(type==0){
            imageMas=imgPathBefore;
        }else{
            imageMas=imgPathAfter;
        }
        compressAllPhoto(imageMas);

        // Файлы картинок пользователя
        File fileImageFront = null;
        File fileImageFront1 = null;
        File fileImageBack= null;
        File fileImageLeft= null;
        File fileImageRight= null;

        // Добавляем файлы в реквест
        RequestBody requestFileFront = null;
        RequestBody requestFileFront1 = null;
        RequestBody requestFileBack = null;
        RequestBody requestFileLeft = null;
        RequestBody requestFileRight = null;

        // Формируем тело для каждого файла
        MultipartBody.Part bodyFront =null;
        MultipartBody.Part bodyFront1 =null;
        MultipartBody.Part bodyBack =null;
        MultipartBody.Part bodyLeft =null;
        MultipartBody.Part bodyRight =null;

        for(int i=0;i<imageMas.length;i++){
            switch (i){
                case 0: if(imageMas[0]!=null){
                    fileImageFront = new File(imageMas[0]);
                    requestFileFront = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageFront);
                    bodyFront =
                            MultipartBody.Part.createFormData("front1", fileImageFront.getName(), requestFileFront);
                }
                break;
                case 1: if(imageMas[1]!=null){
                    fileImageFront1 = new File(imageMas[1]);
                    requestFileFront1 = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageFront1);
                    bodyFront1 =
                            MultipartBody.Part.createFormData("front2", fileImageFront1.getName(), requestFileFront1);
                }
                    break;
                case 2: if(imageMas[2]!=null){
                    fileImageBack = new File(imageMas[2]);
                    requestFileBack = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageBack);
                    bodyBack =
                            MultipartBody.Part.createFormData("back", fileImageBack.getName(), requestFileBack);
                }
                    break;
                case 3: if(imageMas[3]!=null){
                    fileImageLeft = new File(imageMas[3]);
                    requestFileLeft = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageLeft);
                    bodyLeft =
                            MultipartBody.Part.createFormData("left", fileImageLeft.getName(), requestFileLeft);
                }
                    break;
                case 4: if(imageMas[4]!=null){
                    fileImageRight = new File(imageMas[4]);
                    requestFileRight = RequestBody.create(MediaType.parse("multipart/form-data"), fileImageRight);
                    bodyRight =
                            MultipartBody.Part.createFormData("right", fileImageRight.getName(), requestFileRight);
                }
                    break;
            }
        }

        // Отправляем файл на сервер
        Call<Void> resultImageUpload=null;

        if(type==0){
            resultImageUpload= userAddPhotosBefore.uploadImage(token,
                    bodyFront,bodyFront1,bodyBack,bodyLeft,bodyRight);
        }else{
            resultImageUpload= userAddPhotoAfter.uploadImage(token,
                    bodyFront,bodyFront1,bodyBack,bodyLeft,bodyRight);
        }

        sendBtn.setVisibility(View.GONE);
        resultImageUpload.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("ЗапросОтправка", String.valueOf(response.code()));

                progressBar.setVisibility(View.GONE);
                if(response.code()==200){
                    sendBtn.setVisibility(View.GONE);
                    if (type==0){
                        isPhotoBeforeUploadAvailable=false;
                    }else{
                        isPhotoAfterUploadAvailable=false;
                    }
                    createToast(getResources().getString(R.string.reports_photo_send_success));
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                createToast("Произошла ошибка");
                progressBar.setVisibility(View.GONE);
                sendBtn.setVisibility(View.VISIBLE);
            }
        });
    }

    // Ужимаем все фотографии до нужного размера
    public void compressAllPhoto(String allImage[]){

        for (int i=0;i<allImage.length;i++){
            if(allImage[i]!=null) {
                String filePath = SiliCompressor.with(this).compress(allImage[i]);
                allImage[i] = filePath;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);

            // Добавляем выбранное фото пользователя
            Glide.with(getApplicationContext())
                    .load(images.get(0).getPath())
                    .bitmapTransform(new RoundedCornersTransformation(this,30,20))
                    .into(currentImageView);

            // Убираем шаблон человека
            switch (currentImageView.getId()){
                case R.id.image_after_front1_back:
                    allImageShapeAfter.get(0).setVisibility(View.GONE);
                    imgPathAfter[0]=images.get(0).getPath();
                    break;
                case R.id.image_after_front2_back:
                    allImageShapeAfter.get(1).setVisibility(View.GONE);
                    imgPathAfter[1]=images.get(0).getPath();
                    break;
                case R.id.image_after_left_back:
                    allImageShapeAfter.get(3).setVisibility(View.GONE);
                    imgPathAfter[3]=images.get(0).getPath();
                    break;
                case R.id.image_after_right_back:
                    allImageShapeAfter.get(4).setVisibility(View.GONE);
                    imgPathAfter[4]=images.get(0).getPath();
                    break;
                case R.id.image_after_back_back:
                    allImageShapeAfter.get(2).setVisibility(View.GONE);
                    imgPathAfter[2]=images.get(0).getPath();
                    break;
                case R.id.image_before_front1:
                    allImageShapeBefore.get(0).setVisibility(View.GONE);
                    imgPathBefore[0]=images.get(0).getPath();
                    break;
                case R.id.image_before_front2:
                    allImageShapeBefore.get(1).setVisibility(View.GONE);
                    imgPathBefore[1]=images.get(0).getPath();
                    break;
                case R.id.image_before_back:
                    allImageShapeBefore.get(2).setVisibility(View.GONE);
                    imgPathBefore[2]=images.get(0).getPath();
                    break;
                case R.id.image_before_left:
                    allImageShapeBefore.get(3).setVisibility(View.GONE);
                    imgPathBefore[3]=images.get(0).getPath();
                    break;
                case R.id.image_before_right:
                    allImageShapeBefore.get(4).setVisibility(View.GONE);
                    imgPathBefore[4]=images.get(0).getPath();
                    break;
            }
        }
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
