package com.days35.days35app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.UserAddInfo;
import com.days35.days35app.retrofit.UserAddPhoto;
import com.days35.days35app.transform.RoundImageTransform;
import com.iceteck.silicompressorr.SiliCompressor;
import com.nguyenhoanglam.imagepicker.activity.ImagePicker;
import com.nguyenhoanglam.imagepicker.activity.ImagePickerActivity;
import com.nguyenhoanglam.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Darkmonk on 29.08.2016.
 */
public class UserAddInfoActivity extends AppCompatActivity implements View.OnKeyListener {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    //EditText
    @BindView(R.id.edit_name) EditText nameField;
    @BindView(R.id.edit_surname) EditText surnameField;
    @BindView(R.id.edit_email) EditText emailField;
    @BindView(R.id.edit_phone) EditText phoneField;
    // Spinner
    @BindView(R.id.spinner_sex) Spinner spinnerSex;
    @BindView(R.id.spinner_age) Spinner spinnerAge;
    @BindView(R.id.spinner_height) Spinner spinnerHeight;
    @BindView(R.id.spinner_weight) Spinner spinnerWeight;
    @BindView(R.id.spinner_volume_breast) Spinner spinnerBreastVolume;
    @BindView(R.id.spinner_volume_waist) Spinner spinnerWaistVolume;
    @BindView(R.id.spinner_volume_hips) Spinner spinnerHipsVolume;
    @BindView(R.id.spinner_volume_legs) Spinner spinnerLegsVolume;
    @BindView(R.id.spinner_volume_arms) Spinner spinnerArmsVolume;
    // ImageView
    @BindView(R.id.image_user_photo) ImageView imageUserPhoto;
    // Примитивы
    ProgressDialog alertDialog;
    int totalCompress=800;
    public static final int REQUEST_CODE_PICKER=1;
    UserAddPhoto userAddPhoto;
    UserAddInfo userAddInfo;
    String userImagePath="";
    int userId;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_add_info);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        Optimization optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Наполняем спинеры
        setSpinnerInfo();

        // Получаем токен пользователя и id
        AuthorizationObject authObject = Paper.book("user").read("userInfo");
        token = "Bearer " + Paper.book().read("token");
        userId = authObject.id;

        // Получаем тип входа
        String enterType=getIntent().getStringExtra("enterType");

        // Если есть эмейл, то забиваем его в поле и запрещаем редактировать
        if(enterType.equals("common")) {
            emailField.setText(authObject.email);
            emailField.setFocusable(false);
        }else{
            if(authObject.email!=null){
                emailField.setText(authObject.email);
            }
        }

        // Если есть Имя, фамилия и фотка, то забиваем в ячейки
        if(authObject.firstName!=null){
            nameField.setText(authObject.firstName);
        }
        if(authObject.lastName!=null){
            surnameField.setText(authObject.lastName);
        }
        if(authObject.userPic!=null){

        }

        // Подключаем ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Запрос на добавление фото пользователя
        userAddPhoto = retrofit.create(UserAddPhoto.class);

        // Запрос на добавление информации о пользователе
        userAddInfo = retrofit.create(UserAddInfo.class);

        // Листнеры для Эдит текстов
        nameField.setOnKeyListener(this);
        surnameField.setOnKeyListener(this);
        phoneField.setOnKeyListener(this);
    }

    // Переход на предыдущий экран
    public void backClick(View v){
        // Обнуляем сохранённые данные
        // Сбрасываем токен
        Paper.book().write("token", "");
        // Сбрасываем все данные пользователя
        Paper.book().write("emailSaved","");
        Paper.book().write("passwordSaved","");

        Paper.book("user").delete("userInfo");
        Paper.book().delete("autoAuth");


        startActivity(new Intent(this,MainActivity.class));
    }

    // Отправляем фотографию пользователя в базу на сервере
    public void updateUserInfo(View v){
        if(!checkFieldsValidation()){
            return;
        }

        if(userImagePath.equals("")){
            createToast(getResources().getString(R.string.error_image_disable));
            return;
        }

        //File creating from selected URL
        File file=null;
        String filePathCompress = SiliCompressor.with(this).compress(userImagePath);
        try {
            file = new File(filePathCompress);

        }catch (Exception e){
            Log.d("КартинкаОшибка",e.toString());
            return;
        }

        // ПРогресс загрузки на сервер
        createProgressAlert(false);
        // Загружаем нужную картинку на сервер
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // Формируем тело для файлы
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("user_pic", file.getName(), requestFile);

        // Отправляем файл на сервер
        Call<Void> resultImageUpload = userAddPhoto.uploadImage(body, token);

        resultImageUpload.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                createProgressAlert(true);
                Log.d("Запрос1", String.valueOf(response.code()));
                if(response.code()==200){
                    updateUserStats();
                    createProgressAlert(false);
                    Log.d("Картинка", "1этап");
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("КартинкаОшибка1",t.toString());
            }
        });
    }


    // Обновляем информацию о пользователе на сервере
    public void updateUserStats(){

        // Составляем тело запроса
        String firstName=nameField.getText().toString();
        String lastName=surnameField.getText().toString();
        String email=emailField.getText().toString();
        String phone=phoneField.getText().toString();
        int sex=spinnerSex.getSelectedItemPosition();
        int weight=spinnerWeight.getSelectedItemPosition()+39;
        int height=spinnerHeight.getSelectedItemPosition()+129;
        int age=spinnerAge.getSelectedItemPosition()+15;
        int chest=spinnerBreastVolume.getSelectedItemPosition()+59;
        int waist=spinnerWaistVolume.getSelectedItemPosition()+39;
        int hips=spinnerHipsVolume.getSelectedItemPosition()+59;
        int legs=spinnerLegsVolume.getSelectedItemPosition()+19;
        int arms=spinnerArmsVolume.getSelectedItemPosition()+19;

        // Компануем объект
        final AuthorizationObject userProfile=new AuthorizationObject();
        userProfile.id=userId;
        userProfile.firstName=firstName;
        userProfile.lastName=lastName;
        userProfile.userName=firstName+" "+lastName;
        userProfile.phone=phone;
        userProfile.email=email;
        userProfile.sex=sex;
        userProfile.weight=weight;
        userProfile.height=height;
        userProfile.age=age;
        userProfile.chest=chest;
        userProfile.waist=waist;
        userProfile.hips=hips;
        userProfile.legs=legs;
        userProfile.arms=arms;

        // Отправляем запрос на сервер
        Call<Void> getUpdateInfoSuccess = userAddInfo.updateUser(String.valueOf(userId),
                token,
                firstName,
                lastName,
                email,
                phone,
                height,
                age,
                sex,
                weight,
                chest,
                waist,
                hips,
                legs,
                arms);

        getUpdateInfoSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                createProgressAlert(true);
                Log.d("Картинка2", String.valueOf(response.code()));
                Log.d("Картинка2", String.valueOf(response.message()));
                if(response.code()==200){
                    // Сохраняем в базу все параметры пользователя
                    Paper.book("user").write("userInfo",userProfile);
                    startActivity(new Intent(UserAddInfoActivity.this,UserAddPhotoActivity.class));
                    Log.d("Картинка", "2этап");
                    finish();
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("КартинкаОшибка2",t.toString());
            }
        });


    }

    // Проверка валидности всех полей
    public boolean checkFieldsValidation(){

        // Поле Имя
        if(nameField.getText().toString().equals("")){
            createToast(getResources().getString(R.string.error_name_empty));
            return false;
        }

        // Поле Фамилия
        if(surnameField.getText().toString().equals("")){
            createToast(getResources().getString(R.string.error_surname_empty));
            return false;
        }

        // Поле E-mail
        if(!emailField.getText().toString().contains("@")&&
                !emailField.getText().toString().contains(".")){
            createToast(getResources().getString(R.string.error_email_empty));
            return false;
        }

        // Поле Телефон
        if(phoneField.getText().toString().length()!=12){
            createToast(getResources().getString(R.string.error_phone_empty));
            return false;
        }

        // Поле Пол
        if(spinnerSex.getSelectedItemPosition()==0){
            createToast(getResources().getString(R.string.error_sex_select));
            return false;
        }

        // Поле Возраст
        if(spinnerAge.getSelectedItemPosition()==0){
            createToast(getResources().getString(R.string.error_age_select));
            return false;
        }

        // Поле Вес
        if(spinnerWeight.getSelectedItemPosition()==0){
            createToast(getResources().getString(R.string.error_weight_select));
            return false;
        }

        // Поле Рост
        if(spinnerHeight.getSelectedItemPosition()==0){
            createToast(getResources().getString(R.string.error_height_select));
            return false;
        }

        // Поле Объем груди
        if(spinnerBreastVolume.getSelectedItemPosition()==0){
            createToast(getResources().getString(R.string.error_chest_select));
            return false;
        }

        return true;
    }

    // Добавляем фотографию пользователя
    public void addUserPhoto(View v){
        // ImagePicker
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .folderTitle("Folder") // folder selection title
                .imageTitle("Tap to select") // image selection title
                .single() // single mode
                .start(REQUEST_CODE_PICKER); // start image picker activity with request code
    }

    // Наполняем спинеры информацией
    public void setSpinnerInfo(){
        // Пол
        ArrayAdapter<String> adapterSex = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                getResources().getStringArray(R.array.sex));
        spinnerSex.setAdapter(adapterSex);

        // Возраст
        String ageArray[]=new String[51];
        for (int i=0;i<ageArray.length;i++){
            ageArray[i]="Возраст: "+String.valueOf(i+15);
            if(i==0){
                ageArray[i]="Возраст";
            }
        }

        ArrayAdapter<String> adapterAge = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                ageArray);
        spinnerAge.setAdapter(adapterAge);

        // Рост
        String heightArray[]=new String[122];
        for (int i=0;i<heightArray.length;i++){
            heightArray[i]="Рост: "+String.valueOf(i+129)+" см";
            if(i==0){
                heightArray[i]="Рост";
            }
        }

        ArrayAdapter<String> adapterHeight = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                heightArray);
        spinnerHeight.setAdapter(adapterHeight);

        // Вес
        String weightArray[]=new String[212];
        for (int i=0;i<weightArray.length;i++){
            weightArray[i]="Вес: "+String.valueOf(i+39)+" кг";
            if(i==0){
                weightArray[i]="Вес";
            }
        }

        ArrayAdapter<String> adapterWeight = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                weightArray);
        spinnerWeight.setAdapter(adapterWeight);

        // Объём груди
        String breastVolumeArray[]=new String[242];
        for (int i=0;i<breastVolumeArray.length;i++){
            breastVolumeArray[i]="Объем груди: "+String.valueOf(i+59)+" см";
            if(i==0){
                breastVolumeArray[i]="Объем груди";
            }

        }

        ArrayAdapter<String> adapterBreastVolume = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                breastVolumeArray);
        spinnerBreastVolume.setAdapter(adapterBreastVolume);

        // Объём Талии
        String waistArray[]=new String[262];
        for (int i=0;i<waistArray.length;i++){
            waistArray[i]="Объем талии: "+String.valueOf(i+39)+" см";
            if(i==0){
                waistArray[i]="Объем талии";
            }
        }

        ArrayAdapter<String> adapterWaist = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                waistArray);
        spinnerWaistVolume.setAdapter(adapterWaist);

        // Объем бедер
        String hipsArray[]=new String[242];
        for (int i=0;i<hipsArray.length;i++){
            hipsArray[i]="Объем бедер: "+String.valueOf(i+62)+" см";
            if(i==0){
                hipsArray[i]="Объем бедер";
            }
        }

        ArrayAdapter<String> adapterHips = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                hipsArray);
        spinnerHipsVolume.setAdapter(adapterHips);

        // Объем ноги
        String legsArray[]=new String[182];
        for (int i=0;i<legsArray.length;i++){
            legsArray[i]="Объем ноги: "+String.valueOf(i+19)+" см";
            if(i==0){
                legsArray[i]="Объем ноги";
            }
        }

        ArrayAdapter<String> adapterLegs = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                legsArray);
        spinnerLegsVolume.setAdapter(adapterLegs);

        // Объем руки
        String armsArray[]=new String[182];
        for (int i=0;i<armsArray.length;i++){
            armsArray[i]="Объем руки: "+String.valueOf(i+19)+" см";
            if(i==0){
                armsArray[i]="Объем руки";
            }
        }

        ArrayAdapter<String> adapterArms = new ArrayAdapter<String>(this,
                R.layout.item_spinner,
                armsArray);
        spinnerArmsVolume.setAdapter(adapterArms);
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Конструктор Прогресса загрузки
    public void createProgressAlert(boolean isVisible){
        if(isVisible){
            alertDialog.dismiss();
        }else {
            alertDialog = ProgressDialog.show(this, "",
                    "Идёт загрузка...", true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(ImagePickerActivity.INTENT_EXTRA_SELECTED_IMAGES);

            // Добавляем выбранное фото пользователя
            Glide.with(getApplicationContext())
                    .load(images.get(0).getPath())
                    .fitCenter()
                    .transform(new RoundImageTransform(this))
                    .into(imageUserPhoto);
            userImagePath=images.get(0).getPath();

        }
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        if (keyCode == EditorInfo.IME_ACTION_DONE) {

            View view = this.getCurrentFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            return true;
        }
        return false;
    }
}
