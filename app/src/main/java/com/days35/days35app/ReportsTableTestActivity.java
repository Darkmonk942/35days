package com.days35.days35app;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.days35.days35app.objects.reports.ReportTestsObject;
import com.days35.days35app.objects.reports.ReportsTestsBodyObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetReportsTests;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 29.11.2016.
 */

public class ReportsTableTestActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_table) LinearLayout linearTable;
    //TextView
    @BindView(R.id.text_alert) TextView textAlert;
    // Примитивы
    ArrayList<String> tableType=new ArrayList();
    Retrofit retrofit;
    GetReportsTests getReportsTests;
    String token;
    Optimization optimization;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports_table_test);
        ButterKnife.bind(this);

        // Оптимизация элементов интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем токен из базы
        token = "Bearer " + Paper.book().read("token");

        // Подключаем ретрофит и формируем запросы
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Запрос на получение результатов тренировок
        getReportsTests = retrofit.create(GetReportsTests.class);

        // Получаем массивы отчётов по тренировкам
        getUserTrainStats();
    }

    // Переход на предыдущий экран
    public void backClick(View v){
        finish();
    }

    // Получаем с сервера массив тренировок с тестами
    public void getUserTrainStats(){
        Call<ReportTestsObject> getStatsSuccess = getReportsTests.getReportsTests(token);

        getStatsSuccess.enqueue(new Callback<ReportTestsObject>() {
            @Override
            public void onResponse(Call<ReportTestsObject> call, Response<ReportTestsObject> response) {
                Log.d("ТренировкиСтаты", String.valueOf(response.code()));
                if(response.code()==200){
                    // Создаём таблицу для тестов
                    createTestTable(response.body());
                }

            }
            @Override
            public void onFailure(Call<ReportTestsObject> call, Throwable t) {

            }
        });
    }

    // Создаём таблицу тестов
    public void createTestTable(ReportTestsObject reportTestsObject){
        ArrayList<ReportsTestsBodyObject> allTests=reportTestsObject.weekStats;

        textAlert.setText(reportTestsObject.alertText);

        // Создаём лист названий
        tableType.add(getResources().getString(R.string.reports_table_berpi));
        tableType.add(getResources().getString(R.string.reports_table_vipadi));
        tableType.add(getResources().getString(R.string.reports_table_otzhim));
        tableType.add(getResources().getString(R.string.reports_table_hyper));
        tableType.add(getResources().getString(R.string.reports_table_skalolazi));
        tableType.add(getResources().getString(R.string.reports_table_press));

        for (int i=0;i<tableType.size();i++) {
            View tableTest = View.inflate(this, R.layout.item_table_test, null);
            TextView rawName=(TextView)tableTest.findViewById(R.id.raw_name);
            TextView rawText1=(TextView)tableTest.findViewById(R.id.raw_text1);
            TextView rawText2=(TextView)tableTest.findViewById(R.id.raw_text2);

            for (int j=0;j<allTests.size();j++){
                if(j==0){
                    switch (i){
                        case 0: rawText1.setText(String.valueOf(allTests.get(j).burpee));
                            break;
                        case 1: rawText1.setText(String.valueOf(allTests.get(j).dropOut));
                            break;
                        case 2: rawText1.setText(String.valueOf(allTests.get(j).pushUp));
                            break;
                        case 3: rawText1.setText(String.valueOf(allTests.get(j).hyperextension));
                            break;
                        case 4: rawText1.setText(String.valueOf(allTests.get(j).climb));
                            break;
                        case 5: rawText1.setText(String.valueOf(allTests.get(j).press));
                            break;
                    }
                }else{
                    switch (i){
                        case 0: rawText2.setText(String.valueOf(allTests.get(j).burpee));
                            break;
                        case 1: rawText2.setText(String.valueOf(allTests.get(j).dropOut));
                            break;
                        case 2: rawText2.setText(String.valueOf(allTests.get(j).pushUp));
                            break;
                        case 3: rawText2.setText(String.valueOf(allTests.get(j).hyperextension));
                            break;
                        case 4: rawText2.setText(String.valueOf(allTests.get(j).climb));
                            break;
                        case 5: rawText2.setText(String.valueOf(allTests.get(j).pushUp));
                            break;
                    }
                }
            }

            rawName.setText(tableType.get(i));

            optimization.OptimizationLinear((LinearLayout)tableTest);

            linearTable.addView(tableTest);

        }

    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
