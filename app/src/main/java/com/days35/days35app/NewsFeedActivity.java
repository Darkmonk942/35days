package com.days35.days35app;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.objects.AuthorizationObject;
import com.days35.days35app.objects.LikeViewObject;
import com.days35.days35app.objects.PostListObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.DeletePost;
import com.days35.days35app.retrofit.DownloadFile;
import com.days35.days35app.retrofit.GetNewsFeed;
import com.days35.days35app.retrofit.LikePost;
import com.days35.days35app.retrofit.PostPushToken;
import com.days35.days35app.retrofit.PostSharing;
import com.days35.days35app.sharing.FacebookSharing;
import com.days35.days35app.sharing.InstagramSharing;
import com.days35.days35app.sharing.VkSharing;
import com.days35.days35app.transform.RoundImageTransform;
import com.facebook.CallbackManager;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerSimple;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static io.paperdb.Paper.book;

/**
 * Created by Darkmonk on 07.09.2016.
 */
public class NewsFeedActivity extends AppCompatActivity implements View.OnClickListener {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_post_list) LinearLayout linearList;
    // ScrollView
    @BindView(R.id.post_scroll) ScrollView postsScroll;
    // ToolBar
    @BindView(R.id.toolbar_menu) Toolbar toolbarMenu;
    // SwipeRefresh
    @BindView(R.id.swipe_refresh) SwipeRefreshLayout swipeRefresh;
    // Button
    @BindView(R.id.btn_floating) FloatingActionsMenu floatingBtn;
    // Примитивы
    FragmentManager fragmentManager;
    CallbackManager callbackManager;
    AuthorizationObject userProfile;
    PostSharing postSharing;
    DownloadFile downloadFile;
    PostPushToken postPushToken;
    Optimization optimization;
    int totalScrollHeight=0;
    int currentPageCount=1;
    int totalPageCount=0;
    int currentPost=-1;
    GetNewsFeed getNewsFeed;
    DeletePost deletePost;
    LikePost likePost;
    public static ArrayList<PostListObject> allNewsList=new ArrayList();
    ArrayList<PostListObject> moreItemsList =new ArrayList();
    ArrayList<View> allPostView=new ArrayList();
    ArrayList<Pair<ImageView,JCVideoPlayerSimple>> allVideos=new ArrayList();
    Pair<TextView,PostListObject> sharePairCurrent;
    String token;
    int tenPxHeight;
    ImageView imageSharing;
    boolean isAvailableNewItems=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_feed);
        ButterKnife.bind(this);

        // Оптимизация интерфейса
        optimization=new Optimization();
        optimization.Optimization(mainFrame);
        int height= book().read("height");
        tenPxHeight=height/192;

        // Фрагмент для шаринга
        fragmentManager = getFragmentManager();

        // Колбек от Фесбук
        callbackManager = CallbackManager.Factory.create();

        // Получаем текущего пользователя
        userProfile= book("user").read("userInfo",new AuthorizationObject());

        // Добавляем Дроер
        NavigationDrawer navigationDrawer=new NavigationDrawer();
        navigationDrawer.NavigationDrawer(this,toolbarMenu,1);

        // Получаем токен из базы
        token="Bearer "+ book().read("token");

        // Подключаем Ретрофит
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getNewsFeed = retrofit.create(GetNewsFeed.class);
        deletePost=retrofit.create(DeletePost.class);
        likePost=retrofit.create(LikePost.class);
        postPushToken=retrofit.create(PostPushToken.class);
        postSharing=retrofit.create(PostSharing.class);
        downloadFile=retrofit.create(DownloadFile.class);

        // Листнер для Скрола
        postsScroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = postsScroll.getScrollY();
                if(scrollY == 0){
                    swipeRefresh.setEnabled(true);
                }
                else{ swipeRefresh.setEnabled(false);}

                // Проверка на подгрузку контента, когда скролл
                if(scrollY>=totalScrollHeight-totalScrollHeight/3&&isAvailableNewItems){
                    isAvailableNewItems=false;
                    currentPageCount=currentPageCount+1;
                    getNewsFromServer(currentPageCount,false);
                }

            }
        });

        // Получаем список постов (начиная с первой страницы)
        getNewsFromServer(1,true);

        refreshListener();
        swipeRefresh.setEnabled(false);

        // Получаем токен в GCM
        getGCMToken();

    }

    // Получаем список новостей с сервера
    public void getNewsFromServer(int page,final boolean firstPage){

        // Отправляем запрос на получение списка постов
        Call<List<PostListObject>> getPostsSuccess = getNewsFeed.getPosts(page,20,token);

        getPostsSuccess.enqueue(new Callback<List<PostListObject>>() {
            @Override
            public void onResponse(Call<List<PostListObject>> call, Response<List<PostListObject>> response) {

                swipeRefresh.setEnabled(true);
                swipeRefresh.setRefreshing(false);

                if(firstPage) {
                    // Заполняем лист постов информацией с сервера
                    for (int i = 0; i < response.body().size(); i++) {
                        PostListObject postListObject = response.body().get(i);
                        allNewsList.add(postListObject);
                    }

                    // Узнаём окончание постов
                    totalPageCount = Integer.valueOf(response.headers().get("X-Pagination-Page-Count"));

                    // Чистим старый контент
                    linearList.removeAllViews();

                    // Создаём лист
                    getInfoForList(true);

                }else {
                    // Получаем по пагинации новые посты
                    moreItemsList.clear();
                    for (int i = 0; i < response.body().size(); i++) {
                        PostListObject postListObject = response.body().get(i);
                        moreItemsList.add(postListObject);
                    }

                    // Добавляем новые посты в текущий лист
                    //allNewsList.addAll(moreItemsList);

                    // Дополняем лист
                    getInfoForList(false);

                }
            }

            @Override
            public void onFailure(Call<List<PostListObject>> call, Throwable t) {
                swipeRefresh.setEnabled(true);
                swipeRefresh.setRefreshing(false);
                isAvailableNewItems=true;
            }
        });
    }

    // Удаляем выбранный пост
    public void deletePost(String postId){

        Call<Void> getDeleteSuccess = deletePost.deletePost(token,postId);

        Log.d("Делит",postId);

        getDeleteSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("Делит", String.valueOf(response.code()));
                if(response.code()==200){
                    swipeRefresh.setEnabled(false);
                    isAvailableNewItems=true;
                    allNewsList.clear();
                    currentPageCount=1;
                    getNewsFromServer(1,true);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    // лайкаем выбранный пост
    public void likePostRequest(final String postId, final boolean isLiked,
                                final ImageView likeIcon, final TextView likeCount){
        Call<Void> getLikeSuccess = likePost.sendLike(token,postId);

        getLikeSuccess.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("лайк", String.valueOf(response.code()));
                LikeViewObject likeViewObject=(LikeViewObject)likeIcon.getTag();
                if(response.code()==200){
                    // Узнаём текущее кол-во лайков
                    int likes=Integer.valueOf(likeCount.getText().toString());
                    if(isLiked){
                        likeIcon.setImageResource(R.drawable.ic_post_like);
                        likeCount.setText(String.valueOf(likes-1));
                        likeViewObject.isLiked=false;
                        likeCount.setTextColor(getResources().getColor(R.color.colorGreyPostText));
                    }else{
                        likeIcon.setImageResource(R.drawable.ic_like_red);
                        likeCount.setText(String.valueOf(likes+1));
                        likeViewObject.isLiked=true;
                        likeCount.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                    likeCount.setTag(likeViewObject); // Меняем таг после изменений

                    // Сохраняем изменения
                    for (int i=0;i<allNewsList.size();i++){
                        if(allNewsList.get(i).id==Integer.valueOf(postId)){
                            allNewsList.get(i).isLiked=likeViewObject.isLiked;
                            allNewsList.get(i).likeCount=Integer.valueOf(likeCount.getText().toString());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }


    // Добавить Текстовый пост
    public void addPostText(View v){
        startActivity(new Intent(this, CreatPostActivity.class).putExtra("postType","text"));
        floatingBtn.collapse();
    }

    // Добавить Фото-пост
    public void addPostPhoto(View v){
        startActivity(new Intent(this, CreatPostActivity.class).putExtra("postType","photo"));
        floatingBtn.collapse();
    }

    // Добавить Видео-пост
    public void addPostVideo(View v){
        startActivity(new Intent(this, CreatPostActivity.class).putExtra("postType","video"));
        floatingBtn.collapse();
    }


    // Образотка PulltoRefresh
    public void refreshListener(){
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                isAvailableNewItems=true;
                swipeRefresh.setEnabled(false);
                allNewsList.clear();
                currentPageCount=1;
                getNewsFromServer(1,true);
                // Подгружаем заново все элементы
            }
        });

    }

    // Получаем инфу с баз и создаём лист
    public void getInfoForList(boolean isFirstPage){
        if(isFirstPage) {
            for (int i = 0; i < allNewsList.size(); i++) {
                createLinearList(i,allNewsList);
            }
        }else{
            for (int i = 0; i < moreItemsList.size(); i++) {
                createLinearList(i,moreItemsList);
                isAvailableNewItems=true;
            }
        }

        totalScrollHeight=postsScroll.getChildAt(0).getMeasuredHeight();
        Log.d("Длинна Скрола",String.valueOf(postsScroll.getChildAt(0).getMeasuredHeight()));
    }

    // Создаём список Лайнером
    public void createLinearList(int position,ArrayList<PostListObject> currentPostList){
        PostListObject postListObject=currentPostList.get(position);
        String postType="";

        // Создаём пост из айтема
        View postView=null;

        TextView postText=null;
        ImageView postImage=null;
        ImageView startVideoIc=null;

        // Проверка на тип поста
        switch (postListObject.postType){
            case 1: postType="text";
                postView=View.inflate(this,R.layout.item_newsfeed__post,null);
                postText = (TextView)postView.findViewById(R.id.post_text_inner);
                break;
            case 2: postType="photo";
                postView=View.inflate(this,R.layout.item_newsfeed_photo,null);
                postImage=(ImageView)postView.findViewById(R.id.post_image);
                imageSharing=postImage;
                break;
            case 3: postType="video";
                postView=View.inflate(this,R.layout.item_newsfeed_video,null);
                startVideoIc=(ImageView)postView.findViewById(R.id.img_play);
                break;
            default:
                break;
        }

        // Объявляем переменные
        TextView userName = (TextView)postView.findViewById(R.id.text_user_name);
        TextView postDate = (TextView)postView.findViewById(R.id.text_date_post);
        TextView likeCount = (TextView)postView.findViewById(R.id.like_count);
        TextView messageCount = (TextView)postView.findViewById(R.id.message_count);
        TextView shareCount = (TextView)postView.findViewById(R.id.share_count);

        LinearLayout linearContent=(LinearLayout)postView.findViewById(R.id.linear_content) ;

        Button fullPost=(Button)postView.findViewById(R.id.btn_full_post);
        fullPost.setOnClickListener(this);

        ImageView userIcon=(ImageView)postView.findViewById(R.id.ic_user);
        ImageView likeIcon=(ImageView)postView.findViewById(R.id.ic_like);
        ImageView shareIcon=(ImageView)postView.findViewById(R.id.ic_share);

        View userIconField=(View)postView.findViewById(R.id.ic_user_field);
        userIconField.setTag(postListObject.userPost.userId);

        // ДОбавляем в Таг шаринга пару Кол-во - объект
        Pair<TextView,PostListObject> sharePair=new Pair(shareCount,postListObject);
        shareIcon.setTag(sharePair);
        shareIcon.setClickable(true);
        shareIcon.setOnClickListener(this);
        userIconField.setOnClickListener(this);

        // Добавляем в таг лайка всю информацию о нём
        LikeViewObject likeViewObject=new LikeViewObject();
        likeViewObject.postId=postListObject.id;
        likeViewObject.isLiked=postListObject.isLiked;
        likeViewObject.likeCount=likeCount;
        likeIcon.setTag(likeViewObject);
        likeIcon.setOnClickListener(this);

        ImageButton popUpBtn=(ImageButton) postView.findViewById(R.id.ic_popup);

        // Помечаем текущий пост и лайк
        fullPost.setTag(position);

        // Проверяем, лайкал ли пост текущий пользователь
        if(postListObject.isLiked){
            likeIcon.setImageResource(R.drawable.ic_like_red);
        }

        // Определяем возможность его удаления текущим юзером
        if(userProfile.id==postListObject.userPost.userId){
            popUpBtn.setVisibility(View.VISIBLE);
            popUpBtn.setOnClickListener(this);
            popUpBtn.setTag(postListObject.id);
        }

        // Оптимизируем карточку
        optimization.Optimization((FrameLayout)postView);
        optimization.OptimizationLinear(linearContent);

        // Сохраняем в лист пост
        allPostView.add(postView);

        // Заполняем поля информацией
        userName.setText(postListObject.userPost.firstName+" "+postListObject.userPost.lastName);
        postDate.setText(postListObject.date);
        likeCount.setText(String.valueOf(postListObject.likeCount));
        messageCount.setText(String.valueOf(postListObject.commentCount));
        shareCount.setText(String.valueOf(postListObject.shareCount));

        // Подгружаем иконку пользователя
        Glide.with(this)
                .load(postListObject.userPost.userPic)
                .fitCenter()
                .transform(new RoundImageTransform(this))
                .into(userIcon);

        switch (postType){
            case "text":
                postText.setText(postListObject.postText);
                break;
            case "photo":
                Glide.with(this)
                        .load(postListObject.postPic)
                        .diskCacheStrategy( DiskCacheStrategy.ALL )
                        .into(postImage);
                break;
            case "video":
                JCVideoPlayerSimple videoPlayer=(JCVideoPlayerSimple )postView.findViewById(R.id.video_player);

                startVideoIc.setTag(postListObject.postVideo);
                Pair<ImageView,JCVideoPlayerSimple> videoPart=new Pair(startVideoIc,videoPlayer);
                allVideos.add(videoPart);
                break;
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, tenPxHeight*3,0, 0);
        linearList.addView(postView,layoutParams);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ic_popup: // удаление текущего поста
                deletePost(v.getTag().toString());
                break;
            case R.id.btn_full_post: // Переход на полный пост
                startActivity(new Intent(this,FullPost.class).putExtra("post",v.getTag().toString()));
                currentPost=Integer.valueOf(v.getTag().toString());
                break;
            case R.id.ic_like: // Лайк выбранного поста
                // Запрос на отправление лайка на сервер
                LikeViewObject likeViewObject=(LikeViewObject) v.getTag();
                likePostRequest(String.valueOf(likeViewObject.postId),
                                likeViewObject.isLiked,
                                (ImageView)v,
                                likeViewObject.likeCount);
                break;
            case R.id.ic_share: // Шаринг выбранного поста

                sharePairCurrent=(Pair<TextView,PostListObject>)v.getTag();

                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.frame_alert_sharing);
                dialog.setTitle("35 Days");

                FrameLayout instagramFrame=(FrameLayout)dialog.findViewById(R.id.frame_alert_instagram);

                if(sharePairCurrent.second.postType==1){
                    instagramFrame.setVisibility(View.GONE);
                }

                dialog.show();
                break;
            case R.id.ic_user_field:
                startActivity(new Intent(this,UserProfileActivity.class)
                        .putExtra("currentUser",false)
                        .putExtra("userId",Integer.valueOf(v.getTag().toString())));
                break;
                }
    }

    // Включение видео
    public void startVideoClick(View v){
        ImageView image=(ImageView)v;
        // Перебираем все видео
        for (int i=0;i<allVideos.size();i++){
            if(image.equals(allVideos.get(i).first)){
                image.setVisibility(View.GONE);
                allVideos.get(i).second.setVisibility(View.VISIBLE);
                allVideos.get(i).second.setUp(image.getTag().toString()
                        , JCVideoPlayerSimple.SCREEN_LAYOUT_LIST);
                allVideos.get(i).second.startButton.callOnClick();
            }
        }
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Клик в Диалоге при выборе соц-сети для шаринга
    public void alertShareClick(View v){
        switch (v.getId()){
            case R.id.text_alert_vk:
                shareCurrentPost("vk");
                break;
            case R.id.text_alert_fb:
                shareCurrentPost("facebook");
                break;
            case R.id.text_alert_instagram:
                shareCurrentPost("instagram");
                break;
        }
    }

    // Обработка диалога шаринга
    public void shareCurrentPost(String type) {
        switch (type) {
            case "vk":
                VkSharing vkSharing = new VkSharing();
                FragmentManager fragmentManager = getFragmentManager();

                switch (sharePairCurrent.second.postType) {
                    case 1:
                        vkSharing.vkTextShare(token, sharePairCurrent.second.id, this,
                                fragmentManager, 1, "", "35 Дней", postSharing, sharePairCurrent.first);
                        break;
                    case 2:
                        vkSharing.vkTextShare(token, sharePairCurrent.second.id, this,
                                fragmentManager, 2, sharePairCurrent.second.postPic, "35 Дней", postSharing, sharePairCurrent.first);
                        break;
                    case 3:
                        vkSharing.vkTextShare(token, sharePairCurrent.second.id, this,
                                fragmentManager, 3, sharePairCurrent.second.postVideo, "35 Дней", postSharing, sharePairCurrent.first);
                        break;
                }
                break;
            case "facebook":
                FacebookSharing facebookSharing = new FacebookSharing();

                switch (sharePairCurrent.second.postType) {
                    case 1:
                        facebookSharing.facebookSharing(this, callbackManager, token, sharePairCurrent.second.id, "",
                                postSharing, sharePairCurrent.first, 1);
                        break;
                    case 2:
                        facebookSharing.facebookSharing(this, callbackManager, token,
                                sharePairCurrent.second.id, sharePairCurrent.second.postPic,
                                postSharing, sharePairCurrent.first, 2);
                        break;
                    case 3:
                        facebookSharing.facebookSharing(this, callbackManager, token,
                                sharePairCurrent.second.id, sharePairCurrent.second.postVideo,
                                postSharing, sharePairCurrent.first, 3);
                        break;
                }
                break;
            case "instagram":
                InstagramSharing instagramSharing=new InstagramSharing();
                switch (sharePairCurrent.second.postType) {
                    case 2:
                        instagramSharing.startSharing(this, callbackManager, token,
                                sharePairCurrent.second.id, sharePairCurrent.second.postPic,
                                postSharing, sharePairCurrent.first, 2);
                        break;
                    case 3:
                        instagramSharing.startSharing(this, callbackManager, token,
                                sharePairCurrent.second.id, sharePairCurrent.second.postVideo,
                                postSharing, sharePairCurrent.first, 3);
                        break;

                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Получаем изменённый пост

        if(currentPost!=-1){
            PostListObject saveObject=allNewsList.get(currentPost);
            // Находим нужный пост
            for (int i=0;i<allNewsList.size();i++){
                if(saveObject.id==allNewsList.get(i).id){
                    View postView=allPostView.get(i);
                    Log.d("ЛайкиПолучаем_i", String.valueOf(i));

                    // Меняем лайки и комментарии в текущем посте
                    TextView likeCount = (TextView)postView.findViewById(R.id.like_count);
                    TextView messageCount = (TextView)postView.findViewById(R.id.message_count);
                    TextView shareCount = (TextView)postView.findViewById(R.id.share_count);
                    ImageView likeIcon=(ImageView)postView.findViewById(R.id.ic_like);

                    likeCount.setText(String.valueOf(saveObject.likeCount));
                    messageCount.setText(String.valueOf(saveObject.commentCount));
                    shareCount.setText(String.valueOf(saveObject.shareCount));

                    if(saveObject.isLiked){
                        likeIcon.setImageResource(R.drawable.ic_like_red);
                    }else{
                        likeIcon.setImageResource(R.drawable.ic_post_like);
                    }
                }
            }
        }

    }

    // Получаем токен для пушей
    public void getGCMToken(){
        final Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                    InstanceID instanceID = InstanceID.getInstance(NewsFeedActivity.this);
                    try {
                        String token = instanceID.getToken(getResources().getString(R.string.push_sender_id),  GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

                        Log.d("Пуш",token);
                        // Отправляем токен на сервер
                        if(!token.equals(Paper.book().read("pushToken",""))){
                            Paper.book().write("pushToken",token);
                            sendPushToken(token);
                        }


                    } catch (IOException e) {
                        Log.d("Пуш",e.toString());
                        e.printStackTrace();
                    }
            }
        });

        thread.start();
    }

    // Отправляем токен на сервер
    public void sendPushToken(String pushToken){
        Call<Void> sendPushToken = postPushToken.postPushToken(token,1,pushToken);

        // Обработка полученной информации
        sendPushToken.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("ОтправкаТокена", String.valueOf(response.code()));
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
