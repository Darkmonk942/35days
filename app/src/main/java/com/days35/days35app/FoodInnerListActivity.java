package com.days35.days35app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.objects.FoodRequestObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetFoodRecipes;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 29.10.2016.
 */

public class FoodInnerListActivity extends AppCompatActivity implements View.OnClickListener {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_list) LinearLayout linearList;
    // TextView
    @BindView(R.id.text_header) TextView headerText;
    // Примитивы
    Optimization optimization;
    Retrofit retrofit;
    GetFoodRecipes getFoodRecipes;
    String currentWeek;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_inner_list);
        ButterKnife.bind(this);

        // Оптимизация интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Получаем Токен
        token="Bearer "+ Paper.book().read("token");

        // Подключаем ретрофит и формируем запросы
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getFoodRecipes =retrofit.create(GetFoodRecipes.class);

        // Получаем текущую неделю
        currentWeek=getIntent().getStringExtra("week");
        headerText.setText(currentWeek+" "+getResources().getString(R.string.trains_week));

        // Получаем весь набор еды
        getAllFood();


    }

    // Переход на предыдущий экран Списка недель
    public void backClick(View v){
        finish();
    }

    // Запрос на получение всей еды на неделю
    public void getAllFood(){
        Call<FoodRequestObject> getFoodSuccess = getFoodRecipes.getFoodRecipes(token,
                currentWeek);

        getFoodSuccess.enqueue(new Callback<FoodRequestObject>() {
            @Override
            public void onResponse(Call<FoodRequestObject> call, Response<FoodRequestObject> response) {
                Log.d("Рецепты", String.valueOf(response.code()));
                if(response.code()==200){
                    createTrainList(response.body());
                    Paper.book().write("foodRecipe",response.body());
                }else{
                    createToast(getResources().getString(R.string.error_common));
                }

            }
            @Override
            public void onFailure(Call<FoodRequestObject> call, Throwable t) {

            }
        });
    }

    // Создаём лист еды на текущую неделю
    public void createTrainList(FoodRequestObject foodRequestObject){

        for (int i=0;i<5;i++){
            View foodFrame=View.inflate(this,R.layout.item_inner_food,null);

            LinearLayout linearFood=(LinearLayout)foodFrame.findViewById(R.id.linear_food);
            ImageView imageTrain=(ImageView)foodFrame.findViewById(R.id.food_image);
            TextView foodType=(TextView)foodFrame.findViewById(R.id.food_type);
            Button but=(Button)foodFrame.findViewById(R.id.btn_start);
            but.setTag(i+1);
            but.setOnClickListener(this);

            // Меняем текст типа еды и картинку
            String foodImageURL=null;
            switch (i){
                case 0: foodType.setText(getResources().getString(R.string.food_type_breakfast));
                    foodImageURL=foodRequestObject.breakfastList.get(0).foodPic;
                    break;
                case 1: foodType.setText(getResources().getString(R.string.food_type_lunch));
                    foodImageURL=foodRequestObject.lunchList.get(0).foodPic;
                    break;
                case 2: foodType.setText(getResources().getString(R.string.food_type_dinner));
                    foodImageURL=foodRequestObject.dinnerList.get(0).foodPic;
                    break;
                case 3: foodType.setText(getResources().getString(R.string.food_type_snack));
                    foodImageURL=foodRequestObject.snikerList.get(0).foodPic;
                    break;
                case 4: foodType.setText(getResources().getString(R.string.food_type_supper));
                    foodImageURL=foodRequestObject.supperList.get(0).foodPic;
                    break;
            }

            Glide.with(this)
                    .load(foodImageURL)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageTrain);

            // Оптимизация фрейма
            optimization.Optimization((FrameLayout)foodFrame);
            optimization.OptimizationLinear(linearFood);

            linearList.addView(foodFrame);
        }
    }

    // Конструктор Тоастов
    public void createToast(String toastText){
        Toast.makeText(this, toastText,
                Toast.LENGTH_SHORT).show();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    // Переход на рецепты текущего типа еды
    @Override
    public void onClick(View v) {
        startActivity(new Intent(this,FoodRecipeActivity.class)
                .putExtra("week",currentWeek)
                .putExtra("type",v.getTag().toString()));
    }
}
