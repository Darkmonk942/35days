package com.days35.days35app;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.days35.days35app.objects.bonus.BonusTrainingObject;
import com.days35.days35app.optimization.Optimization;
import com.days35.days35app.retrofit.GetBonusTrainingVideo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerSimple;
import io.paperdb.Paper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by DarKMonK on 24.12.2016.
 */

public class BonusTrainActivity extends AppCompatActivity {
    // FrameLayout
    @BindView(R.id.main_frame) FrameLayout mainFrame;
    // LinearLayout
    @BindView(R.id.linear_list) LinearLayout linearList;
    // Player
    @BindView(R.id.video_player) JCCustomPlayer mainVideoPlayer;
    // ImageView
    @BindView(R.id.player_place) ImageView playerPlace;
    @BindView(R.id.img_play) ImageView imgPlay;
    // TextView
    @BindView(R.id.train_name) TextView trainMainName;
    @BindView(R.id.text_header) TextView textHeader;
    // ProgressBar
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    // Примитивы
    Optimization optimization;
    BonusTrainingObject currentBonusTraining;
    GetBonusTrainingVideo getBonusTrainingVideo;
    List<BonusTrainingObject> allTrains=new ArrayList();
    String bonusTrainingType;
    String token;
    boolean newVideoStarted=true;
    int playerWidth=0;
    int playerHeight=0;
    int height,width,tenPxHeight;
    int currentTrainTime=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bonus_train);
        ButterKnife.bind(this);

        mainVideoPlayer.backButton.setVisibility(View.GONE);
        mainVideoPlayer.tinyBackImageView.setVisibility(View.GONE);

        // Оптимизация элементов интерфейса
        optimization = new Optimization();
        optimization.Optimization(mainFrame);

        // Размеры экрана
        height= Paper.book().read("height", 0);
        tenPxHeight=height/192;
        width=Paper.book().read("width",0);

        // Получаем токен
        token = "Bearer " + Paper.book().read("token");

        // Тип тренирвоки
        bonusTrainingType= String.valueOf(getIntent().getIntExtra("id",0));
        textHeader.setText(getIntent().getStringExtra("title"));

        // Подключаем ретрофит и формируем запросы
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        getBonusTrainingVideo=retrofit.create(GetBonusTrainingVideo.class);

        // Получаем список тренировок с сервера
        getVideoTrainings();

        // Вешаем слушателя на прогрессБар для определния окончания тренировки
    }

    // Переход на предыдущий экран
    public void backClick(View v){
        finish();
    }

    // Получаем с сервера список видео тренировок
    public void getVideoTrainings(){
        Call<List<BonusTrainingObject>> getTrainingsSuccess = getBonusTrainingVideo.getTrainings(token,bonusTrainingType);

        getTrainingsSuccess.enqueue(new Callback<List<BonusTrainingObject>>() {
            @Override
            public void onResponse(Call<List<BonusTrainingObject>> call, Response<List<BonusTrainingObject>> response) {
                Log.d("Бонусы", String.valueOf(response.code()));
                if(response.code()==200){

                    allTrains=(ArrayList)response.body();
                    // Подгружаем все данные по полученным видео на фреймы
                    createVideoList(response.body());

                    // Отображаем первую тренирвоку
                    firstTrainingShow(response.body().get(0));
                    currentBonusTraining=allTrains.get(0);
                }

            }
            @Override
            public void onFailure(Call<List<BonusTrainingObject>> call, Throwable t) {

            }
        });
    }

    // Составляем список видео в рол
    public void createVideoList(List<BonusTrainingObject> allVideos){

        for (int i=0;i<allVideos.size();i++){
            View trainFrame=View.inflate(this,R.layout.item_bonus_video,null);
            ImageView previewImg=(ImageView)trainFrame.findViewById(R.id.img_preview);
            TextView trainNameFull=(TextView) trainFrame.findViewById(R.id.train_name_full);
            TextView trainName=(TextView) trainFrame.findViewById(R.id.train_name);

            // Подгружаем обложку тренировки
            Glide.with(this)
                    .load(allVideos.get(i).trainingPic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(previewImg);

            // название тренирвоки
            String trainNameShape=String.valueOf(i+1)+". "+allVideos.get(i).trainingTitle;

            trainName.setText(allVideos.get(i).trainingTitle);
            trainNameFull.setText(trainNameShape);
            allVideos.get(i).textPreview=trainNameShape;
            trainFrame.setTag(allVideos.get(i));


            optimization.Optimization((FrameLayout)trainFrame);

            linearList.addView(trainFrame);

        }
    }

    // Отображаем первую тренировку
    public void firstTrainingShow(BonusTrainingObject firstTrain){
        // Подгружаем обложку тренировки
        Glide.with(this)
                .load(firstTrain.trainingPic)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(playerPlace);

        trainMainName.setText(firstTrain.textPreview);
    }

    // Старт первого видео
    public void startVideoClick(View v){

        v.setVisibility(View.GONE);
        playerPlace.setVisibility(View.GONE);
        mainVideoPlayer.setVisibility(View.VISIBLE);
        mainVideoPlayer.setUp(currentBonusTraining.trainingVid
                , JCVideoPlayerSimple.SCREEN_LAYOUT_NORMAL);

        JCVideoPlayer.NORMAL_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        JCVideoPlayer.FULLSCREEN_ORIENTATION = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        mainVideoPlayer.startButton.callOnClick();

        mainVideoPlayer.setOnStopListener(new OnCustomEventListener() {
            @Override
            public void onEvent() {
                for (int i=0;i<allTrains.size();i++){
                    if(currentBonusTraining.equals(allTrains.get(i))){
                        if(i!=allTrains.size()-1){
                            Log.d("Время", String.valueOf(i));
                            currentBonusTraining=allTrains.get(i+1);
                            trainMainName.setText(currentBonusTraining.textPreview);

                            mainVideoPlayer.setUp(currentBonusTraining.trainingVid, JCVideoPlayerSimple.SCREEN_LAYOUT_NORMAL);

                            mainVideoPlayer.startButton.callOnClick();
                            newVideoStarted=true;
                            break;
                        }
                    }
                }
            }
        });

    }

    // Выбор из рола видео
    public void checkMainVideo(View v){
        FrameLayout currentFrame=(FrameLayout)v;
        currentBonusTraining=(BonusTrainingObject)currentFrame.getTag();
        trainMainName.setText(currentBonusTraining.textPreview);

        if(imgPlay.getVisibility()==View.VISIBLE){
            // Подгружаем обложку тренировки
            Glide.with(this)
                    .load(currentBonusTraining.trainingPic)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(playerPlace);


        }else{
            // Подгружаем новое видео из рола
            mainVideoPlayer.setUp(currentBonusTraining.trainingVid, JCVideoPlayerSimple.SCREEN_LAYOUT_NORMAL);
            mainVideoPlayer.startButton.callOnClick();
        }
    }

    /*// Таймер для просчёта времени тренировки
    public void startTimer(){

        myTimer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                handlerTimer.post(new Runnable() {
                    public void run() {
                            currentTrainTime=currentTrainTime+1;
                            progressBar.setProgress(currentTrainTime*10);

                        if(currentTrainTime==10){
                            // Узнаём следующее видео для воспроизведения
                            for (int i=0;i<allTrains.size();i++){
                                if(currentBonusTraining.equals(allTrains.get(i))){
                                    if(i!=allTrains.size()-1){
                                        Log.d("Время", String.valueOf(i));
                                        currentBonusTraining=allTrains.get(i+1);
                                        trainMainName.setText(currentBonusTraining.textPreview);

                                        mainVideoPlayer.setUp(currentBonusTraining.trainingVid, JCVideoPlayerSimple.SCREEN_LAYOUT_NORMAL);

                                        mainVideoPlayer.startButton.callOnClick();
                                        newVideoStarted=true;
                                        break;
                                    }
                                }
                            }

                            myTimer.cancel();
                            currentTrainTime=0;
                        }

                    }
                });
            }};

        myTimer.schedule(timerTask, 0, 1000);
    }*/

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        playerHeight=playerPlace.getMeasuredHeight();
        playerWidth=playerPlace.getMeasuredWidth();

        FrameLayout.LayoutParams newParams=new FrameLayout.LayoutParams(playerWidth,playerHeight);
        newParams.setMargins(0,tenPxHeight*14,0,0);
        //mainPlayer.setLayoutParams(newParams);
        mainVideoPlayer.setLayoutParams(newParams);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }

    // Подключение к Activity Калиграфии
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
