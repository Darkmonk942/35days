package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 25.10.2016.
 */

public interface CreateComment {
    @FormUrlEncoded
    @POST("api/comment")
    Call<Void> sendComment(@Header("Token") String token,
                          @Field("post_id") int postId,
                          @Field("comment_text") String commentText);
}
