package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 28.10.2016.
 */

public interface DeleteComment {
    @DELETE("api/comment/{id}")
    Call<Void> deleteComment(@Header("Token") String token,
                          @Path("id") String commentId);
}
