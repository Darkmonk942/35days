package com.days35.days35app.retrofit;

import com.days35.days35app.objects.bonus.BonusTrainingObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 31.12.2016.
 */

public interface GetBonusTrainingVideo {
    @GET("api/week-trainings/bonus-training/{id}")
    Call<List<BonusTrainingObject>> getTrainings(@Header("Token") String token,
                                                 @Path("id") String id);
}
