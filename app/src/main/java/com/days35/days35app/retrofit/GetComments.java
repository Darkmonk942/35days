package com.days35.days35app.retrofit;

import com.days35.days35app.objects.CommentObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by DarKMonK on 25.10.2016.
 */

public interface GetComments {
    @GET("api/post/comments/{id}")
    Call<List<CommentObject>> getComments(@Header("Token") String token,
                                          @Path("id") String postId,
                                          @Query("page") int page,
                                          @Query("per-page") int paging);
}
