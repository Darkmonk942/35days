package com.days35.days35app.retrofit;

import com.days35.days35app.objects.PhotoAfterObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 10.12.2016.
 */

public interface GetPhotoAfter {
    @GET("api/user/get-pics-after")
    Call<PhotoAfterObject> getPhotoAfter(@Header("Token") String token);
}
