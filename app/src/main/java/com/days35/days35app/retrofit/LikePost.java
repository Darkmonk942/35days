package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 29.10.2016.
 */

public interface LikePost {
    @POST("api/post/like/{id}")
    Call<Void> sendLike(@Header("Token") String token,
                           @Path("id") String postId);
}
