package com.days35.days35app.retrofit;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by DarKMonK on 10.12.2016.
 */

public interface CreateVideoReport {
    @Multipart
    @POST("api/week-trainings/video-report")
    Call<Void> sendVideoReport(@Header("Token") String token,
                               @Part MultipartBody.Part reportVideo,
                               @Part("report_text") String reportText);
}
