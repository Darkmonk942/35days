package com.days35.days35app.retrofit;

import com.days35.days35app.objects.RegistrationObject;
import com.days35.days35app.objects.RequestRegistrationObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Darkmonk on 29.08.2016.
 */
public interface UserRegistration {
    @FormUrlEncoded
    @POST("api/user/registration")
    Call<RegistrationObject> createUser(@Field("email")String email,@Field("password")String password);

}
