package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 30.12.2016.
 */

public interface PostVotingId {
    @FormUrlEncoded
    @POST("api/voting/post")
    Call<Void> postVoting(@Header("Token") String token,
                                          @Field("ids") String allId);
}
