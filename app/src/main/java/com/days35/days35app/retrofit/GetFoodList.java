package com.days35.days35app.retrofit;

import com.days35.days35app.objects.FoodWeekObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 03.11.2016.
 */

public interface GetFoodList {
    @GET("api/week/foods/{id}")
    Call<FoodWeekObject> getFoodList(@Header("Token") String token,
                                    @Path("id") String weekId);
}
