package com.days35.days35app.retrofit;

import com.days35.days35app.objects.UserParamsObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 04.12.2016.
 */

public interface UserPostParams {
    @FormUrlEncoded
    @POST("api/user/user-stat")
    Call<UserParamsObject> postUserParams(@Header("Token") String token,
                                          @Field("weight") int weight,
                                          @Field("chest") int chest,
                                          @Field("waist") int waist,
                                          @Field("hips") int hips,
                                          @Field("feet") int feet,
                                          @Field("hand") int hand);
}
