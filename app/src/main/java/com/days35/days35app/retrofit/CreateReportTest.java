package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 22.12.2016.
 */

public interface CreateReportTest {
    @FormUrlEncoded
    @POST("api/week-trainings/fitness-test")
    Call<Void> sendReportTest(@Header("Token") String token,
                           @Field("week_id") int week_id,
                           @Field("burpee") int burpee,
                           @Field("dropout") int dropout,
                           @Field("pushup") int pushup,
                           @Field("hyperextension") int hyperextension,
                           @Field("climb") int climb,
                           @Field("press") int press);
}
