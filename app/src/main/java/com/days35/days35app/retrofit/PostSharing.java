package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 19.01.2017.
 */

public interface PostSharing {
    @POST("api/post/sharing/{post_id}")
    Call<Void> postShare(@Header("Token") String token,
                         @Path("post_id") String postId);
}
