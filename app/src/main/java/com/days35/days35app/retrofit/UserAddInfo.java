package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 18.09.2016.
 */
public interface UserAddInfo {
    @FormUrlEncoded
    @PUT("api/user/{id}")
    Call<Void> updateUser(@Path("id") String id,
                            @Header("Token") String token,
                                        @Field("first_name") String firstName,
                                        @Field("last_name") String lastName,
                                        @Field("email") String email,
                                        @Field("phone") String phone,
                                        @Field("height") int height,
                                        @Field("age") int age,
                                        @Field("sex") int sex,
                                        @Field("weight") int weight,
                                        @Field("chest") int chest,
                                        @Field("waist") int waist,
                                        @Field("hips") int hips,
                                        @Field("feet") int feet,
                                        @Field("hand") int hand);
}
