package com.days35.days35app.retrofit;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by DarKMonK on 20.10.2016.
 */

public interface CreateVideoPost {
    @Multipart
    @POST("api/post")
    Call<Void> sendVideoPost(@Header("Token") String token,
                             @Part MultipartBody.Part userVideo,
                             @Part("post_title") String postTitle);
}
