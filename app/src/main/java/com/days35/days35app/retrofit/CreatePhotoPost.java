package com.days35.days35app.retrofit;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by DarKMonK on 19.10.2016.
 */

public interface CreatePhotoPost {
    @Multipart
    @POST("api/post")
    Call<Void> sendPhotoPost(@Header("Token") String token,
                             @Part MultipartBody.Part user_pic,
                             @Part("post_title") String postTitle);
}
