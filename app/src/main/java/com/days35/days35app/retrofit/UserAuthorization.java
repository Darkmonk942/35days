package com.days35.days35app.retrofit;

import com.days35.days35app.objects.AuthorizationObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Darkmonk on 08.09.2016.
 */
public interface UserAuthorization {
    @FormUrlEncoded
    @POST("api/user/login")
    Call<AuthorizationObject> authUser(@Field("email")String email, @Field("password")String password);
}
