package com.days35.days35app.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by DarKMonK on 12.03.2017.
 */

public interface DownloadFile {
    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);
}
