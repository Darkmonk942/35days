package com.days35.days35app.retrofit;

import com.days35.days35app.objects.AuthorizationObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 25.01.2017.
 */

public interface PostFacebookEnter {
    @FormUrlEncoded
    @POST("api/user/signup-fb")
    Call<AuthorizationObject> postFBToken( @Field("token") String vkToken,
                                           @Field("fb_id") String fbId);
}
