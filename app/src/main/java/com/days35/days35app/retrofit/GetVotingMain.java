package com.days35.days35app.retrofit;

import com.days35.days35app.objects.voting.VotingMainScreenObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 27.12.2016.
 */

public interface GetVotingMain {
    @GET("api/voting/main")
    Call<VotingMainScreenObject> getVotingInfo(@Header("Token") String token);
}
