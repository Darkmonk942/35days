package com.days35.days35app.retrofit;

import com.days35.days35app.objects.reports.ReportVideoObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 21.12.2016.
 */

public interface GetReportsVideo {
    @GET("api/week-trainings/video-report")
    Call<ReportVideoObject> getVideoReports(@Header("Token") String token);
}
