package com.days35.days35app.retrofit;

import com.days35.days35app.objects.reports.ReportTestsObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 22.12.2016.
 */

public interface GetReportsTests {
    @GET("api/week-trainings/fitness-test")
    Call<ReportTestsObject> getReportsTests(@Header("Token") String token);
}
