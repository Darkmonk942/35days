package com.days35.days35app.retrofit;

import com.days35.days35app.objects.PostListObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by DarKMonK on 17.10.2016.
 */

public interface GetNewsFeed {
    @GET("api/post?")
    Call<List<PostListObject>> getPosts(@Query("page") int page, @Query("per-page") int paging,
                                        @Header("Token") String token);
}
