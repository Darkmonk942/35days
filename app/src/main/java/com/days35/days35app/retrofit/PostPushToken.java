package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 09.01.2017.
 */

public interface PostPushToken {
    @FormUrlEncoded
    @POST("api/user/register-token")
    Call<Void> postPushToken(@Header("Token") String token,
                             @Field("type") int platformType,
                             @Field("token") String pushToken);
}
