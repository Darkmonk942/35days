package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 22.02.2017.
 */

public interface LogoutRequest {
    @GET("api/user/logout")
    Call<Void> userLogout(@Header("Token") String token);
}
