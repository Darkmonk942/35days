package com.days35.days35app.retrofit;

import com.days35.days35app.objects.FoodRequestObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 07.11.2016.
 */

public interface GetFoodRecipes {
    @GET("api/week/foods/{id}")
    Call<FoodRequestObject> getFoodRecipes(@Header("Token") String token,
                                           @Path("id") String weekId);
}
