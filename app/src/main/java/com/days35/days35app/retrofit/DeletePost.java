package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 26.10.2016.
 */

public interface DeletePost {
    @DELETE("api/post/{id}")
    Call<Void> deletePost(@Header("Token") String token,
                          @Path("id") String postId);
}
