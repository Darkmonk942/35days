package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 16.09.2016.
 */
public interface RestorePassword {
    @FormUrlEncoded
    @POST("api/user/request-password-reset")
    Call<Void> restorePassword(@Field("email")String email);
}
