package com.days35.days35app.retrofit;

import com.days35.days35app.objects.bonus.BonusTrainingMainObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 03.01.2017.
 */

public interface GetBonusList {
    @GET("api/week-trainings/bonus-training-main")
    Call<BonusTrainingMainObject> getBonusList(@Header("Token") String token);
}
