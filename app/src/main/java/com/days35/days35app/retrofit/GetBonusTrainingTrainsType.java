package com.days35.days35app.retrofit;

import com.days35.days35app.objects.reports.TrainTasksBodyObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 11.04.2017.
 */

public interface GetBonusTrainingTrainsType {
    @GET("api/week-trainings/bonus-training/{id}")
    Call<List<TrainTasksBodyObject>> getTrainings(@Header("Token") String token,
                                                  @Path("id") String id);
}
