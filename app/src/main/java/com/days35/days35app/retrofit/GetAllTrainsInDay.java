package com.days35.days35app.retrofit;

import com.days35.days35app.objects.TrainTasksObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 15.11.2016.
 */

public interface GetAllTrainsInDay {
    @GET("api/week-trainings/day/training/{id}")
    Call<TrainTasksObject> getAllTasks(@Header("Token") String token,
                                                @Path("id") String dayId);
}
