package com.days35.days35app.retrofit;

import com.days35.days35app.objects.UserStatsObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 04.12.2016.
 */

public interface UserGetParams {
    @GET("api/user/user-stat")
    Call<UserStatsObject> getUserParams(@Header("Token") String token);
}
