package com.days35.days35app.retrofit;

import com.days35.days35app.objects.AuthorizationObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 21.01.2017.
 */

public interface PostVkEnter {
    @FormUrlEncoded
    @POST("api/user/signup-vk")
    Call<AuthorizationObject> postVkToken( @Field("token") String vkToken);
}
