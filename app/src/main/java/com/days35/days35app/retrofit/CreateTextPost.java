package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by DarKMonK on 11.10.2016.
 */

public interface CreateTextPost {
    @FormUrlEncoded
    @POST("api/post")
    Call<Void> sendTextPost(@Header("Token") String token,
                            @Field("post_text") String postText,
                            @Field("post_title") String postTitle);
}
