package com.days35.days35app.retrofit;

import com.days35.days35app.objects.reports.ReportMainObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 08.12.2016.
 */

public interface GetReportsInfo {
    @GET("api/user/main-reports")
    Call<List<ReportMainObject>> getReportsInfo(@Header("Token") String token);
}
