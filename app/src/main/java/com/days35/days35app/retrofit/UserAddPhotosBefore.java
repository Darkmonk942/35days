package com.days35.days35app.retrofit;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by DarKMonK on 04.10.2016.
 */

public interface UserAddPhotosBefore {
    @Multipart
    @POST("api/user/pics-before-upload")
    Call<Void> uploadImage(@Header("Token") String token,
                           @Part MultipartBody.Part front1Photo,
                           @Part MultipartBody.Part front2Photo,
                           @Part MultipartBody.Part backPhoto,
                           @Part MultipartBody.Part leftPhoto,
                           @Part MultipartBody.Part rightPhoto);
}
