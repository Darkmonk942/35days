package com.days35.days35app.retrofit;

import com.days35.days35app.objects.voting.VotingStartObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 26.12.2016.
 */

public interface GetVotingStart {
    @GET("api/voting/start")
    Call<VotingStartObject> getVotingPlayers(@Header("Token") String token);
}
