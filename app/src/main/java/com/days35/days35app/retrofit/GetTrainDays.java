package com.days35.days35app.retrofit;

import com.days35.days35app.objects.TrainDayObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 10.11.2016.
 */

public interface GetTrainDays {
    @GET("api/week-trainings/days/{id}")
    Call<List<TrainDayObject>> getDayTraining(@Header("Token") String token,
                                               @Path("id") String weekId);
}
