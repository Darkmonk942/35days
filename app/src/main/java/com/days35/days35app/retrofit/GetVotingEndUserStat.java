package com.days35.days35app.retrofit;

import com.days35.days35app.objects.voting.VotingTableStat;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 30.12.2016.
 */

public interface GetVotingEndUserStat {
    @GET("api/voting/get-my-quarterfinal")
    Call<VotingTableStat> getVotingUserStat(@Header("Token") String token);
}
