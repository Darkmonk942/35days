package com.days35.days35app.retrofit;

import com.days35.days35app.objects.voting.VotingTableAllStats;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 30.12.2016.
 */

public interface GetVotingEndAllStat {
    @GET("api/voting/get-quarterfinal")
    Call<List<VotingTableAllStats>> getVotingAllStat(@Header("Token") String token);
}
