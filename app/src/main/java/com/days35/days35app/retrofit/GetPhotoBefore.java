package com.days35.days35app.retrofit;

import com.days35.days35app.objects.PhotoBeforeObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 04.12.2016.
 */

public interface GetPhotoBefore {
    @GET("api/user/get-pics-before")
    Call<PhotoBeforeObject> getPhotoBefore(@Header("Token") String token);
}
