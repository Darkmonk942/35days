package com.days35.days35app.retrofit;

import com.days35.days35app.objects.AuthorizationObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 18.01.2017.
 */

public interface GetUserInfo {
    @GET("api/user/{id}")
    Call<AuthorizationObject> getUserInfo(@Header("Token") String token,
                                          @Path("id") String id);
}
