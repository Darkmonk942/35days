package com.days35.days35app.retrofit;

import com.days35.days35app.objects.ReportPicsObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 17.12.2016.
 */

public interface GetReportsPhoto {
    @GET("api/user/pic-report")
    Call<ReportPicsObject> getReportsPhoto(@Header("Token") String token);
}
