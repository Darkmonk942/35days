package com.days35.days35app.retrofit;

import com.days35.days35app.objects.TrainWeekObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by DarKMonK on 08.11.2016.
 */

public interface GetTrainWeeks {
    @GET("api/week-trainings")
    Call<List<TrainWeekObject>> getWeekTraining(@Header("Token") String token);
}
