package com.days35.days35app.retrofit;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by DarKMonK on 18.09.2016.
 */
public interface UserAddPhoto {
    @Multipart
    @POST("api/user/image-upload")
    Call<Void> uploadImage(@Part MultipartBody.Part user_pic, @Header("Token") String token);
}
