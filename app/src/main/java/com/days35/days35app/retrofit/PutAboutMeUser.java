package com.days35.days35app.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by DarKMonK on 04.03.2017.
 */

public interface PutAboutMeUser {
    @FormUrlEncoded
    @PUT("api/user/{id}")
    Call<Void> setAboutMe(@Header("Token") String token,
                          @Field("about_me") String aboutMe,
                          @Path("id") String id);
}
