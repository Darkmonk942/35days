package com.days35.days35app;

import android.content.Context;
import android.util.AttributeSet;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

/**
 * Created by DarKMonK on 05.01.2017.
 */

public class JCCustomPlayer extends JCVideoPlayerStandard {

    OnCustomEventListener onCustomEventListener;

    public JCCustomPlayer(Context context) {
        super(context);
    }

    public JCCustomPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onAutoCompletion() {
            super.onAutoCompletion();
        if(onCustomEventListener != null)
        {
            onCustomEventListener.onEvent();
        }
    }

    @Override
    public void onCompletion(){
        super.onCompletion();
    }


    public void setOnStopListener(OnCustomEventListener eventListener)
    {
        onCustomEventListener = eventListener;
    }


}
